package br.com.dexsystem.thread;

import java.awt.image.BufferedImage;

import com.futronictech.AnsiSDKLib;


public class CapturarThread extends OperacaoThread {

    private byte[] dedoCheck;
    private float matchScore = 0;


    public CapturarThread(OperacaoListener operacaoListener, int finger, float matchScore) {
        super(operacaoListener);
        this.finger = finger;
        this.matchScore = matchScore;
    }


    @Override
    public void run() {
        boolean dev_open = false;
        try {
            if (!isLeitorConectado()) {
                mensagem = getAnsiLib().GetErrorMessage();
                resultadoCaptura();
                return;
            }
            dev_open = true;
            if (!getAnsiLib().FillImageSize()) {
                mensagem =  getAnsiLib().GetErrorMessage();
                resultadoCaptura();
                return;
            }
            int[] realSize = new int[1];
            byte[] template = new byte[getAnsiLib().GetMaxTemplateSize()];
            imagemBuffer = new byte[getAnsiLib().GetImageSize()];
            for(;;) {
                if (IsCancelar()) {
                	setCancelada(false);
                    break;
                }
                if (getAnsiLib().CreateTemplate((byte) finger, imagemBuffer, template, realSize)) {
                    imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
                    resultadoImagem();
                    dedo = template;
                    break;
                } else {
                    int lastError = getAnsiLib().GetErrorCode();
                    if ((lastError == AnsiSDKLib.FTR_ERROR_EMPTY_FRAME) || (lastError == AnsiSDKLib.FTR_ERROR_NO_FRAME) || (lastError == AnsiSDKLib.FTR_ERROR_MOVABLE_FINGER )) {
                        Thread.sleep(100);
                        continue;
                    } else {
                        String erro = String.format("A captura falhou! Erro: %s.", getAnsiLib().GetErrorMessage());
                        mensagem = erro;
                        mensagemConsole(erro);
                        break;
                    }
                }
            }
            mensagemConsole("Retire seu dedo do leitor.");
            while(getAnsiLib().IsFingerPresent()) {}
            if (IsCancelar()) {
            	setCancelada(false);
				return;
			}
            mensagemConsole("Coloque seu dedo novamente no leitor");
            for(;;) {
            	if (IsCancelar()) {
            		setCancelada(false);
                    break;
                }
            	realSize = new int[1];
                template = new byte[getAnsiLib().GetMaxTemplateSize()];
                if (getAnsiLib().CreateTemplate((byte) finger, imagemBuffer, template, realSize)) {
                    imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
                    resultadoImagem();
                    dedoCheck = template;
                    break;
                } else {
                    int lastError = getAnsiLib().GetErrorCode();
                    if ((lastError == AnsiSDKLib.FTR_ERROR_EMPTY_FRAME) || (lastError == AnsiSDKLib.FTR_ERROR_NO_FRAME) || (lastError == AnsiSDKLib.FTR_ERROR_MOVABLE_FINGER )) {
                        Thread.sleep(100);
                        continue;
                    } else {
                        String erro = String.format("A captura falhou! Erro %s.", getAnsiLib().GetErrorMessage());
                        mensagem = erro;
                        mensagemConsole(erro);
                        break;
                    }
                }
            }
            mensagemConsole("Realizando match...");
			Thread.sleep(500);
            
			float[] matchResult = new float[1];
			boolean match = getAnsiLib().MatchTemplates(dedo, dedoCheck, matchResult);
			if (match) {
				if (matchResult[0] > matchScore) {
					mensagemConsole("Match realizado com sucesso!");
					Thread.sleep(800);
				} else {
					dedo = null;
					mensagemConsole("Falha no match!");
					Thread.sleep(800);
					mensagem = "As digitais n�o conferem, por favor, repita a a��o!";
				}
			} else {
				dedo = null;
				mensagemConsole("Falha no match!");
				Thread.sleep(800);
				mensagem = "As digitais n�o conferem, por favor, repita a a��o!";
			}
			cancelar();
			resultadoCaptura();
        } catch (Exception e) {
            mensagem = e.getMessage();
            resultadoCaptura();
            e.printStackTrace();
        }
        if(dev_open) {
            getAnsiLib().CloseDevice();
        }
        cancelar();
    }
}