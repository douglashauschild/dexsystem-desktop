package br.com.dexsystem.thread;

import java.awt.image.BufferedImage;
import java.util.List;

import com.futronictech.AnsiSDKLib;

public class VerificarThread extends OperacaoThread {

    private byte finger = 0;
    private List<byte[]> template = null;
    private float matchScore = 0;
    private boolean encerarThread;
    public int dedo;

    public VerificarThread(OperacaoListener operacaoListener, byte finger, List<byte[]> template, float matchScore, int dedo) {
        super(operacaoListener);
        this.finger = finger;
        this.template = template;
        this.matchScore = matchScore;
        this.dedo = dedo;
    }

    @Override
    public void run() {
        boolean dev_open = false;
        try {
            result = 0;
            if (!isLeitorConectado()) {
                mensagem = getAnsiLib().GetErrorMessage();
            	imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
            	resultadoImagem();
                resultadoVerificacao();
                return;
            }
            dev_open = true;
            if (!getAnsiLib().FillImageSize()) {
                mensagem = getAnsiLib().GetErrorMessage();
            	imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
            	resultadoImagem();
                resultadoVerificacao();
                return;
            }
            imagemBuffer = new byte[getAnsiLib().GetImageSize()];
            for(;;) {
                if (IsCancelar()) {
                	System.out.println("Finalizando Verify...");
                    result = 0;
                    imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
                    resultadoImagem();
                    resultadoVerificacao();
                    break;
                }
            	result = 0;
                float[] matchResult = new float[1];
                if (getAnsiLib().VerifyTemplate(finger, template.get(dedo), imagemBuffer, matchResult)) {
                    String op_info = String.format("Verificação concluida. Resultado: %s(%f).", matchResult[0] > matchScore ? "OK" : "FAILED", matchResult[0]);
                    System.out.println(op_info);
                    if (matchResult[0] > matchScore) {
                        result = 1;   	
                        encerarThread = true;
                    }
                } else {
                    int lastError = getAnsiLib().GetErrorCode();
                    if (lastError == AnsiSDKLib.FTR_ERROR_EMPTY_FRAME || lastError == AnsiSDKLib.FTR_ERROR_NO_FRAME || lastError == AnsiSDKLib.FTR_ERROR_MOVABLE_FINGER) {
                        Thread.sleep(100);
                        continue;
                    } 
                }
                
                if (encerarThread) {
                	System.out.println("Finalizando Verificação...");
					break;
				}
                
                imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
                resultadoImagem();
                resultadoVerificacao();  
            }
        } catch(Exception e) {
            mensagem = e.getMessage();
            result = 0;
            imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
            resultadoImagem();
            resultadoVerificacao();
        }
        if (dev_open) {
            getAnsiLib().CloseDevice();
            if (result == 1) {
            	imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY);
            	resultadoImagem();
            	resultadoVerificacao();  
        	}
        }
    }

	public void setDedo(int dedo) {
		this.dedo = dedo;
	}   
}