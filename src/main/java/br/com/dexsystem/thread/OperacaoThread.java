package br.com.dexsystem.thread;

import java.awt.image.BufferedImage;
import java.util.List;

import com.futronictech.AnsiSDKLib;

import br.com.dexsystem.model.vo.DigitalVO;

//import org.apache.woden.ErrorInfo;

public class OperacaoThread extends Thread {

	private AnsiSDKLib ansiLib = null;
    protected OperacaoListener operacaoListener = null;
    protected ErroInfo erroInfo = null;
    protected int result = 1;
    protected String mensagem;
    private boolean cancelada = false;
    public BufferedImage imagem;
    byte[] imagemBuffer;
    protected byte[] dedo;
    protected int finger = 0;
//    private Integer dedoIdentificado;
    
    
    public OperacaoThread() {
    }
    
    public OperacaoThread(OperacaoListener businessListener) {
    	this.ansiLib = new AnsiSDKLib();
        this.operacaoListener = businessListener;
    }
    
    public boolean isLeitorConectado() {
    	return ansiLib.OpenDevice();
    }

    public boolean IsCancelar() {
        return cancelada;
    }

    public void cancelar() {
        cancelada = true;
    }
    
    
//    protected void resultIdentify(DigitalVO empregadoDigital) {
//        if (operacaoListener != null) {
//            operacaoListener.resultIdentify(empregadoDigital, result, errInfo, mensagem);
//        }
//    }
    
    protected void resultadoVerificacao() {
        if (operacaoListener != null) {
            operacaoListener.resultadoVerificacao(result, erroInfo, mensagem);
        }
    }

    protected void resultadoCaptura() {
        if (operacaoListener != null) {
            operacaoListener.resultadoCaptura(dedo, finger, result, erroInfo, mensagem);
        }
    }

    protected void resultadoImagem() {
        if (operacaoListener != null) {
            operacaoListener.resultadoImagem(imagem, imagemBuffer);
        }
    }

    protected void resultadoIdentificacao(List<DigitalVO> empregados) {
        if (operacaoListener != null) {
            operacaoListener.resultadoIdentificacao(empregados, result, erroInfo, mensagem);
        }
    }
//    
//    protected void resultIdentifyDX(List<ConfirmacaoVO> empregados) {
//        if (operacaoListener != null) {
//            operacaoListener.resultIdentify(empregados);
//        }
//    }
//    
//    protected void resultStatusIdentify(boolean status) {
//        if (operacaoListener != null) {
//            operacaoListener.resultStatusIdentify(status);
//        }
//    }
    
    protected void mensagemConsole(String message) {
        if (operacaoListener != null) {
            operacaoListener.mensagemConsole(message);
        }
    }
    
    /********   Getters and Setters   ********/
    
	public AnsiSDKLib getAnsiLib() {
		return ansiLib;
	}

	public void setAnsiLib(AnsiSDKLib ansiLib) {
		this.ansiLib = ansiLib;
	}
    
	public boolean isCancelada() {
		return cancelada;
	}

	public void setCancelada(boolean cancelada) {
		this.cancelada = cancelada;
	}
}