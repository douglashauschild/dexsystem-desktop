package br.com.dexsystem.thread;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.util.ArrayList;
import java.util.List;

import com.futronictech.AnsiSDKLib;

import br.com.dexsystem.model.vo.DigitalVO;

public class IdentificarThread extends OperacaoThread {

    private byte finger = 0;
    private List<DigitalVO> template = null;
    private float matchScore = 0;

    public IdentificarThread(OperacaoListener operacaoListener, byte finger, List<DigitalVO> template, float matchScore) {
        super(operacaoListener);
        this.finger = finger;
        this.template = template;
        this.matchScore = matchScore;
    }

    @Override
    public void run() {
        boolean dev_open = false;
        try {
            result = 0;
            if (!isLeitorConectado()) {
                return;
            }
            dev_open = true;
            if (!getAnsiLib().FillImageSize()) {
                return;
            }
            imagemBuffer = new byte[getAnsiLib().GetImageSize()];
            for(;;) {
                if (IsCancelar()) {
                	System.out.println("Finalizando Identificação...");
                	setCancelada(false);
                    break;
                }
                int tmplSize = getAnsiLib().GetMaxTemplateSize();
                byte[] templateBase = new byte[tmplSize];
                int[] realSize = new int[1];
                
                if (getAnsiLib().CreateTemplate(finger, imagemBuffer, templateBase, realSize)) {
                    System.out.println("Identificando...");
                    imagem = new BufferedImage(getAnsiLib().GetImageWidth(), getAnsiLib().GetImageHeight(), BufferedImage.TYPE_BYTE_GRAY );
                    
                    resultadoImagem();
                    
                    DataBuffer db1 = imagem.getRaster().getDataBuffer();
                    for( int i = 0; i < db1.getSize(); i++ ) {
                        db1.setElem( i, imagemBuffer[i] );
                    }
                    procurarNoTemplate(templateBase);
                    break;
                } else {
                    int lastError = getAnsiLib().GetErrorCode();
                    if (lastError == AnsiSDKLib.FTR_ERROR_EMPTY_FRAME || 
                        lastError == AnsiSDKLib.FTR_ERROR_NO_FRAME ||
                        lastError == AnsiSDKLib.FTR_ERROR_MOVABLE_FINGER ) {
                        Thread.sleep(100);
                        continue;
                    } else {
                        String error = String.format("Captura falhou. Erro: %s.",  getAnsiLib().GetErrorMessage());   
                        break;
                    }
                }
            }
        } catch(Exception e) {
        }
        if (dev_open) {
            getAnsiLib().CloseDevice();
        }
    }
    
    
    private void procurarNoTemplate(byte[] baseTemplate) {
        List<DigitalVO> empregados = new ArrayList<DigitalVO>();
        float[] matchResult = new float[1];
        for (int i=0; i<template.size(); i++) { //Percorre todos os empregados
        	if (IsCancelar()) {
        		System.out.println("Finalizando Identify...");
                break;
            }
        	for (int x=0; x<template.get(i).getDigitais().size(); x++) { //Percorre todas as digitais
                if (getAnsiLib().MatchTemplates(baseTemplate, template.get(i).getDigitais().get(x), matchResult ) && matchResult[0] > matchScore) {
                	if (IsCancelar()) {
                		System.out.println("Finalizando Identify...");
                		setCancelada(false);
                        break;
                    }
                	empregados.add(template.get(i));
                    System.out.println("Finalizando Identify...");
                    break;
                }
        	}
        }
        resultadoIdentificacao(empregados);
    }
}