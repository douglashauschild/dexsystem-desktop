package br.com.dexsystem.thread;

import java.awt.Frame;

import br.com.dexsystem.service.sincronizacao.Sincronizacao;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.PropertiesUtils;

public class SincronizacaoThread implements Runnable {
	
	private String token;
	private Sincronizacao sincronizacao;

	public SincronizacaoThread(Frame parent, String token) {
		super();
		this.token = token;
		this.sincronizacao = new Sincronizacao(parent);
	}


	public void run() {
		while (true) {
			try {
				Thread.sleep(PropertiesUtils.getTempoSincronizacao(true));
				if (ConexaoThread.temConexao) {
					sincronizacao.sincronizar(token);
				}
			} catch (InterruptedException e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
		}
	}
}