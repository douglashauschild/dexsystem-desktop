package br.com.dexsystem.thread;

public interface ErroLocator {

public String getDocumentBaseURI();
    
    public String getLocationURI();
    
    public int getLineNumber();
    
    public int getColumnNumber();
}
