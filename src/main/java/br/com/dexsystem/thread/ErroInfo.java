package br.com.dexsystem.thread;

public interface ErroInfo {

	public ErroLocator getErrorLocator();

    public String getKey();

    public String getMessage();
    
    public Exception getException();
}
