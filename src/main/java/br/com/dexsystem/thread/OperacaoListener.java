package br.com.dexsystem.thread;

import java.awt.image.BufferedImage;
import java.util.List;

import br.com.dexsystem.model.vo.DigitalVO;

public interface OperacaoListener {

//    void resultIdentify(DigitalVO empregadoDigital, int result, ErrorInfo errInfo, String message);
    void resultadoVerificacao(int result, ErroInfo errInfo, String mensagem);
    void resultadoCaptura(byte[] dedo, int finger, int result, ErroInfo errInfo, String mensagem);
    void resultadoImagem(BufferedImage imagemDigital, byte[] imagemBuffer);
    void resultadoIdentificacao(List<DigitalVO> empregados, int result, ErroInfo errInfo, String message);
//	void resultStatusIdentify(boolean status);
//	void resultIdentify(List<ConfirmacaoVO> empregadosEncontrados);
	void mensagemConsole(String messagem);
}