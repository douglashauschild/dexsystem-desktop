package br.com.dexsystem.thread;

import br.com.dexsystem.util.InternetUtils;
import br.com.dexsystem.util.LoggerUtils;

public class ConexaoThread implements Runnable {

	public static boolean temConexao; 

	
	public void run() {
		while (true) {
			verificaConexoes();
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
		}
	}
	
	public void verificaConexoes() {
		System.out.println("Verificando conexao...");
		temConexao = InternetUtils.isConexaoInternet() && InternetUtils.isConexaoServico();
		
		if (!temConexao) {
			System.out.println("Sem conex�o com a internet");
		}
	}
}