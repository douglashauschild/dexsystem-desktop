package br.com.dexsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa_digital")	
@NamedQueries({
	@NamedQuery(name = "pessoaDigital.max", query = "SELECT MAX(p.id) FROM PessoaDigital p") 
})
public class PessoaDigital extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Sincronizar
	@Column(name = "posicao")
	private String posicao;
	
	@Sincronizar
	@Column(name = "digital")
	private byte[] digital;
	
	@Sincronizar
	@Column(name = "anulado")
	private Character anulado;
	
	@Sincronizar
	@Column(name = "motivo_anulacao")
	private String motivoAnulacao;
	
	
	public PessoaDigital() {
		super();
	}
	
	public boolean isAnulado() {
		return anulado != null && anulado.equals('1');
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getPosicao() {
		return posicao;
	}

	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}

	public byte[] getDigital() {
		return digital;
	}

	public void setDigital(byte[] digital) {
		this.digital = digital;
	}

	public Character getAnulado() {
		return anulado;
	}

	public void setAnulado(Character anulado) {
		this.anulado = anulado;
	}

	public String getMotivoAnulacao() {
		return motivoAnulacao;
	}

	public void setMotivoAnulacao(String motivoAnulacao) {
		this.motivoAnulacao = motivoAnulacao;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getPosicao(), getDigital(), getAnulado(), getMotivoAnulacao());
	}
}