package br.com.dexsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "empresa")	
public class Empresa extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Column(name = "tipo")
	private Character tipo;
	
	@Column(name = "matriz_id")
	private Long idMatriz;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private PessoaJuridica pessoaJuridica;
	
	public Empresa() {
		super();
	}
	
	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}


	public Long getIdMatriz() {
		return idMatriz;
	}

	public void setIdMatriz(Long idMatriz) {
		this.idMatriz = idMatriz;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getTipo(), getIdMatriz());
	}
}