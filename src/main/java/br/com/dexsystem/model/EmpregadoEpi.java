package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.dexsystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dexsystem.util.FormatacaoUtils;

@Entity
@Table(name = "empregado_epi")	
@NamedQueries({
	@NamedQuery(name = "empregadoEpi.max", query = "SELECT MAX(e.id) FROM EmpregadoEpi e") 
})
public class EmpregadoEpi extends SincronizacaoAbstract implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "empregado_id")
	private Long idEmpregado;
	
	@Sincronizar
	@Column(name = "epi_id")
	private Long idEpi;
	
	@Sincronizar
	@Column(name = "usuario_id")
	private Long idUsuario;
	
	@Sincronizar
	@Column(name = "grade_item_id")
	private Long idGradeItem;
	
	@Sincronizar
	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prevista_entrega")
	private Date dataPrevistaEntrega;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_entrega")
	private Date dataEntrega;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prevista_troca")
	private Date dataPrevistaTroca;
	
	@Sincronizar
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_confirmacao")
	private Date dataConfirmacao;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_substituicao")
	private Date dataSubstituicao;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_devolucao")
	private Date dataDevolucao;
	
	@Sincronizar
	@Column(name = "motivo_id")
	private Long idMotivo;
	
	@Sincronizar
	@Column(name = "tipo_confirmacao")
	private Character tipoConfirmacao;
	
	@Sincronizar
	@Column(name = "inclusao_manual")
	private Character inclusaoManual;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "empregado_id", insertable = false, updatable = false)
    private Empregado empregado;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "epi_id", insertable = false, updatable = false)
    private Epi epi;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "grade_item_id", insertable = false, updatable = false)
    private GradeItem gradeItem;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "motivo_id", insertable = false, updatable = false)
    private Motivo motivo;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", insertable = false, updatable = false)
    private Usuario usuario;

	public EmpregadoEpi() {
		super();
		this.excluido = '0';
	}
	
	public EmpregadoEpi clone() throws CloneNotSupportedException {
		return (EmpregadoEpi) super.clone();
    }
	
	public boolean isConfirmado() {
		return dataConfirmacao != null;
	}

	public boolean isSubstituicao() {
		return dataConfirmacao != null;
	}
	
	public boolean isFinalizado() {
		return dataSubstituicao != null || dataDevolucao != null;
	}
	
	public String getDataPrevistaEntregaFormatada() {
		return dataPrevistaEntrega != null ? FormatacaoUtils.getDataString(dataPrevistaEntrega) : "";
	}
	
	public String getDataEntregaFormatada() {
		return dataEntrega != null ? FormatacaoUtils.getDataString(dataEntrega) : "";
	}
	
	public String getDataConfirmacaoFormatada() {
		return dataConfirmacao != null ? FormatacaoUtils.getDataHoraString(dataConfirmacao) : "";
	}
	
	public String getDataPrevistaTrocaFormatada() {
		return dataPrevistaTroca != null ? FormatacaoUtils.getDataString(dataPrevistaTroca) : "";
	}
	
	public String getDataSubstituicaoFormatada() {
		return dataSubstituicao != null ? FormatacaoUtils.getDataString(dataSubstituicao) : "";
	}
	
	public String getDataDevolucaoFormatada() {
		return dataDevolucao != null ? FormatacaoUtils.getDataString(dataDevolucao) : "";
	}
	
	public String getAcaoEntrega() throws Exception {
		String retorno = "";
		if (dataConfirmacao != null) {
			retorno = "Substituição";
		} else {
			retorno = "Primeira Entrega";
		}
		return retorno;
	}
	
	public String getStatusDescricao() throws Exception {
		String retorno = "";
		if (dataSubstituicao != null) {
			retorno = StatusEmpregadoEpiEnum.SUBSTITUIDO.getDescricao();
		} else if (dataDevolucao != null) {
			retorno = StatusEmpregadoEpiEnum.DEVOLVIDO.getDescricao();
		} else if (dataConfirmacao == null && dataPrevistaEntrega != null && dataPrevistaEntrega.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getDescricao();
		} else if (dataConfirmacao != null && dataPrevistaTroca != null && dataPrevistaTroca.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.VENCIDO.getDescricao();		
		} else if (dataConfirmacao != null) {
			retorno = StatusEmpregadoEpiEnum.EM_DIA.getDescricao();
		} else {
			retorno = StatusEmpregadoEpiEnum.AGUARDANDO.getDescricao();
		}
		return retorno;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public Long getIdGradeItem() {
		return idGradeItem;
	}

	public void setIdGradeItem(Long idGradeItem) {
		this.idGradeItem = idGradeItem;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataPrevistaEntrega() {
		return dataPrevistaEntrega;
	}

	public void setDataPrevistaEntrega(Date dataPrevistaEntrega) {
		this.dataPrevistaEntrega = dataPrevistaEntrega;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataPrevistaTroca() {
		return dataPrevistaTroca;
	}

	public void setDataPrevistaTroca(Date dataPrevistaTroca) {
		this.dataPrevistaTroca = dataPrevistaTroca;
	}

	public Date getDataConfirmacao() {
		return dataConfirmacao;
	}

	public void setDataConfirmacao(Date dataConfirmacao) {
		this.dataConfirmacao = dataConfirmacao;
	}

	public Date getDataSubstituicao() {
		return dataSubstituicao;
	}

	public void setDataSubstituicao(Date dataSubstituicao) {
		this.dataSubstituicao = dataSubstituicao;
	}

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Long getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Long idMotivo) {
		this.idMotivo = idMotivo;
	}

	public Character getTipoConfirmacao() {
		return tipoConfirmacao;
	}

	public void setTipoConfirmacao(Character tipoConfirmacao) {
		this.tipoConfirmacao = tipoConfirmacao;
	}

	public Character getInclusaoManual() {
		return inclusaoManual;
	}

	public void setInclusaoManual(Character inclusaoManual) {
		this.inclusaoManual = inclusaoManual;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Empregado getEmpregado() {
		return empregado;
	}

	public void setEmpregado(Empregado empregado) {
		this.empregado = empregado;
	}

	public Epi getEpi() {
		return epi;
	}

	public void setEpi(Epi epi) {
		this.epi = epi;
	}

	public GradeItem getGradeItem() {
		return gradeItem;
	}

	public void setGradeItem(GradeItem gradeItem) {
		this.gradeItem = gradeItem;
	}

	public Motivo getMotivo() {
		return motivo;
	}

	public void setMotivo(Motivo motivo) {
		this.motivo = motivo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getIdGradeItem(), getQuantidade(), getDataPrevistaEntrega(), getDataEntrega(), getDataPrevistaTroca(), getDataConfirmacao(), getDataSubstituicao(), getDataDevolucao(), getIdMotivo(), getTipoConfirmacao(), getInclusaoManual(), getExcluido());
	}
}