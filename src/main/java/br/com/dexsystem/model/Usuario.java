package br.com.dexsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.dexsystem.model.constant.SimNaoEnum;

@Entity
@Table(name = "usuario")	
public class Usuario extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "nome")
	private String nome;	

	@Column(name = "email")
	private String email;

	@Column(name = "senha")
	private String senha;
	
	@Column(name = "acesso_desktop")
	private Character acessoModuloDesktop;
	
	@Column(name = "excluido")
	private Character excluido;
	
	
	public Usuario() {
		super();
	}


	public boolean isExcluido() {
		return excluido != null && excluido.equals('1');
	}
	
	public boolean isPossuiAcessoDesktop() {
		return acessoModuloDesktop != null && acessoModuloDesktop.equals(SimNaoEnum.SIM.getKey());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	public Character getAcessoModuloDesktop() {
		return acessoModuloDesktop;
	}

	public void setAcessoModuloDesktop(Character acessoModuloDesktop) {
		this.acessoModuloDesktop = acessoModuloDesktop;
	}


	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getNome(), getSenha(), getEmail(), getAcessoModuloDesktop(), getExcluido());
	}
}