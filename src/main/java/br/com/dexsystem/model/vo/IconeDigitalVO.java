package br.com.dexsystem.model.vo;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.Icon;

public class IconeDigitalVO implements Icon {
	
	public IconeDigitalVO() {
		m_Image = null;
	}

	public void paintIcon(Component c, Graphics g, int x, int y) {
		if (m_Image != null) {
			g.drawImage(m_Image, x, y, getIconWidth(), getIconHeight(), null);
		} else {
			g.fillRect(x, y, getIconWidth(), getIconHeight());
		}
	}

	public int getIconWidth() {
		return 160;
	}

	public int getIconHeight() {
		return 240;
	}

	public boolean LoadImage(InputStream file) {
		boolean bRetCode = false;
		Image newImg;
		try {
			newImg = ImageIO.read(file);
			bRetCode = true;
			setImage(newImg);
		} catch (IOException e) {
		}
		return bRetCode;
	}

	public boolean LoadImage(String path) {
		boolean bRetCode = false;
		Image newImg;
		try {
			File f = new File(path);
			newImg = ImageIO.read(f);
			bRetCode = true;
			setImage(newImg);
		} catch (IOException e) {
		}
		return bRetCode;
	}

	public void setImage(Image Img) {
		if (Img != null) {
			m_Image = Img.getScaledInstance(getIconWidth(), getIconHeight(), Image.SCALE_FAST);
		} else {
			m_Image = null;
		}
	}

	private Image m_Image;
}
