package br.com.dexsystem.model.vo;

public class EstatisticasEntregaVO {

	private Integer aguardandoEntrega;
	private Integer epiDia;
	private Integer epiVencido;
	private Integer entregasAtrasadas;
	
	public EstatisticasEntregaVO(int aguardandoEntrega, int epiDia, int epiVencido, int entregasAtrasadas) {
		super();
		this.aguardandoEntrega = aguardandoEntrega;
		this.epiDia = epiDia;
		this.epiVencido = epiVencido;
		this.entregasAtrasadas = entregasAtrasadas;
	}

	public Integer getAguardandoEntrega() {
		return aguardandoEntrega;
	}

	public void setAguardandoEntrega(Integer aguardandoEntrega) {
		this.aguardandoEntrega = aguardandoEntrega;
	}

	public Integer getEpiDia() {
		return epiDia;
	}

	public void setEpiDia(Integer epiDia) {
		this.epiDia = epiDia;
	}

	public Integer getEpiVencido() {
		return epiVencido;
	}

	public void setEpiVencido(Integer epiVencido) {
		this.epiVencido = epiVencido;
	}

	public Integer getEntregasAtrasadas() {
		return entregasAtrasadas;
	}

	public void setEntregasAtrasadas(Integer entregasAtrasadas) {
		this.entregasAtrasadas = entregasAtrasadas;
	}
}