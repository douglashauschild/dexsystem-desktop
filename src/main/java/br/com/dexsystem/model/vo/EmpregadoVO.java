package br.com.dexsystem.model.vo;

public class EmpregadoVO {

	private Long id;
	private Long idPessoa;
	private String empresa;
	private String nome;
	private String idade;
	private String matricula;
	private String cpf;
	private String rg;
	private String dataAdmissao;
	private String dataDemissao;
	private String setor;
	private String cargo;
	private String senha;
		
	
	public EmpregadoVO() {
		super();
	}

	public EmpregadoVO(Long id, Long idPessoa, String empresa, String nome, String idade, String matricula, String cpf, String rg, String admissao, String demissao, String setor, String cargo, String senha) {
		super();
		this.id = id;
		this.idPessoa = idPessoa;
		this.empresa = getAjustarInformacao(empresa);
		this.nome = getAjustarInformacao(nome);
		this.idade = getAjustarInformacao(idade);
		this.matricula = getAjustarInformacao(matricula);
		this.cpf = getAjustarInformacao(cpf);
		this.rg = getAjustarInformacao(rg);
		this.dataAdmissao = admissao;
		this.dataDemissao = demissao;
		this.setor = getAjustarInformacao(setor);
		this.cargo = getAjustarInformacao(cargo);
		this.senha = senha;
	}
	
	private String getAjustarInformacao(String informacao) {
		return informacao != null && !informacao.isEmpty() ? informacao : "N�o informado";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(String dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}