package br.com.dexsystem.model.vo;

import java.io.Serializable;

import org.hibernate.type.Type;

public class ColunaVO implements Serializable {

	private static final long serialVersionUID = -4767795363176143310L;
	private String colunaNativa;
	private String coluna;
	private Type tipo;
	
	public ColunaVO() {
		super();
	}
	
	public ColunaVO(String colunaNativa, String coluna, Type tipo) {
		super();
		this.colunaNativa = colunaNativa;
		this.coluna = coluna;
		this.tipo = tipo;
	}

	public String getColunas() {
		return colunaNativa + " as " + coluna;
	}

	public String getColunaNativa() {
		return colunaNativa;
	}

	public void setColunaNativa(String colunaNativa) {
		this.colunaNativa = colunaNativa;
	}

	public String getColuna() {
		return coluna;
	}

	public void setColuna(String coluna) {
		this.coluna = coluna;
	}

	public Type getTipo() {
		return tipo;
	}

	public void setTipo(Type tipo) {
		this.tipo = tipo;
	}
}