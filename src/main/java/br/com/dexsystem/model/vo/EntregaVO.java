package br.com.dexsystem.model.vo;

import java.util.Date;

public class EntregaVO {

	private boolean selecionado;
	private Long id;
	private Long idEpi;
	private String epi;
	private String ca;
	private Integer quantidade;
	private Date dataPrevistaEntrega;
	private Date dataEntrega;
	private Date dataPrevistaTroca;
	private String status;
	
	private Date dataSubstituicao;
	private Date dataDevolucao;
	private boolean caVencido;
	
	public EntregaVO() {
		super();
	}

	public EntregaVO(Long id, Long idEpi, String epi, String ca, Integer quantidade, Date dataPrevistaEntrega, Date dataEntrega, Date dataPrevistaTroca, String status, Date dataSubstituicao, Date dataDevolucao, boolean caVencido) {
		super();
		this.id = id;
		this.idEpi = idEpi;
		this.epi = epi;
		this.ca = ca;
		this.quantidade = quantidade;
		this.dataPrevistaEntrega = dataPrevistaEntrega;
		this.dataEntrega = dataEntrega;
		this.dataPrevistaTroca = dataPrevistaTroca;
		this.status = status;
		
		this.dataSubstituicao = dataSubstituicao;
		this.dataDevolucao = dataDevolucao;
		this.caVencido = caVencido;
	}
	
	public boolean isSubstituido() {
		return dataSubstituicao != null;
	}
	
	public boolean isDevolvido() {
		return dataSubstituicao == null && dataDevolucao != null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public String getEpi() {
		return epi;
	}

	public void setEpi(String epi) {
		this.epi = epi;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataPrevistaEntrega() {
		return dataPrevistaEntrega;
	}

	public void setDataPrevistaEntrega(Date dataPrevistaEntrega) {
		this.dataPrevistaEntrega = dataPrevistaEntrega;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataPrevistaTroca() {
		return dataPrevistaTroca;
	}

	public void setDataPrevistaTroca(Date dataPrevistaTroca) {
		this.dataPrevistaTroca = dataPrevistaTroca;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public Date getDataSubstituicao() {
		return dataSubstituicao;
	}

	public void setDataSubstituicao(Date dataSubstituicao) {
		this.dataSubstituicao = dataSubstituicao;
	}

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public boolean isCaVencido() {
		return caVencido;
	}

	public void setCaVencido(boolean caVencido) {
		this.caVencido = caVencido;
	}
}