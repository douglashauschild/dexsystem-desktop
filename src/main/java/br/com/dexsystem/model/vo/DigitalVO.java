package br.com.dexsystem.model.vo;

import java.util.List;

public class DigitalVO {
	
	private Long idEmpregado;
	private List<byte[]> digitais;


	/***********   Getters and Setters   ***********/	

	public List<byte[]> getDigitais() {
		return digitais;
	}

	public void setDigitais(List<byte[]> digitais) {
		this.digitais = digitais;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}
}