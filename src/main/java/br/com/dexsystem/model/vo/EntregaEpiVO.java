package br.com.dexsystem.model.vo;

import br.com.dexsystem.model.GradeItem;

public class EntregaEpiVO {

	private boolean devolver;
	private boolean substituir;
	private Long id;
	private Long idEpi;
	private Long idGrade;
	private String epi;
	private String ca;
	private String acao;
	private Integer quantidade;
	private SelectVO tamanho;
	private SelectVO motivo;
	
	private Integer quantidadeOriginal;
	private SelectVO tamanhoOriginal;
	
	public EntregaEpiVO(Long id, Long idEpi, String epi, String ca, String acao, Integer quantidade, Long idGrade, GradeItem gradeItem) {
		super();
		this.devolver = acao.equals("Primeira Entrega") ? false: true;
		this.substituir = acao.equals("Primeira Entrega") ? false: true;
		this.id = id;
		this.idEpi = idEpi;
		this.idGrade = idGrade;
		this.epi = epi;
		this.ca = ca;
		this.acao = acao;
		this.quantidade = quantidade;
		
		if (gradeItem != null) {
			this.tamanho = new SelectVO(gradeItem.getId(), gradeItem.getTamanho());
		} else {
			this.tamanho = new SelectVO(null, idGrade != null ? "-- Selecione --" : "EPI sem grade");
		}
		this.motivo = new SelectVO(null, "-- Selecione --");
		
		this.quantidadeOriginal = this.quantidade;
		this.tamanhoOriginal = this.tamanho;
	}

	public EntregaEpiVO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public String getEpi() {
		return epi;
	}

	public void setEpi(String epi) {
		this.epi = epi;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public boolean isDevolver() {
		return devolver;
	}

	public void setDevolver(boolean devolver) {
		this.devolver = devolver;
	}

	public boolean isSubstituir() {
		return substituir;
	}

	public void setSubstituir(boolean substituir) {
		this.substituir = substituir;
	}

	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		this.acao = acao;
	}

	public SelectVO getTamanho() {
		return tamanho;
	}

	public void setTamanho(SelectVO tamanho) {
		this.tamanho = tamanho;
	}

	public SelectVO getMotivo() {
		return motivo;
	}

	public void setMotivo(SelectVO motivo) {
		this.motivo = motivo;
	}

	public Long getIdGrade() {
		return idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}

	public Integer getQuantidadeOriginal() {
		return quantidadeOriginal;
	}

	public void setQuantidadeOriginal(Integer quantidadeOriginal) {
		this.quantidadeOriginal = quantidadeOriginal;
	}

	public SelectVO getTamanhoOriginal() {
		return tamanhoOriginal;
	}

	public void setTamanhoOriginal(SelectVO tamanhoOriginal) {
		this.tamanhoOriginal = tamanhoOriginal;
	}
}