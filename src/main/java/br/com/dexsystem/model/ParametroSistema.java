package br.com.dexsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.dexsystem.model.constant.SimNaoEnum;
import br.com.dexsystem.model.constant.TipoSelecaoEmpregadoEnum;
import br.com.dexsystem.model.constant.TipoSelecaoEmpresaEnum;
import br.com.dexsystem.model.constant.TipoSelecaoEpiEnum;
import br.com.dexsystem.model.constant.TipoSelecaoPessoaFisicaEnum;

@Entity
@Table(name = "parametro_sistema")	
public class ParametroSistema extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "selecao_empresa")
	private Character selecaoEmpresa;
	
	@Column(name = "selecao_epi")
	private Character selecaoEpi;
	
	@Column(name = "selecao_pessoa_fisica")
	private Character selecaoPessoaFisica;
	
	@Column(name = "selecao_empregado")
	private Character selecaoEmpregado;
	
	@Column(name = "informar_motivo")
	private Character informarMotivo;
	
	@Column(name = "texto_ficha_epi")
	private String textoFichaEpi;
	
	
	public ParametroSistema() {
		super();
	}
	
	public boolean isSelecaoEmpresaSimples() {
		return selecaoEmpresa != null && selecaoEmpresa.equals(TipoSelecaoEmpresaEnum.RAZAO_SOCIAL.getKey());
	}
	
	public boolean isSelecaoEmpregadoSimples() {
		return selecaoEmpregado != null && selecaoEmpregado.equals(TipoSelecaoEmpregadoEnum.NOME.getKey());
	}
	
	public boolean isSelecaoPessoaFisicaSimples() {
		return selecaoPessoaFisica != null && selecaoPessoaFisica.equals(TipoSelecaoPessoaFisicaEnum.NOME.getKey());
	}
	
	public boolean isSelecaoEpiSimples() {
		return selecaoEpi != null && selecaoEpi.equals(TipoSelecaoEpiEnum.NOME.getKey());
	}
	
	public boolean isInformarMotivoEntrega() {
		return informarMotivo != null && informarMotivo.equals(SimNaoEnum.SIM.getKey());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Character getSelecaoEmpresa() {
		return selecaoEmpresa;
	}

	public void setSelecaoEmpresa(Character selecaoEmpresa) {
		this.selecaoEmpresa = selecaoEmpresa;
	}

	public Character getSelecaoEpi() {
		return selecaoEpi;
	}

	public void setSelecaoEpi(Character selecaoEpi) {
		this.selecaoEpi = selecaoEpi;
	}

	public Character getSelecaoPessoaFisica() {
		return selecaoPessoaFisica;
	}

	public void setSelecaoPessoaFisica(Character selecaoPessoaFisica) {
		this.selecaoPessoaFisica = selecaoPessoaFisica;
	}

	public Character getSelecaoEmpregado() {
		return selecaoEmpregado;
	}

	public void setSelecaoEmpregado(Character selecaoEmpregado) {
		this.selecaoEmpregado = selecaoEmpregado;
	}

	public Character getInformarMotivo() {
		return informarMotivo;
	}

	public void setInformarMotivo(Character informarMotivo) {
		this.informarMotivo = informarMotivo;
	}

	public String getTextoFichaEpi() {
		return textoFichaEpi;
	}

	public void setTextoFichaEpi(String textoFichaEpi) {
		this.textoFichaEpi = textoFichaEpi;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getSelecaoEmpresa(), getSelecaoEpi(), getSelecaoPessoaFisica(), getSelecaoEmpregado(), getInformarMotivo(), getTextoFichaEpi());
	}
}