package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.dexsystem.util.FormatacaoUtils;

@Entity
@Table(name = "empregado")	
public class Empregado extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Column(name = "empresa_id")
	private Long idEmpresa;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_admissao")
	private Date dataAdmissao;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_demissao")
	private Date dataDemissao;
	
	@Column(name = "matricula")
	private String matricula;
	
	@Column(name = "senha")
	private String senha;
	
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private PessoaFisica pessoaFisica;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "empresa_id", insertable = false, updatable = false)
    private Empresa empresa;
	
	public Empregado() {
		super();
	}
	
	public boolean isAdmitido() {
		Date dataHoje = new Date();
		return (dataAdmissao.before(dataHoje) || dataAdmissao.equals(dataHoje)) && (dataDemissao == null || dataDemissao.after(dataHoje));
	}
	
	public String getDataAdmissaoFormatada() {
		return dataAdmissao != null ? FormatacaoUtils.getDataString(dataAdmissao) : "";
	}
	
	public String getDataDemissaoFormatada() {
		return dataDemissao != null ? FormatacaoUtils.getDataString(dataDemissao) : "";
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Date getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getDataAdmissao(), getDataDemissao(), getMatricula(), getSenha(), getExcluido());
	}
}