package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "controle_sincronizacao")	
@NamedQueries({
	@NamedQuery(name = "controleSincronizacao.max", query = "SELECT MAX(c.id) FROM ControleSincronizacao c") 
})
public class ControleSincronizacao extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "dispositivo_id")
	private Long idDispositivo;
	
	@Column(name = "tabela")
	private String tabela;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date dataHora;
	
	
	public ControleSincronizacao() {
		super();
	}
	

	public ControleSincronizacao(Long idDispositivo, String tabela, String checksum, String checksumAlteracao) {
		super(checksum, checksumAlteracao);
		this.idDispositivo = idDispositivo;
		this.tabela = tabela;
		this.dataHora = new Date();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return null;
	}
}