package br.com.dexsystem.model;

public interface SincronizacaoInterface {

	public String getHash(boolean inclusao) throws Exception;
}