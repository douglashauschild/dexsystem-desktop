package br.com.dexsystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unidade_medida")	
public class UnidadeMedida extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "descricao")
	private String descricao;	
	
	@Column(name = "excluido")
	private Character excluido;
	

	public UnidadeMedida() {
		super();
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getDescricao(), getExcluido());
	}
}