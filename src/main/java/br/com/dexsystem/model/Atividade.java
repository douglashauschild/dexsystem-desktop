package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.dexsystem.util.FormatacaoUtils;

@Entity
@Table(name = "atividade")	
public class Atividade extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "empregado_id")
	private Long idEmpregado;
	
	@Column(name = "cargo_id")
	private Long idCargo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Column(name = "funcao")
	private String funcao;
	
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "empregado_id", insertable = false, updatable = false)
    private Empregado empregado;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "cargo_id", insertable = false, updatable = false)
    private Cargo cargo;
	
	
	public Atividade() {
		super();
	}
	
	public String getDataInicioFormatada() {
		return dataInicio != null ? FormatacaoUtils.getDataString(dataInicio) : "";
	}
	
	public String getDataFimFormatada() {
		return dataFim != null ? FormatacaoUtils.getDataString(dataFim) : "";
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	
	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}

	public Empregado getEmpregado() {
		return empregado;
	}

	public void setEmpregado(Empregado empregado) {
		this.empregado = empregado;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getDataInicio(), getDataFim(), getFuncao(), getExcluido());
	}
}