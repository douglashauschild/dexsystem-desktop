package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.dexsystem.util.FormatacaoUtils;

@Entity
@Table(name = "pessoa_juridica")	
public class PessoaJuridica extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Column(name = "nome_fantasia")
	private String nomeFantasia;
	
	@Column(name = "cnpj")
	private String cnpj;
	
	@Column(name = "inscricao_estadual")
	private String inscricaoEstadual;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private Pessoa pessoa;
	
	public PessoaJuridica() {
		super();
	}
	
	public String getCnpjLimpo() {
		return cnpj != null ? FormatacaoUtils.removerFormatacao(cnpj) : "";
	}
	
	public String getCnpjFormatado() {
		String cnpjFormatado = ""; 
		try {
			cnpjFormatado = cnpj != null ? FormatacaoUtils.getCnpjFormatado(cnpj) : "";
		} catch(Exception e) {
			e.printStackTrace();
		}
		return cnpjFormatado;
	}
	
	public String getDataInicioFormatada() {
		return dataInicio != null ? FormatacaoUtils.getDataString(dataInicio) : "";
	}
	
	public String getDataFimFormatada() {
		return dataFim != null ? FormatacaoUtils.getDataString(dataFim) : "";
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getNomeFantasia(), getCnpj(), getInscricaoEstadual(), getDataInicio(), getDataFim(), getExcluido());
	}
}