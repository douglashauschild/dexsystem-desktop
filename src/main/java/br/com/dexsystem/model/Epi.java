package br.com.dexsystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.dexsystem.util.FormatacaoUtils;

@Entity
@Table(name = "epi")	
public class Epi extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "grade_id")
	private Long idGrade;
	
	@Column(name = "unidade_id")
	private Long idUnidade;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "ca")
	private String ca;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_validade_ca")
	private Date dataValidadeCa;

	@Column(name = "vida_util")
	private Integer vidaUtil;
	
	@Column(name = "vida_util_unidade")
	private Character vidaUtilUnidade;
	
	@Column(name = "descartavel")
	private Character descartavel;
	
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "grade_id", insertable = false, updatable = false)
    private Grade grade;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "unidade_id", insertable = false, updatable = false)
    private UnidadeMedida unidadeMedida;
	
	
	public Epi() {
		super();
	}
	
	
	public boolean isPossuiGrade() {
		return idGrade != null;
	}
	
	public boolean isDescartavel() {
		return descartavel != null && descartavel.equals('1');
	}
	
	public boolean isCaVencido() {
		Date dataAtual = new Date();
		return dataValidadeCa != null && (dataValidadeCa.before(dataAtual) || dataValidadeCa.equals(dataAtual));
	}
	
	public String getDataValidadeCaFormatada() {
		return dataValidadeCa != null ? FormatacaoUtils.getDataString(dataValidadeCa) : "";
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdGrade() {
		return idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getVidaUtil() {
		return vidaUtil;
	}

	public void setVidaUtil(Integer vidaUtil) {
		this.vidaUtil = vidaUtil;
	}

	public Character getVidaUtilUnidade() {
		return vidaUtilUnidade;
	}

	public void setVidaUtilUnidade(Character vidaUtilUnidade) {
		this.vidaUtilUnidade = vidaUtilUnidade;
	}

	public Character getDescartavel() {
		return descartavel;
	}

	public void setDescartavel(Character descartavel) {
		this.descartavel = descartavel;
	}

	public Date getDataValidadeCa() {
		return dataValidadeCa;
	}

	public void setDataValidadeCa(Date dataValidadeCa) {
		this.dataValidadeCa = dataValidadeCa;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getIdGrade(), getIdUnidade(), getNome(), getCa(), getDataValidadeCa(), getVidaUtil(), getVidaUtilUnidade(), getDescartavel(), getExcluido());
	}
}