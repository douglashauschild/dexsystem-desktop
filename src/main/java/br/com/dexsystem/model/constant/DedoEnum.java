package br.com.dexsystem.model.constant;

public enum DedoEnum {
    POLEGAR_DIREITO("D1", 1, "Polegar Direito"),
    INDICADOR_DIREITO("D2", 2, "Indicador Direito"),
    MEDIO_DIREITO("D3", 3, "M�dio Direito"),
    ANELAR_DIREITO("D4", 4, "Anelar Direito"),
    MINIMO_DIREITO("D5", 5, "M�nimo Direito"),
    POLEGAR_ESQUERDO("E1", 6, "Polegar Esquerdo"),
    INDICADOR_ESQUERDO("E2", 7, "Indicador Esquerdo"),
    MEDIO_ESQUERDO("E3", 8, "M�dio Esquerdo"),
    ANELAR_ESQUERDO("E4", 9, "Anelar Esquerdo"),
    MINIMO_ESQUERDO("E5", 10, "M�nimo Esquerdo");

    private String key;
    private int posicao;
    private String nome;
    

    DedoEnum(String key, Integer posicao, String nome) {
        this.nome = nome;
        this.posicao = posicao;
        this.key = key;
    }

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPosicao() {
		return posicao;
	}

	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}

	public static DedoEnum getByKey(String key) throws EnumConstantNotPresentException {
        for (DedoEnum dedo : DedoEnum.values()) {
            if (dedo.getKey().equals(key)) {
                return dedo;
            }
        }
        throw new EnumConstantNotPresentException(DedoEnum.class, "Chave " + key + " n�o encontrada");
    }
	
	public static DedoEnum getByPosicao(int posicao) throws EnumConstantNotPresentException {
        for (DedoEnum dedo : DedoEnum.values()) {
            if (dedo.getPosicao() == posicao) {
                return dedo;
            }
        }
        throw new EnumConstantNotPresentException(DedoEnum.class, "Chave " + posicao + " n�o encontrada");
    }
}
