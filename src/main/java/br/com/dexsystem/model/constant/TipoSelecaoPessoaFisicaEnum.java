package br.com.dexsystem.model.constant;


public enum TipoSelecaoPessoaFisicaEnum {
	NOME('S', "Nome"),
	CPF_NOME('C', "CPF | Nome");
	
	private Character key;
	private String descricao;

	private TipoSelecaoPessoaFisicaEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoSelecaoPessoaFisicaEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoSelecaoPessoaFisicaEnum tipo : TipoSelecaoPessoaFisicaEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoPessoaFisicaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoSelecaoPessoaFisicaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoSelecaoPessoaFisicaEnum tipo : TipoSelecaoPessoaFisicaEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoPessoaFisicaEnum.class, "Tipo " + descricao + " não encontrado");
	}
}