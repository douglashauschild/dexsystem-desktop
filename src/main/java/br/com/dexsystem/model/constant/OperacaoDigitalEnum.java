package br.com.dexsystem.model.constant;

public enum OperacaoDigitalEnum {
    CAPTURA('C', "Captura"),
    IDENTIFICACAO('I', "Identifica��o"),
    VERIFICACAO('V', "Verifica��o");

    private Character key;
    private String nome;
    

    OperacaoDigitalEnum(Character key, String nome) {
        this.nome = nome;
        this.key = key;
    }

	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public static OperacaoDigitalEnum getByKey(Character key) throws EnumConstantNotPresentException {
        for (OperacaoDigitalEnum tipo : OperacaoDigitalEnum.values()) {
            if (tipo.getKey().equals(key)) {
                return tipo;
            }
        }
        throw new EnumConstantNotPresentException(OperacaoDigitalEnum.class, "Chave " + key + " n�o encontrada");
    }
}
