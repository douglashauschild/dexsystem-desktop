package br.com.dexsystem.model.constant;


public enum TipoConfirmacaoEnum {
	SENHA('S', "Senha"),
	BIOMETRIA('B', "Biometria");

	private Character key;
	private String descricao;

	private TipoConfirmacaoEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoConfirmacaoEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoConfirmacaoEnum permissao : TipoConfirmacaoEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoConfirmacaoEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoConfirmacaoEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoConfirmacaoEnum permissao : TipoConfirmacaoEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoConfirmacaoEnum.class, "Tipo de empresa " + descricao + " não encontrada");
	}
}