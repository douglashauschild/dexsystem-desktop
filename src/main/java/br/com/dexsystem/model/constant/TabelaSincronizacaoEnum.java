package br.com.dexsystem.model.constant;

import br.com.dexsystem.model.Atividade;
import br.com.dexsystem.model.Cargo;
import br.com.dexsystem.model.CargoEpi;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.Empresa;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.Grade;
import br.com.dexsystem.model.GradeItem;
import br.com.dexsystem.model.Motivo;
import br.com.dexsystem.model.ParametroSistema;
import br.com.dexsystem.model.Pessoa;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.PessoaFisica;
import br.com.dexsystem.model.PessoaJuridica;
import br.com.dexsystem.model.Setor;
import br.com.dexsystem.model.UnidadeMedida;
import br.com.dexsystem.model.Usuario;

public enum TabelaSincronizacaoEnum {
	USUARIO("usuario", "Usuario", Usuario.class),
	MOTIVO("motivo", "Motivo", Motivo.class),
	UNIDADE_MEDIDA("unidade_medida", "Unidade de Medida", UnidadeMedida.class),
	GRADE("grade", "Grade", Grade.class),
	GRADE_ITEM("grade_item", "Tamanhos de Grade", GradeItem.class),
	EPI("epi", "EPI", Epi.class),
	PESSOA("pessoa", "Pessoa", Pessoa.class),
	PESSOA_JURIDICA("pessoa_juridica", "Pessoa Jur�dica", PessoaJuridica.class),
	EMPRESA("empresa", "Empresa", Empresa.class),
	SETOR("setor", "Setor", Setor.class),
	CARGO("cargo", "Cargo", Cargo.class),
	PESSOA_FISICA("pessoa_fisica", "Pessoa F�sica", PessoaFisica.class),
	CARGO_EPI("cargo_epi", "Cargo EPI", CargoEpi.class),
	EMPREGADO("empregado", "Empregado", Empregado.class),
	ATIVIDADE("atividade", "Atividade", Atividade.class),
	PARAMETRO_SISTEMA("parametro_sistema", "Par�metros do Sistema", ParametroSistema.class),
	EMPREGADO_EPI("empregado_epi", "Empregado EPI", EmpregadoEpi.class),
	PESSOA_DIGITAL("pessoa_digital", "Pessoa Digital", PessoaDigital.class);

	private String tabela;
	private String cadastro;
	private Class classe;

	private TabelaSincronizacaoEnum (String tabela, String cadastro, Class clazz) {
		this.tabela = tabela;
		this.cadastro = cadastro;
		this.classe = clazz;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public String getCadastro() {
		return cadastro;
	}

	public void setCadastro(String cadastro) {
		this.cadastro = cadastro;
	}

	public Class getClasse() {
		return classe;
	}

	public void setClasse(Class classe) {
		this.classe = classe;
	}

	public static TabelaSincronizacaoEnum getByTabela(String tabela) throws EnumConstantNotPresentException {
		for (TabelaSincronizacaoEnum table : TabelaSincronizacaoEnum.values()) {
			if (table.getTabela().equals(tabela)) {
				return table;
			}
		}
		throw new EnumConstantNotPresentException(TabelaSincronizacaoEnum.class, "Chave " + tabela + " n�o encontrada");
	}
}