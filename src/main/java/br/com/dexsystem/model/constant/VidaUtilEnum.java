package br.com.dexsystem.model.constant;


public enum VidaUtilEnum {
	SELECIONE(null, "-- Selecione --"),
	DIA('D', "Dia(s)"),
	MES('M', "Mês(es)"),
	ANO('A', "Ano(s)");

	
	private Character key;
	private String descricao;

	private VidaUtilEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static VidaUtilEnum getByKey(String key) throws EnumConstantNotPresentException {
		for (VidaUtilEnum permissao : VidaUtilEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(VidaUtilEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static VidaUtilEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (VidaUtilEnum permissao : VidaUtilEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(VidaUtilEnum.class, "Permissão " + descricao + " não encontrada");
	}
}