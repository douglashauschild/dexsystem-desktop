package br.com.dexsystem.util;

import java.util.Iterator;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.Manifest;

import br.com.dexsystem.Principal;

public class VersaoUtils {

	public static String getVersao() {
		try {
			Manifest manifest = new Manifest(Principal.class.getClassLoader().getResource("META-INF/MANIFEST.MF").openStream());
			
			String versao = "";
			String build = "";
			Attributes attributes = manifest.getMainAttributes();
			if (attributes != null){
			    Iterator it = attributes.keySet().iterator();
			    while (it.hasNext()){
			        Name key = (Name) it.next();
			        String keyword = key.toString();
			        if (keyword.equals("Implementation-Version")){
			        	versao = (String) attributes.get(key);
			        }
			        if (keyword.equals("Implementation-Build")){
			        	build = (String) attributes.get(key);
			        }
			    }
			}
			if (versao.isEmpty()) {
				versao = "1";
			}
			if (build.isEmpty()) {
				build = "0";
			}
			return versao+"."+build;
		} catch(Exception e) {
			return null;
		}
	}
}