package br.com.dexsystem.util;

import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.text.MaskFormatter;

public class FormatacaoUtils {
    
	
    public static String getDataHoraString(Date data) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(data);
    }
    
    public static String getDataString(Date data) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(data);
    }
    
    public static String getHoraString(Date data) {
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        return formatter.format(data);
    }
    
    public static String getDataSQL(Date data) throws Exception {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(data);
    }
    
    public static Date getData(String data) throws Exception {
        return getData(data, "dd/MM/yyyy");
    }
    
    public static String getDataChecksum(Date data) throws Exception {
    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(data);
    }
    
    public static String getDataArquivos(Date data) throws Exception {
    	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        return formatter.format(data);
    }
    
    public static Date getData(Date data) throws Exception {
        return getData(getDataString(data), "dd/MM/yyyy");
    }
    
    public static Date getDataHora(String data) throws Exception {
        return getData(data, "dd/MM/yyyy HH:mm:ss");
    }
        
    public static Date getData(String data, String formato) throws Exception {
        DateFormat formatter = new SimpleDateFormat(formato);
        return (data == null) ? null : formatter.parse(data);
    }
        
    public static String removerFormatacao(String dado) {
        String retorno = "";
        for (int i = 0; i < dado.length(); i++) {
            if (dado.charAt(i) != '.' && dado.charAt(i) != '/' && dado.charAt(i) != '-' && dado.charAt(i) != '(' && dado.charAt(i) != ')') {
                retorno = retorno + dado.charAt(i);
            }
        }
        return (retorno);
    }
      
	public static int diferencaDias(Date maior, Date menor) {
		if (maior == null || menor == null) {
			return 0;
		}
		Date maiorCorrigida = null;
		Date menorCorrigida = null;
		try {
			maiorCorrigida = getData(maior);
			menorCorrigida = getData(menor);
		} catch (Exception e) {
			maiorCorrigida = maior;
			menorCorrigida = menor;
			e.printStackTrace();
		}
		Calendar calMenor = Calendar.getInstance();
		calMenor.setTime(menorCorrigida);
		Calendar calMaior = Calendar.getInstance();
		calMaior.setTime(maiorCorrigida);
		long ini = calMenor.getTimeInMillis();
		long fim = calMaior.getTimeInMillis();
		long nroHoras = (fim - ini) / 1000 / 3600;
		int nroDias = (int) nroHoras / 24;
		return nroDias;
	}
		
	
	public static Date getPrimeiroDiaDoMes(int mes) {
		Calendar gc = Calendar.getInstance();
		gc.set(gc.get(Calendar.YEAR), mes, 01);
		gc.set(Calendar.DAY_OF_MONTH, 1);
		return gc.getTime();
	}
	
	public static Date getUltimoDiaDoMes(int mes) {
		Calendar gc = Calendar.getInstance();
		gc.set(gc.get(Calendar.YEAR), mes, 01);
		gc.set(Calendar.DAY_OF_MONTH, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
		return gc.getTime();
	}
	
	public static Date incluirDia(Date data, Integer dias) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		return (calendar.getTime());
	}
	
	public static Date incluirMes(Date data, Integer meses) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.MONTH, meses);
		return (calendar.getTime());
	}
	
	public static Date incluirAno(Date data, Integer anos) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(data);
		calendar.add(Calendar.YEAR, anos);
		return (calendar.getTime());
	}
	
	public static Integer getIdade(Date menorData, Date maiorData) {
		if (maiorData == null || menorData == null || maiorData.before(menorData))
			return null;

		int dias = diferencaDias(maiorData, menorData);
		Double anos = dias/365.25;
		return anos.intValue();
	}

	public static String getCnpjFormatado(String cnpj) throws Exception {
		return adicionarMascara(cnpj, "##.###.###/####-##");
	}
	
	public static String getCpfFormatado(String cpf) throws Exception {
		return adicionarMascara(cpf, "###.###.###-##");
	}
	
	public static String getRgFormatado(String rg) throws Exception {
        return adicionarMascara(rg, "##.###.###-#");
	}
	
	public static String adicionarMascara(String texto, String mascara) throws Exception {
		MaskFormatter mask = new MaskFormatter(mascara);
        mask.setValueContainsLiteralCharacters(false);
        return mask.valueToString(texto);
	}
	
	public static String ajustarBusca(String texto) {
		if (texto.length() == 0) {
        	return null;
        } else {
        	Map<String, String> replacements = new HashMap();
            replacements.put("a", "[a��]");
            replacements.put("e", "[e�]");
            replacements.put("i", "[i�]");
            replacements.put("o", "[o���]");
            replacements.put("u", "[u�]");
            String regex = "";
            for (char c : texto.toCharArray()) {
                String replacement = replacements.get(Normalizer.normalize(Character.toString(Character.toLowerCase(c)), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", ""));
                if (replacement == null) {
                    regex += c;
                } else {
                    regex += replacement;
                }
            }
            return regex;
        }
	}
	
	public static boolean isDataValida(String date) {
	    try {
	    	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    	df.setLenient(false);
	    	df.parse(date);
	        return true;
	    } catch (Exception e) {
	       return false;
	    } 
	}
	
	public static Long getMilisegundos(Long valor) {
		return valor * 60000;
	}
}
