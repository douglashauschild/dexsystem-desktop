package br.com.dexsystem.util;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

public class FileUtils {
	
    
    public static void verificarCriarDiretorio(String caminho) {
    	File diretorio = new File(caminho);
		if (!diretorio.exists()) {
		   diretorio.mkdirs();
		} 
    }
    
    
    public static void abrirPdf(String path) {
    	if (Desktop.isDesktopSupported() && path != null && !path.isEmpty()) {
		    try {
		        File myFile = new File(path);
		        Desktop.getDesktop().open(myFile);
		    } catch (IOException ex) {
		        // no application registered for PDFs
		    }
		}
    }
}
