package br.com.dexsystem.util;

import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import br.com.dexsystem.service.client.VerificacaoClientService;

public class InternetUtils {
	
	//Verifica se tem conex�o com a internet	
	public static boolean isConexaoInternet() {
		try {	  
			URL url = new URL("https://www.google.com.br");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			
		    SSLContext sslContext = SSLContext.getInstance(ConstantUtils.PROTOCOLO_TLSv1_2); 
			sslContext.init(null, new TrustManager[] { 
				new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {return null;}
					public void checkClientTrusted(X509Certificate[] certs, String authType) {}
					public void checkServerTrusted(X509Certificate[] certs, String authType) {}
				} 
			}, new SecureRandom());
			SSLContext.setDefault(sslContext);

			SSLSocketFactory sslF = (SSLSocketFactory) SSLSocketFactory.getDefault();
			con.setSSLSocketFactory(sslF);
			
			con.setUseCaches(false);
			con.setDoOutput(true);
			con.setDoInput(true);
			Object objData = con.getContent();
		} catch (UnknownHostException e) {
			LoggerUtils.logErro(e);
			return false;
		} catch (IOException e) {
			LoggerUtils.logErro(e);
			return false;
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			return false;
		}
		return true;
	}
	
	
	public static boolean isConexaoServico() {
		return new VerificacaoClientService().isConexaoServico();
	}
	
}
