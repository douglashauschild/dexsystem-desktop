package br.com.dexsystem.util;

import java.awt.Component;

import javax.swing.JOptionPane;

public class MensageiroUtils {
	
	
	public static void infoMsg(Component parentComponent, String mensagem) {
		mensagem(parentComponent, mensagem, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void warnMsg(Component parentComponent, String mensagem) {
		mensagem(parentComponent, mensagem, JOptionPane.WARNING_MESSAGE);
	}
	
	public static void errorMsg(Component parentComponent, String mensagem) {
		mensagem(parentComponent, mensagem, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void mensagem(Component parentComponent, String mensagem, int icone) {
		JOptionPane.showMessageDialog(parentComponent, mensagem, ConstantUtils.NOME_SISTEMA, icone);
	}
	
	public static int confirmMsg(Component parentComponent, String mensagem, Object[] opcoes, Object opcaoDefault) {
		return JOptionPane.showOptionDialog(parentComponent, mensagem, ConstantUtils.NOME_SISTEMA, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcaoDefault);
	}
}
