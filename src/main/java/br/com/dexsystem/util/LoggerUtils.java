package br.com.dexsystem.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerUtils {
	private static Logger logger = Logger.getLogger("br.com.dexsystem");
	private static FileHandler handler = null;
	private static SimpleFormatter formatter = new SimpleFormatter();
	
	public static Logger getLogger() {
		try {
			if (handler == null) {
				FileUtils.verificarCriarDiretorio(ConstantUtils.LOCAL_LOGS);
				handler = new FileHandler(ConstantUtils.LOCAL_LOGS + "log.txt", true);
				logger.addHandler(handler);
				handler.setFormatter(formatter);
			}
			return logger;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void logErro(Exception e) {
		StringWriter sw = new StringWriter();
	    PrintWriter pw = new PrintWriter(sw);
	    e.printStackTrace(pw);
	    String aux = sw.toString();

		LoggerUtils.getLogger().warning(aux);
	}
	
	public static void logText(String erro) {
		LoggerUtils.getLogger().info(erro);
	}
}
