package br.com.dexsystem.util;

import java.security.MessageDigest;

public class CriptografiaUtils {

	public static final String CHAVE = "31072020";

	private static String[] EncodeCharSet = { /*
											 * 00 01 02 03 04 05 06 07 08 09 0A
											 * 0B 0C 0D 0E 0F
											 */
	/* 00 */"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
	/* 10 */"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0",
	/* 20 */"0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0", "63", "0", "0",
	/* 30 */"01", "02", "03", "04", "05", "06", "07", "08", "09", "0", "0", "0", "0", "0", "0", "0",
	/* 40 */"0", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51",
	/* 50 */"52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "0", "0", "0", "0", "0",
	/* 60 */"0", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
	/* 70 */"26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "0 ", "0", "0", "0", "0" };

	
	private static int encodeChar(int chr) {
		return new Byte(EncodeCharSet[chr]);
	}

	private static String decode(String chave) {
		String resultado = "";
		while ((chave.length() % 4) != 0) {
			chave += (char) 0;
		}
		int posicao = 0;
		char[] charArray = chave.toCharArray();
		while (posicao + 3 <= chave.length()) {
			charArray[posicao + 0] = (char) encodeChar(charArray[posicao + 0]);
			charArray[posicao + 1] = (char) encodeChar(charArray[posicao + 1]);
			charArray[posicao + 2] = (char) encodeChar(charArray[posicao + 2]);
			charArray[posicao + 3] = (char) encodeChar(charArray[posicao + 3]);
			
			char[] tempCharArray = { '0', '0', '0', '0' };
			tempCharArray[1] = (char) (((int) (charArray[posicao]) & 0x3F) + (((int) (charArray[posicao + 1]) & 0x03) << 6));
			tempCharArray[2] = (char) ((((int) (charArray[posicao + 1]) & 0x3C) >> 2) + (((int) (charArray[posicao + 2]) & 0x0F) << 4));
			tempCharArray[3] = (char) ((((int) (charArray[posicao + 2]) & 0x30) >> 4) + (((int) (charArray[posicao + 3]) & 0x3F) << 2));

			resultado += "" + (char) tempCharArray[1] + "" + (char) tempCharArray[2] + "" + (char) tempCharArray[3];
			posicao = posicao + 4;
		}
		return (resultado);
	}


	public static String encrypt(String texto, String chave) {
		String novaChave = decode(chave);
		int posicaoChave = 0;
		String resultado = "Encr" + texto;
		char[] novaChaveCharArray = novaChave.toCharArray();
		char[] charArray = resultado.toCharArray();
		for (int x = 4; x < resultado.length(); x++) {
			charArray[x] = (char) ((int) (charArray[x]) ^ (int) (novaChaveCharArray[posicaoChave]));
			posicaoChave++;
			if (posicaoChave >= novaChave.length()) {
				posicaoChave = 0;
			}
		}
		resultado = new String(charArray);
		return (resultado);
	}

	
	public static String decrypt(String texto, String chave) {
		if (!texto.contains("Encr"))
			return texto;

		String novaChave = decode(chave);
		int posicaoChave = 0;
		char[] charArray = texto.substring(4, texto.length()).toCharArray();
		char[] novaChaveArray = novaChave.toCharArray();
		for (int x = 0; x < charArray.length; x++) {
			charArray[x] = (char) (((int) charArray[x]) ^ ((int) novaChaveArray[posicaoChave]));
			posicaoChave++;
			if (posicaoChave >= novaChave.length())
				posicaoChave = 0;
		}
		return new String(charArray);
	}
	
	
	public static String gerarHash(String texto) throws Exception {
		MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        byte[] md5hash;
        md.update(texto.getBytes("iso-8859-1"), 0, texto.length());
        md5hash = md.digest();
        return convertToHex(md5hash);
	}
	
    private static String convertToHex(byte[] data) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        }
        return buf.toString();
    } 	
}
