package br.com.dexsystem.util;

import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import br.com.dexsystem.model.Atividade;
import br.com.dexsystem.model.Cargo;
import br.com.dexsystem.model.CargoEpi;
import br.com.dexsystem.model.ControleSincronizacao;
import br.com.dexsystem.model.Dispositivo;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.Empresa;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.Grade;
import br.com.dexsystem.model.GradeItem;
import br.com.dexsystem.model.Motivo;
import br.com.dexsystem.model.ParametroSistema;
import br.com.dexsystem.model.Pessoa;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.PessoaFisica;
import br.com.dexsystem.model.PessoaJuridica;
import br.com.dexsystem.model.Setor;
import br.com.dexsystem.model.UnidadeMedida;
import br.com.dexsystem.model.Usuario;


public class HibernateUtil {

    public static SessionFactory sessionFactory;
    
    static {
        abrirConexao();
    }
        
    public static void abrirConexao() {
        try {            
            Configuration configuration = new Configuration();
            Properties properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("hibernate.properties"));
            
            String url = ConstantUtils.LOCAL_DB + "dexsystem";
            properties.setProperty("hibernate.connection.url", "jdbc:h2:file:" + url + ";AUTO_SERVER=TRUE;DEFRAG_ALWAYS=TRUE");
            configuration.setProperties(properties);
            
            //mapeando classes
            configuration.addAnnotatedClass(Dispositivo.class);
            configuration.addAnnotatedClass(ControleSincronizacao.class);
            configuration.addAnnotatedClass(Atividade.class);
            configuration.addAnnotatedClass(Cargo.class);
            configuration.addAnnotatedClass(CargoEpi.class);
            configuration.addAnnotatedClass(Empregado.class);
            configuration.addAnnotatedClass(EmpregadoEpi.class);
            configuration.addAnnotatedClass(Empresa.class);
            configuration.addAnnotatedClass(Epi.class);
            configuration.addAnnotatedClass(Grade.class);
            configuration.addAnnotatedClass(GradeItem.class);
            configuration.addAnnotatedClass(Motivo.class);
            configuration.addAnnotatedClass(ParametroSistema.class);
            configuration.addAnnotatedClass(Pessoa.class);
            configuration.addAnnotatedClass(PessoaDigital.class);
            configuration.addAnnotatedClass(PessoaFisica.class);
            configuration.addAnnotatedClass(PessoaJuridica.class);
            configuration.addAnnotatedClass(Setor.class);
            configuration.addAnnotatedClass(UnidadeMedida.class);
            configuration.addAnnotatedClass(Usuario.class);
            
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build(); 
            
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }
    
    public static void close() {
        if (getSession() != null && getSession().isOpen()) {
            getSession().close();
        }
    }
}