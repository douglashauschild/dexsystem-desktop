package br.com.dexsystem.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesUtils {
	
//	private static final String URL_PADRAO = "http://localhost:8080/dexSystem";
	private static final String URL_PADRAO = "http://ec2-18-219-236-41.us-east-2.compute.amazonaws.com:8080/dexSystem";
	private static final String TEMPO_SINCRONIZACAO = "5";
	
	public static Properties readProperties() throws Exception {
		return verificaProperties();
	}
	
	public static Properties verificaProperties() throws IOException {
		Properties properties = new Properties();
		try {
			FileInputStream fis = new FileInputStream(ConstantUtils.LOCAL_PROPERTIES);
			try {
				properties.load(fis);
				fis.close();
			} catch (IOException e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
			properties = verificaPropriedades(properties);
		} catch (FileNotFoundException e) {
			properties = criarArquivoProperties();
		}
		return properties;
	}
	
	
	public static Properties criarArquivoProperties() throws IOException {
		Properties prop = new Properties();
		OutputStream output = new FileOutputStream(ConstantUtils.LOCAL_PROPERTIES);
		
		prop = verificaPropriedades(prop);
		prop.store(output, null);
		
		if (output != null) {
			output.close();
		}
		return prop;
	}
	
	
	public static Properties verificaPropriedades(Properties properties) {
		if ((properties.getProperty("url")) == null) {
			properties.setProperty("url", URL_PADRAO);
		}
		if ((properties.getProperty("tempoSincronizacao")) == null) {
			properties.setProperty("tempoSincronizacao", TEMPO_SINCRONIZACAO);
		}
		return properties;
	}
	

	public static String getUrl() {
		String url = URL_PADRAO;
		try {
			Properties properties = PropertiesUtils.readProperties();
			url = properties.getProperty("url");
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			e.printStackTrace();
		}
		return url;
	}
	
	
	public static Long getTempoSincronizacao(boolean milisegundos) {
		String tempoSincronizacao = TEMPO_SINCRONIZACAO;
		try {
			Properties properties = PropertiesUtils.readProperties();
			tempoSincronizacao = properties.getProperty("tempoSincronizacao");
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			e.printStackTrace();
		}
		Long tempo = Long.valueOf(tempoSincronizacao);
		return milisegundos ? FormatacaoUtils.getMilisegundos(tempo) : tempo;
	}
	
	
	public static void gravarConfiguracoes(String url, String tempoSincronizacao) throws Exception {
		Properties properties = readProperties();
		FileOutputStream propertiesOut = new FileOutputStream(ConstantUtils.LOCAL_PROPERTIES);
		
		properties.setProperty("url", url);
		properties.setProperty("tempoSincronizacao", tempoSincronizacao);
		
		properties.store(propertiesOut, null);
		propertiesOut.close();
	}
	
}
