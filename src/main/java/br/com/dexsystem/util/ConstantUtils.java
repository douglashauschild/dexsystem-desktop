package br.com.dexsystem.util;

import java.io.File;

public class ConstantUtils {

	public static final String NOME_SISTEMA = "DEX System - M�dulo Desktop";
	public static final String ICONE_SISTEMA = "icones/logo-72x72.png";
	
	public static final String ICONE_CONEXAO_SUCESSO = "icones/conexao_sucesso.png";
	public static final String ICONE_CONEXAO_ERRO = "icones/conexao_erro.png";
	public static final String ICONE_ATUALIZAR = "icones/refresh.png";
	public static final String ICONE_SINCRONIZAR = "icones/sincronizar.png";
	public static final String ICONE_ENTREGA_EPI = "icones/entrega_epi.png";
	public static final String ICONE_FICHA_EPI = "icones/ficha_epi.png";
	
	public static final String ICONE_MAO_ESQUERDA = "icones/digitais/mao_esquerda.png";
	public static final String ICONE_MAO_DIREITA = "icones/digitais/mao_direita.png";
	public static final String ICONE_MAO_ESQUERDA_INDICATIVO = "icones/digitais/mao_esquerda_indicativo.png";
	public static final String ICONE_MAO_DIREITA_INDICATIVO = "icones/digitais/mao_direita_indicativo.png";
	
	public static final String ICONE_DIGITAL_CADASTRADA = "icones/digitais/digital_cadastrada.png";
	public static final String ICONE_DIGITAL_ANULADA = "icones/digitais/digital_anulada.png";
	
	public static final String LOCAL_DB = getDiretorioUsuario() + File.separator + "db" + File.separator;
	public static final String LOCAL_LOGS = getDiretorioUsuario() + File.separator + "logs" + File.separator;
	public static final String LOCAL_PROPERTIES = getDiretorioUsuario() + File.separator + "config.properties";
	public static final String LOCAL_CHAVES_ENCRIPTACAO = getDiretorioUsuario() + File.separator + "chaves" + File.separator;
	
	public static final String PROTOCOLO_TLSv1_2 = "TLSv1.2";
	
	
	public static String getDiretorioAplicacao() {
		return new File("").getAbsolutePath();
	}
	
	public static String getDiretorioUsuario() {
		return System.getProperty("user.home") + File.separator + "dexSystem";
	}
}
