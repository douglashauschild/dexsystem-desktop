package br.com.dexsystem.view;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import com.futronictech.AnsiSDKLib;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.constant.DedoEnum;
import br.com.dexsystem.model.constant.OperacaoDigitalEnum;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.DigitalVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.IconeDigitalVO;
import br.com.dexsystem.service.EmpregadoService;
import br.com.dexsystem.service.PessoaDigitalService;
import br.com.dexsystem.thread.CapturarThread;
import br.com.dexsystem.thread.ErroInfo;
import br.com.dexsystem.thread.IdentificarThread;
import br.com.dexsystem.thread.OperacaoListener;
import br.com.dexsystem.thread.OperacaoThread;
import br.com.dexsystem.thread.VerificarThread;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.MensageiroUtils;

public class Digital extends JDialog implements OperacaoListener {

	private static final long serialVersionUID = -3412310113850971838L;
	private JButton btnSortearDedo;
    private JLabel lblDigital;
    private JLabel lblImg;
    private JLabel lblMsg;
	
    private boolean retorno = false;
    private List<EmpregadoVO> empregadosRetorno;
    private EmpregadoVO empregadoVO;
    private EmpregadoService empregadoService;
    private PessoaDigitalService pessoaDigitalService;
    private List<PessoaDigital> digitaisCadastradas;
    
    //variaveis para biometria
  	public float[] matchScoreValue = new float[6];
  	private IconeDigitalVO imagemDigital;
  	private OperacaoThread operacaoThread = null;
    
  	public Digital(JFrame parent) {
        super(parent);
        initComponents();
        
        this.empregadoService = new EmpregadoService();
        this.pessoaDigitalService = new PessoaDigitalService();
    }
  	
    public Digital(Dialog parent, CredencialVO credencialVO) {
        super(parent);
        initComponents();
        
        this.empregadoService = new EmpregadoService();
        this.pessoaDigitalService = new PessoaDigitalService();
    }
    
    private void initComponents() {
        lblDigital = new JLabel();
        lblMsg = new JLabel();
        lblImg = new JLabel();
        btnSortearDedo = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Incluir EPI"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        lblDigital.setBackground(new java.awt.Color(0, 0, 0));

        lblMsg.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblMsg.setText("Por favor, coloque sua m�o no sensor.");

        btnSortearDedo.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnSortearDedo.setText("Sortear outro dedo");
        btnSortearDedo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	sortearDedo();
            }
        });
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnSortearDedo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(lblDigital, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblImg, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblMsg)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSortearDedo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblImg)
                    .addComponent(lblDigital, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    public boolean abrirCaptura(EmpregadoVO empregadoVO, String dedo) {
    	return abrirTela(null, empregadoVO, OperacaoDigitalEnum.CAPTURA.getKey(), dedo);
    }
    
    public List<EmpregadoVO> abrirIdentificacao(Long idEmpresa) {
    	abrirTela(idEmpresa, null, OperacaoDigitalEnum.IDENTIFICACAO.getKey(), null);
    	return empregadosRetorno;
    }
    
    public boolean abrirConfirmacao(EmpregadoVO empregadoVO) {
    	return abrirTela(null, empregadoVO, OperacaoDigitalEnum.VERIFICACAO.getKey(), null);
    }
    
    private boolean abrirTela(Long idEmpresa, EmpregadoVO empregadoVO, Character operacao, String dedo) {
		setModal(true);

		cancelarThread();
		carregarTela();
		
		if (empregadoVO != null) {
			this.empregadoVO = empregadoVO;
		}
		if (operacao.equals(OperacaoDigitalEnum.CAPTURA.getKey())) {
			boolean leitorConectado = carregarCaptura(dedo);
			if (!leitorConectado) {
				mensagemLeitor();
				return false;	
			}
			montarImagemIndicativa(operacao, dedo);
		} else if (operacao.equals(OperacaoDigitalEnum.IDENTIFICACAO.getKey())) {
			boolean leitorConectado = carregarIdentificacao(idEmpresa);
			if (!leitorConectado) {
				mensagemLeitor();
				return false;	
			}
			montarImagemIndicativa(operacao, dedo);
		} else if (operacao.equals(OperacaoDigitalEnum.VERIFICACAO.getKey())) {
			boolean leitorConectado = carregarVerificacao(empregadoVO);
			if (!leitorConectado) {
				mensagemLeitor();
				return false;	
			}
		}
		pack();
        setVisible(true);
        
        cancelarThread();
        this.pessoaDigitalService = null;
        this.empregadoService = null;
        this.digitaisCadastradas = null;
        
        return retorno;
    }
    
    
    private void cancelarThread() {
    	if (operacaoThread != null) {
        	operacaoThread.cancelar();
    	}
    }
    
    private void carregarTela() {
    	matchScoreValue[0] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_LOW;
        matchScoreValue[1] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_LOW_MEDIUM;
        matchScoreValue[2] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_MEDIUM;
        matchScoreValue[3] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_HIGH_MEDIUM;
        matchScoreValue[4] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_HIGH;
        matchScoreValue[5] = AnsiSDKLib.FTR_ANSISDK_MATCH_SCORE_VERY_HIGH;
        
    	imagemDigital = new IconeDigitalVO();
    	lblDigital.setIcon(imagemDigital);
    }
    
    
    private void montarImagemIndicativa(Character operacao, String dedo) {
    	lblImg.removeAll();
    	if (operacao.equals(OperacaoDigitalEnum.CAPTURA.getKey()) || operacao.equals(OperacaoDigitalEnum.VERIFICACAO.getKey())) {
	    	lblImg.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(isMaoDireita(dedo) ? ConstantUtils.ICONE_MAO_DIREITA_INDICATIVO : ConstantUtils.ICONE_MAO_ESQUERDA_INDICATIVO)));
	    	
	    	JLabel lblBorder = new JLabel();
	    	if (dedo.equals(DedoEnum.POLEGAR_DIREITO.getKey())) {
	    		lblBorder = getBordaDedo(3, 80, 30, 40);
	    	} else if (dedo.equals(DedoEnum.INDICADOR_DIREITO.getKey())) {
	    		lblBorder = getBordaDedo(35, 10, 30, 40);
	    	} else if (dedo.equals(DedoEnum.MEDIO_DIREITO.getKey())) {
	    		lblBorder = getBordaDedo(63, 0, 30, 40);
	    	} else if (dedo.equals(DedoEnum.ANELAR_DIREITO.getKey())) {
	    		lblBorder = getBordaDedo(91, 10, 30, 40);
	    	} else if (dedo.equals(DedoEnum.MINIMO_DIREITO.getKey())) {
	    		lblBorder = getBordaDedo(116, 43, 30, 40);
	    	} else if (dedo.equals(DedoEnum.POLEGAR_ESQUERDO.getKey())) {
	    		lblBorder = getBordaDedo(114, 80, 30, 40);
	    	} else if (dedo.equals(DedoEnum.INDICADOR_ESQUERDO.getKey())) {
	    		lblBorder = getBordaDedo(81, 10, 30, 40);
	    	} else if (dedo.equals(DedoEnum.MEDIO_ESQUERDO.getKey())) {
	    		lblBorder = getBordaDedo(52, 0, 30, 40);
	    	} else if (dedo.equals(DedoEnum.ANELAR_ESQUERDO.getKey())) {
	    		lblBorder = getBordaDedo(26, 10, 30, 40);
	    	} else if (dedo.equals(DedoEnum.MINIMO_ESQUERDO.getKey())) {
	    		lblBorder = getBordaDedo(0, 43, 30, 40);
	    	}
	    	lblImg.add(lblBorder);
    	} else if (operacao.equals(OperacaoDigitalEnum.IDENTIFICACAO.getKey())) {
    		lblImg.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_MAO_DIREITA_INDICATIVO)));
    		lblImg.add(getBordaDedo(3, 80, 30, 40));
	    	lblImg.add(getBordaDedo(33, 10, 30, 40));
	    	lblImg.add(getBordaDedo(63, 0, 30, 40));
	    	lblImg.add(getBordaDedo(93, 10, 30, 40));
	    	lblImg.add(getBordaDedo(116, 43, 30, 40));
    	}
    }
    
    private boolean isMaoDireita(String dedo) {
    	return dedo.equals(DedoEnum.POLEGAR_DIREITO.getKey()) || dedo.equals(DedoEnum.INDICADOR_DIREITO.getKey()) || dedo.equals(DedoEnum.MEDIO_DIREITO.getKey()) || dedo.equals(DedoEnum.ANELAR_DIREITO.getKey()) || dedo.equals(DedoEnum.MINIMO_DIREITO.getKey());
    }
    
    private boolean carregarCaptura(String dedo) {
    	btnSortearDedo.setVisible(false);
    	mensagemConsole("Coloque o dedo indicado no sensor.");
    	cancelarThread();
    	operacaoThread = new CapturarThread(this, (byte) DedoEnum.getByKey(dedo).getPosicao(), matchScoreValue[4]);
    	if (!operacaoThread.isLeitorConectado()) {
    		return false;
    	}
    	operacaoThread.start();
    	return true;
    }
    
    private boolean carregarIdentificacao(Long idEmpresa) {
    	btnSortearDedo.setVisible(false);
    	
    	List<DigitalVO> digitaisVO = new ArrayList<DigitalVO>();
    	List<Empregado> empregados = empregadoService.buscarAtivos(idEmpresa);
		for (Empregado empregado : empregados) {
			List<PessoaDigital> digitais = pessoaDigitalService.buscarPorIdPessoa(empregado.getIdPessoa());
			
			DigitalVO digitalVO = new DigitalVO();
			digitalVO.setIdEmpregado(empregado.getId());
			digitalVO.setDigitais(new ArrayList<byte[]>());
			
			for (PessoaDigital pessoaDigital : digitais) {
				digitalVO.getDigitais().add(pessoaDigital.getDigital());
			}
			digitaisVO.add(digitalVO);
		}
    	
    	mensagemConsole("Coloque um dos dedos no sensor.");
    	cancelarThread();
    	operacaoThread = new IdentificarThread(this, (byte) 0, digitaisVO, matchScoreValue[4]);
    	if (!operacaoThread.isLeitorConectado()) {
    		return false;
    	}
    	operacaoThread.start();
    	return true;
    }

    private boolean carregarVerificacao(EmpregadoVO empregadoVO) {
    	btnSortearDedo.setVisible(true);
    	digitaisCadastradas = pessoaDigitalService.buscarPorIdPessoa(empregadoVO.getIdPessoa());
		
    	List<byte[]> digitais = new ArrayList<byte[]>();
		for (PessoaDigital pessoaDigital : digitaisCadastradas) {
			digitais.add(pessoaDigital.getDigital());
		}
		mensagemConsole("Coloque o dedo indicado no sensor.");
		cancelarThread();
    	operacaoThread = new VerificarThread(this, (byte)0, digitais, matchScoreValue[4], getDedoSorteado());
    	if (!operacaoThread.isLeitorConectado()) {
    		return false;
    	}
    	operacaoThread.start();
    	return true;
    }
    
	@Override
	public void resultadoCaptura(byte[] dedo, int finger, int result, ErroInfo errInfo, String mensagem) {
		try {
			if (dedo != null) {
				Empregado empregado = empregadoService.buscarPorId(empregadoVO.getId());
				if (empregado != null) {
					PessoaDigital pessoaDigital = new PessoaDigital();
					pessoaDigital.setIdPessoa(empregado.getIdPessoa());
					pessoaDigital.setPosicao(DedoEnum.getByPosicao(finger).getKey());
					pessoaDigital.setDigital(dedo);
					pessoaDigitalService.gravar(pessoaDigital);
					retorno = true;
				}
			} else {
				if (mensagem != null && !mensagem.isEmpty()) {
					MensageiroUtils.errorMsg(this, mensagem);
				} else {
					MensageiroUtils.errorMsg(this, "Ocorreu um erro ao gravar a digital! Por favor, repita a a��o!");
				}
			}
			dispose();
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			e.printStackTrace();
		}	
	}
	
	@Override
	public void resultadoIdentificacao(List<DigitalVO> empregados, int result, ErroInfo errInfo, String message) {
		if (empregados != null && empregados.size() != 0L) {
			empregadosRetorno = empregadoService.buscarPosPesquisaDigital(empregados);
		} else {
			MensageiroUtils.warnMsg(this, "<html>Nenhum empregado encontrado!<br>Verifique se o filtro da empresa est� preenchido e tente novamente.</html>");
		}
		dispose();
	}
	
	@Override
	public void resultadoVerificacao(int result, ErroInfo errInfo, String mensagem) {
		try {
    		if (result == 1) {
    			retorno = true;
    			dispose();
    		} else {
    			mensagemConsole("<html><font size=3>Coloque o dedo indicado sobre o leitor para confirma��o!</font></html>");
    		}
    	} catch(Exception e) {
    		LoggerUtils.logErro(e);
			e.printStackTrace();
		}	
		
	}
	
	@Override
	public void resultadoImagem(BufferedImage imagem, byte[] imagemBuffer) {
		DataBuffer dataBuffer= imagem.getRaster().getDataBuffer();
        for (int i=0; i< dataBuffer.getSize(); i++) {
        	dataBuffer.setElem(i, imagemBuffer[i]);
        }
        if (imagemDigital == null) {
        	imagemDigital = new IconeDigitalVO();
        }
        imagemDigital.setImage(imagem);
        lblDigital.setIcon(imagemDigital);
        lblDigital.repaint();
	}

	@Override
	public void mensagemConsole(String messagem) {
		lblMsg.setText(messagem);
		lblMsg.setHorizontalAlignment(JLabel.LEFT);
	}
	
	private JLabel getBordaDedo(int x, int y, int width, int height) {
		JLabel lblBorder = new JLabel();
    	lblBorder.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
    	lblBorder.setBounds(x, y, width, height);
    	return lblBorder;
	}
	
	private void sortearDedo() {
		if (operacaoThread != null && operacaoThread instanceof VerificarThread) {
			((VerificarThread) operacaoThread).setDedo(getDedoSorteado());
		}
    }
	
	private int getDedoSorteado() {
    	Integer dedo = null;
    	
    	if (dedo == null) {
    		int total = digitaisCadastradas.size();

        	Random random = new Random();  
        	dedo = random.nextInt(total);
        	mensagemConsole("Coloque o dedo indicado no sensor.");
        	montarImagemIndicativa(OperacaoDigitalEnum.VERIFICACAO.getKey(), digitaisCadastradas.get(dedo).getPosicao());
    	}
    	return dedo;
    }
	
	private void mensagemLeitor() {
		MensageiroUtils.warnMsg(this, "Leitor n�o identificado! Verifique se o leitor digital est� conectado ao computador e tente novamente.");
	}
}
