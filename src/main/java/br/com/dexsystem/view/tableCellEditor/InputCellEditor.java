package br.com.dexsystem.view.tableCellEditor;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

public class InputCellEditor extends AbstractCellEditor implements TableCellEditor {
	
	private static final long serialVersionUID = 9025328331461134317L;
	JComponent component = new JTextField();

	@Override
	public Object getCellEditorValue() {
		 return ((JTextField) component).getText();
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		JTextField textField = ((JTextField) component); 
		textField.setText(value.toString());
		textField.setFont(new Font("Tahoma", 0, 14));
		textField.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
					textField.getToolkit().beep();
					e.consume();
				}
			}
		});
		return component;
	}

}