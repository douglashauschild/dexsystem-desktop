package br.com.dexsystem.view.tableCellEditor;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.dexsystem.model.vo.SelectVO;
import br.com.dexsystem.service.GradeItemService;
import br.com.dexsystem.view.tableModel.EntregaEpiTableModel;

public class TamanhoComboBoxCellEditor extends ComboBoxCellEditor {
	
	private static final long serialVersionUID = 1212762840013144554L;
	private GradeItemService gradeItemService;

	
	public TamanhoComboBoxCellEditor() {
		super();
		this.gradeItemService = new GradeItemService();
	}


	@Autowired
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		Long idGrade = ((EntregaEpiTableModel) table.getModel()).getDados().get(row).getIdGrade();
		 
		List<SelectVO> tamanhosCombo = new ArrayList<SelectVO>();
		if (idGrade != null) {
	    	try {
	    		tamanhosCombo = gradeItemService.getGradeItensCombo(idGrade);
	    	} catch(Exception e) {
	    		e.printStackTrace();
	    	}
	    	super.setLista(tamanhosCombo);
	    	return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		} 
		return null;
    }
     
}