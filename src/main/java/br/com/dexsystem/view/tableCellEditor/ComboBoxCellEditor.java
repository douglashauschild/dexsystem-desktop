package br.com.dexsystem.view.tableCellEditor;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import br.com.dexsystem.model.vo.SelectVO;

public class ComboBoxCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
	
	private static final long serialVersionUID = -5022860575412959793L;
	
	private SelectVO selectVO;
    private List<SelectVO> lista;
    
    
    public ComboBoxCellEditor() {
    }
    
    public ComboBoxCellEditor(List<SelectVO> lista) {
        this.lista = lista;
    }
     
    @Override
    public Object getCellEditorValue() {
        return this.selectVO;
    }
	

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof SelectVO) {
            this.selectVO = (SelectVO) value;
        }
         
        JComboBox<SelectVO> comboSelectVO = new JComboBox<SelectVO>();
        comboSelectVO.setFont(new java.awt.Font("Tahoma", 0, 14));
        for (SelectVO selectVO : lista) {
            comboSelectVO.addItem(selectVO);
        }
         
        comboSelectVO.setSelectedItem(selectVO);
        comboSelectVO.addActionListener(this);
         
        if (isSelected) {
            comboSelectVO.setBackground(table.getSelectionBackground());
        } else {
            comboSelectVO.setBackground(table.getSelectionForeground());
        }
         
        return comboSelectVO;
    }
 
    @Override
    public void actionPerformed(ActionEvent event) {
        JComboBox<SelectVO> comboSelectVO = (JComboBox<SelectVO>) event.getSource();
        this.selectVO = (SelectVO) comboSelectVO.getSelectedItem();
    }

	public void setLista(List<SelectVO> lista) {
		this.lista = lista;
	}
}