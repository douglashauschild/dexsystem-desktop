package br.com.dexsystem.view.pdfView;

import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.vo.FichaEpiVO;
import br.com.dexsystem.service.ParametroSistemaService;
import br.com.dexsystem.util.FormatacaoUtils;

public class FichaEpiView {
	
	private ParametroSistemaService parametroSistemaService;
	

    public FichaEpiView() {
		super();
		this.parametroSistemaService =  new ParametroSistemaService();
	}

	public String gerarFichaEpi(FichaEpiVO fichaEpiVO, String diretorio) throws Exception {
		Document document = new Document(PageSize.A4.rotate(), 36, 36, 54, 36);
    	
    	String arquivo = diretorio + "/Ficha_EPI_" + fichaEpiVO.getEmpregado() +"_"+FormatacaoUtils.getDataArquivos(new Date())+".pdf";
    	
    	PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(arquivo));
    	HeaderFooterPageEvent event = new HeaderFooterPageEvent(fichaEpiVO.getUsuario());
 		writer.setPageEvent(event);
 		
    	document.open();
    	
		document.addTitle("Ficha de Entrega de EPI - " + fichaEpiVO.getEmpregado());
		document.addCreationDate();
		
		boolean informarMotivo = parametroSistemaService.buscar().isInformarMotivoEntrega();
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100.0f);
    	table.setWidths(new float[] { 7, 3 });
        table.setSpacingBefore(10);
		
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(5);

	    cell.setPhrase(new Phrase("FICHA DE ENTREGA DE EPI"));
	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	    cell.setColspan(2);
	    table.addCell(cell);

	    cell = new PdfPCell();
		cell.setBackgroundColor(BaseColor.WHITE);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell.setPadding(5);
		
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setSize(10);
		
		cell.setColspan(2);
		cell.setPhrase(new Phrase("EMPRESA: " + fichaEpiVO.getEmpresa(), font));
		table.addCell(cell);
		
		cell.setColspan(1);
		cell.setPhrase(new Phrase("EMPREGADO: " + fichaEpiVO.getEmpregado(), font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("MATR�CULA: " + getValidarInformacao(fichaEpiVO.getMatricula()), font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("SETOR ATUAL: " + getValidarInformacao(fichaEpiVO.getSetor()), font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("DATA ADMISS�O: " + fichaEpiVO.getDataAdmissao(), font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("CARGO ATUAL: " + getValidarInformacao(fichaEpiVO.getCargo()), font));
		table.addCell(cell);
		
		cell.setPhrase(new Phrase("DATA DEMISS�O: " + fichaEpiVO.getDataDemissao(), font));
		table.addCell(cell);
		
		document.add(table);
		
		
		String texto = parametroSistemaService.buscar().getTextoFichaEpi();
		if (texto != null && !texto.isEmpty()) {
			font.setSize(11);
			Paragraph termo = new Paragraph("\n" + getSubstituirTags(fichaEpiVO, texto) + "\n\n", font);
			termo.setAlignment(Element.ALIGN_JUSTIFIED);
			termo.setIndentationLeft(5);
			termo.setIndentationRight(5);
	        document.add(termo);
		}
		
		font.setSize(10);
		table = new PdfPTable(informarMotivo ? 6 : 5);
        table.setWidthPercentage(100.0f);
    	table.setWidths(informarMotivo ? new float[] { 4, 1, (float) 0.6, (float) 1.5, 3, 2 } : new float[] { 6, 1, (float) 0.6, (float) 1.5, (float) 2.5 });
        table.setSpacingBefore(10);		
		
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cell.setColspan(1);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setPhrase(new Phrase("NOME", font));
		table.addCell(cell);
		
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPhrase(new Phrase("CA", font));
		table.addCell(cell);
		
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPhrase(new Phrase("QTD", font));
		table.addCell(cell);
		
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPhrase(new Phrase("DATA ENTREGA", font));
		table.addCell(cell);

		if (informarMotivo) {
    		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		cell.setPhrase(new Phrase("MOTIVO", font));
    		table.addCell(cell);
		}
		
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPhrase(new Phrase("DATA CONFIRMA��O", font));
		table.addCell(cell);

		table.setHeaderRows(1);
		
		for (EmpregadoEpi empregadoEpi : fichaEpiVO.getEpis()) {
    		cell.setBackgroundColor(BaseColor.WHITE);
    		cell.setColspan(1);
    		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		cell.setPhrase(new Phrase(empregadoEpi.getEpi().getNome(), font));
    		table.addCell(cell);
    		
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		cell.setPhrase(new Phrase(empregadoEpi.getEpi().getCa(), font));
    		table.addCell(cell);
    		
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		cell.setPhrase(new Phrase(empregadoEpi.getQuantidade().toString(), font));
    		table.addCell(cell);
    		
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		cell.setPhrase(new Phrase(FormatacaoUtils.getDataString(empregadoEpi.getDataEntrega()), font));
    		table.addCell(cell);
    	
    		if (informarMotivo) {
        		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        		cell.setPhrase(new Phrase(empregadoEpi.getMotivo() != null ? empregadoEpi.getMotivo().getDescricao() : "", font));
        		table.addCell(cell);
    		}
    		
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		cell.setPhrase(new Phrase(FormatacaoUtils.getDataHoraString(empregadoEpi.getDataConfirmacao()), font));
    		table.addCell(cell);
		}
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cell.setColspan(informarMotivo ? 6 : 5);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setPhrase(new Phrase("TOTAL: " + fichaEpiVO.getEpis().size(), font));
		table.addCell(cell);
        document.add(table);
        
        //add campo pra assinatura
        Paragraph titulo = new Paragraph("\n\n\n\n\n___________________________________________________");
		titulo.setAlignment(Element.ALIGN_CENTER);
        document.add(titulo);
        
        titulo = new Paragraph("ASSINATURA DO EMPREGADO");
		titulo.setAlignment(Element.ALIGN_CENTER);
        document.add(titulo);
        
        document.close();
        return arquivo;
    }
	
	private String getValidarInformacao(String texto) {
		return texto != null && !texto.isEmpty() && !texto.equals("Empregado sem atividade cadastrada") && !texto.equals("N�o informado") ? texto : "";
	}
	
	private String getSubstituirTags(FichaEpiVO fichaEpiVO, String texto) {
		if (texto.contains("{NOME_EMPRESA}")) {
			texto = texto.replace("{NOME_EMPRESA}", fichaEpiVO.getEmpresa());
		}
		if (texto.contains("{NOME_EMPREGADO}")) {
			texto = texto.replace("{NOME_EMPREGADO}", fichaEpiVO.getEmpregado());
		}
		if (texto.contains("{CPF_EMPREGADO}")) {
			texto = texto.replace("{CPF_EMPREGADO}", fichaEpiVO.getCpf());
		}
		if (texto.contains("{RG_EMPREGADO}")) {
			texto = texto.replace("{RG_EMPREGADO}", fichaEpiVO.getRg());
		}
		if (texto.contains("{MATRICULA_EMPREGADO}")) {
			texto = texto.replace("{MATRICULA_EMPREGADO}", fichaEpiVO.getMatricula());
		}
		if (texto.contains("{DATA_ATUAL}")) {
			texto = texto.replace("{DATA_ATUAL}", FormatacaoUtils.getDataString(new Date()));
		}
		return texto;
	}
}
