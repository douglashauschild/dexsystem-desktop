package br.com.dexsystem.view;

import java.awt.Image;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.Dispositivo;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.Usuario;
import br.com.dexsystem.model.constant.DedoEnum;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.SelectVO;
import br.com.dexsystem.service.DispositivoService;
import br.com.dexsystem.service.EmpregadoService;
import br.com.dexsystem.service.EmpresaService;
import br.com.dexsystem.service.FichaEpiService;
import br.com.dexsystem.service.PessoaDigitalService;
import br.com.dexsystem.service.UsuarioService;
import br.com.dexsystem.service.sincronizacao.Sincronizacao;
import br.com.dexsystem.thread.ConexaoThread;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.FormatacaoUtils;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.util.VersaoUtils;
import br.com.dexsystem.view.tableCellRender.EmpregadoTableCellRenderer;
import br.com.dexsystem.view.tableModel.EmpregadoTableModel;

public class Dashboard extends JFrame {
	
	private static final long serialVersionUID = -5892229797552008206L;
	
	private JButton btnSincronizar;
	private JButton btnEntregar;
	private JButton btnFichaEpi;
    private JButton btnPesquisarPelaDigital;
    private JComboBox<SelectVO> comboEmpresa;
    private JScrollPane jScrollPane1;
    private JToolBar jToolBar;
    private JLabel lblDataSincronizacao;
    private JLabel lblEmpresa;
    private JLabel lblPesquisar;
    private JLabel lblUsuario;
    private JPanel panelEmpregados;
    private JPanel panelInfo;
    private JPanel panelPrincipal;
    private JPanel tabDigital;
    private JPanel tabInfo;
    private JTable tabelaEmpregados;
    private JTabbedPane tabs;
    private JTextField txtPesquisar;
    private JLabel lblCargoEmpregado;
    private JLabel lblCargoTitle;
    private JLabel lblEmpresaEmpregado;
    private JLabel lblEmpresaTitle;
    private JLabel lblIdadeEmpregado;
    private JLabel lblIdadeTitle;
    private JLabel lblMatriculaEmpregado;
    private JLabel lblMatriculaTitle;
    private JLabel lblNomeEmpregado;
    private JLabel lblNomeTitle;
    private JLabel lblSetorEmpregado;
    private JLabel lblSetorTitle;
    private JLabel lblD1;
    private JLabel lblD2;
    private JLabel lblD3;
    private JLabel lblD4;
    private JLabel lblD5;
    private JLabel lblE1;
    private JLabel lblE2;
    private JLabel lblE3;
    private JLabel lblE4;
    private JLabel lblE5;
    private JLabel lblMaoDireita;
    private JLabel lblMaoEsquerda;
    private JLabel lblMsgCadastroDigital;
    private JButton btnAtualizar;
    
	private CredencialVO credencialVO;
	private Usuario usuarioLogado;
	private EmpregadoVO empregadoSelecionado;
	private EmpregadoService empregadoService;
	private PessoaDigitalService pessoaDigitalService;
	private FichaEpiService fichaEpiService;
	
	
	public Dashboard(CredencialVO credencialVO) {
		this.credencialVO = credencialVO ;
        initComponents();
        
        this.empregadoService = new EmpregadoService();
        this.pessoaDigitalService = new PessoaDigitalService();
        this.fichaEpiService = new FichaEpiService(this);
    }
	
	
	private void initComponents() {
		panelPrincipal = new JPanel();
        lblDataSincronizacao = new JLabel();
        lblUsuario = new JLabel();
        jToolBar = new JToolBar();
        btnSincronizar = new JButton();
        btnEntregar = new JButton();
        btnFichaEpi = new JButton();
        panelEmpregados = new JPanel();
        jScrollPane1 = new JScrollPane();
        tabelaEmpregados = new JTable();
        comboEmpresa = new JComboBox<>();
        lblEmpresa = new JLabel();
        txtPesquisar = new JTextField();
        lblPesquisar = new JLabel();
        panelInfo = new JPanel();
        tabs = new JTabbedPane();
        tabInfo = new JPanel();
        tabDigital = new JPanel();
        lblEmpresaTitle = new JLabel();
        lblNomeTitle = new JLabel();
        lblIdadeTitle = new JLabel();
        lblMatriculaTitle = new JLabel();
        lblSetorTitle = new JLabel();
        lblCargoTitle = new JLabel();
        lblEmpresaEmpregado = new JLabel();
        lblNomeEmpregado = new JLabel();
        lblMatriculaEmpregado = new JLabel();
        lblIdadeEmpregado = new JLabel();
        lblSetorEmpregado = new JLabel();
        lblCargoEmpregado = new JLabel();
        btnPesquisarPelaDigital = new JButton();
        lblD1 = new JLabel();
        lblD4 = new JLabel();
        lblD2 = new JLabel();
        lblD3 = new JLabel();
        lblD5 = new JLabel();
        lblE1 = new JLabel();
        lblE2 = new JLabel();
        lblE3 = new JLabel();
        lblE4 = new JLabel();
        lblE5 = new JLabel();
        lblMaoDireita = new JLabel();
        lblMaoEsquerda = new JLabel();
        lblMsgCadastroDigital = new JLabel();
        btnAtualizar = new JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent e) {
        		fecharAplicacao();
        	}
        });
        
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Login"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        lblUsuario.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblUsuario.setText("Usu�rio: ");
        
        lblDataSincronizacao.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblDataSincronizacao.setHorizontalAlignment(SwingConstants.RIGHT);
        
        lblEmpresaTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblEmpresaTitle.setText("Empresa");

        lblNomeTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblNomeTitle.setText("Nome");

        lblIdadeTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblIdadeTitle.setText("Idade");

        lblMatriculaTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblMatriculaTitle.setText("Matr�cula");

        lblSetorTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblSetorTitle.setText("Setor");

        lblCargoTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblCargoTitle.setText("Cargo");

        lblEmpresaEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblEmpresaEmpregado.setText("N�o informado");

        lblNomeEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblNomeEmpregado.setText("N�o informado");

        lblMatriculaEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblMatriculaEmpregado.setText("N�o informado");

        lblIdadeEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblIdadeEmpregado.setText("N�o informado");

        lblSetorEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblSetorEmpregado.setText("N�o informado");

        lblCargoEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblCargoEmpregado.setText("N�o informado");
        
        comboEmpresa.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        comboEmpresa.setModel(new javax.swing.DefaultComboBoxModel<>());
        comboEmpresa.addItem(new SelectVO(null, "-- Todas --"));
        comboEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	filtrarRegistros();
            }
        });

        lblEmpresa.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblEmpresa.setText("Empresa:");

        txtPesquisar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        txtPesquisar.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				filtrarPesquisa(evt);
			}
		});

        lblPesquisar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblPesquisar.setText("Pesquisar:");
        
        btnPesquisarPelaDigital.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnPesquisarPelaDigital.setText("Pesquisar pela Digital");
        btnPesquisarPelaDigital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pesquisarPelaDigital();
            }
        });
        
        btnAtualizar.setToolTipText("Atualizar tabela de empregados");
        btnAtualizar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnAtualizar.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_ATUALIZAR))); 
        btnAtualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popularTabela();
            }
        });
        
        carregarDigitais();

        carregarTabela();

        panelPrincipal.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(lblUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(159, 159, 159)
                .addComponent(lblDataSincronizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 497, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblDataSincronizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jToolBar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jToolBar.setFloatable(false);

        btnSincronizar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnSincronizar.setText("Sincronizar");
        btnSincronizar.setFocusable(false);
        btnSincronizar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSincronizar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSincronizar.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SINCRONIZAR)));
        btnSincronizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	forcarSincronizacao();
            }
        });
        jToolBar.add(Box.createHorizontalStrut(10));
        jToolBar.add(btnSincronizar);
        jToolBar.add(Box.createHorizontalStrut(80));
        
        btnEntregar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnEntregar.setText("Entregar EPIs");
        btnEntregar.setFocusable(false);
        btnEntregar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEntregar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnEntregar.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_ENTREGA_EPI)));
        btnEntregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entregar();
            }
        });
        jToolBar.add(btnEntregar);
        jToolBar.add(Box.createHorizontalStrut(10));
        
        btnFichaEpi.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnFichaEpi.setText("Ficha de EPI");
        btnFichaEpi.setFocusable(false);
        btnFichaEpi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFichaEpi.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFichaEpi.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_FICHA_EPI)));
        btnFichaEpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gerarFichaEpi();
            }
        });
        jToolBar.add(btnFichaEpi);

		jScrollPane1.setViewportView(tabelaEmpregados);

		javax.swing.GroupLayout panelEmpregadosLayout = new javax.swing.GroupLayout(panelEmpregados);
        panelEmpregados.setLayout(panelEmpregadosLayout);
        panelEmpregadosLayout.setHorizontalGroup(
            panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEmpregadosLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEmpregadosLayout.createSequentialGroup()
                        .addGroup(panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblEmpresa)
                            .addComponent(lblPesquisar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboEmpresa, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panelEmpregadosLayout.createSequentialGroup()
                                .addComponent(txtPesquisar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPesquisarPelaDigital)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
        panelEmpregadosLayout.setVerticalGroup(
            panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEmpregadosLayout.createSequentialGroup()
                .addGroup(panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEmpresa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEmpregadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPesquisarPelaDigital, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1))
        );

        tabs.setFont(new java.awt.Font("Tahoma", 0, 14)); 

        javax.swing.GroupLayout tabInfoLayout = new javax.swing.GroupLayout(tabInfo);
        tabInfo.setLayout(tabInfoLayout);
        tabInfoLayout.setHorizontalGroup(
            tabInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabInfoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(tabInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tabInfoLayout.createSequentialGroup()
                        .addComponent(lblCargoEmpregado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(20, 20, 20))
                    .addComponent(lblSetorEmpregado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(tabInfoLayout.createSequentialGroup()
                        .addComponent(lblIdadeEmpregado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(tabInfoLayout.createSequentialGroup()
                        .addGroup(tabInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEmpresaEmpregado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMatriculaEmpregado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblNomeEmpregado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(tabInfoLayout.createSequentialGroup()
                                .addGroup(tabInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblCargoTitle)
                                    .addComponent(lblSetorTitle)
                                    .addComponent(lblIdadeTitle)
                                    .addComponent(lblNomeTitle)
                                    .addComponent(lblEmpresaTitle)
                                    .addComponent(lblMatriculaTitle))
                                .addGap(0, 412, Short.MAX_VALUE)))
                        .addGap(20, 20, 20))))
        );
        tabInfoLayout.setVerticalGroup(
            tabInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tabInfoLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblEmpresaTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEmpresaEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblNomeTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNomeEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblIdadeTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblIdadeEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblMatriculaTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMatriculaEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSetorTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSetorEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCargoTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCargoEmpregado)
                .addContainerGap(197, Short.MAX_VALUE))
        );

        tabs.addTab("Cadastro", tabInfo);

        javax.swing.GroupLayout tabDigitalLayout = new javax.swing.GroupLayout(tabDigital);
        tabDigital.setLayout(tabDigitalLayout);
        tabDigitalLayout.setHorizontalGroup(
            tabDigitalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 514, Short.MAX_VALUE)
        );
        tabDigitalLayout.setVerticalGroup(
            tabDigitalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 488, Short.MAX_VALUE)
        );

        tabs.addTab("Digitais", tabDigital);

        javax.swing.GroupLayout panelInfoLayout = new javax.swing.GroupLayout(panelInfo);
        panelInfo.setLayout(panelInfoLayout);
        panelInfoLayout.setHorizontalGroup(
            panelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelInfoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(tabs, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelInfoLayout.setVerticalGroup(
            panelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabs)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(panelEmpregados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(panelInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelEmpregados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20)
                .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
	
	public void abrirTela() {
		lblDataSincronizacao.setText("Vers�o: " + VersaoUtils.getVersao());
        setVisible(true);
    }
	
	public void carregarInformacoes() {
		UsuarioService usuarioService = new UsuarioService();
		usuarioLogado = usuarioService.getUsuario(credencialVO.getEmail());
		usuarioService = null;
		
		lblUsuario.setText("Usu�rio: " + usuarioLogado.getNome());
		
		popularComboEmpresas();
		popularTabela();
	}
	
	public void atualizarDataSincronizacao() {
		DispositivoService dispositivoService = new DispositivoService();
		Dispositivo dispositivo = dispositivoService.geDispositivo();
		if (dispositivo != null) {
			lblDataSincronizacao.setText("�ltima Sincroniza��o: " + dispositivo.getDataSincronizacaoFormatada() + "   |   Vers�o: " + VersaoUtils.getVersao());
		}
		dispositivo = null;
		dispositivoService = null;
		repaint();
	}
	
	private void entregar() {
		EmpregadoVO empregado = getEmpregadoSelecionado();
		if (empregado == null) {
			MensageiroUtils.warnMsg(this, "Selecione primeiro um empregado na tabela!");
		} else if ((empregado.getSenha() == null || empregado.getSenha().isEmpty()) && !pessoaDigitalService.isPossuiDigital(empregado.getIdPessoa())) {
			String opcoes[] = { "Cadastrar digitais", "Cancelar entrega"};
			String msg = "<html>Este empregado n�o possui senha nem digitais cadastradas para confirmar o recebimento de EPIs!<br><br><strong>Observa��o:</strong><br>A senha s� pode ser cadastrada atrav�s do m�dulo online!<br><br></html>";
			int confirmacao = MensageiroUtils.confirmMsg(this, msg, opcoes, opcoes[0]);
			if (confirmacao == 0) {
				tabs.setSelectedIndex(1);
			}
		} else {
			Entrega entrega = new Entrega(this, credencialVO);
			entrega.abrirTela(empregadoSelecionado);
			entrega = null;
		}
	}
	
	private void gerarFichaEpi() {
		EmpregadoVO empregado = getEmpregadoSelecionado();
		if (empregado == null) {
			MensageiroUtils.warnMsg(this, "Selecione primeiro um empregado na tabela!");
		} else {
			fichaEpiService.gerarFichaEpi(empregadoSelecionado, credencialVO.getNome());
		}
	}
	
	private void forcarSincronizacao() {
		if (Sincronizacao.sincronizando) {
			MensageiroUtils.warnMsg(this, "Aguarde o fim da sincroniza��o e repita a a��o!");
		} else if (!ConexaoThread.temConexao) {
			MensageiroUtils.warnMsg(this, "� preciso ter conex�o com a internet para sincronizar!");
		} else {
			Sincronizacao sincronizacao = new Sincronizacao(this, true);
			sincronizacao.sincronizar(credencialVO.getToken());
			sincronizacao = null;
		}
	}
	
	private void popularComboEmpresas() {
		comboEmpresa.removeAllItems();
		
		EmpresaService empresaService = new EmpresaService();
		List<SelectVO> empresas = empresaService.getEmpresasCombo();
		comboEmpresa.addItem(new SelectVO(null, "-- Todas --"));
		for (SelectVO empresa : empresas) {
			comboEmpresa.addItem(empresa);
		}
		empresaService = null;
	}
	
	public void popularTabela() {
		EmpregadoService empregadoService = new EmpregadoService();
		Long idEmpresa = getIdEmpresaSelecionada();
		List<EmpregadoVO> empregados = empregadoService.buscarParaTabela(idEmpresa);
		popularTabela(empregados);
		empregadoService = null;
	}
	
	private void popularTabela(List<EmpregadoVO> empregados) {
    	((EmpregadoTableModel) tabelaEmpregados.getModel()).setDados(empregados);
    	ajustarColunasEpis(tabelaEmpregados);
		selecionarPrimeiraLinha();
	}
	
	private void ajustarColunasEpis(JTable tabela) {
		TableColumnModel tableColumn = tabela.getColumnModel(); 
		tableColumn.getColumn(0).setHeaderRenderer(new EmpregadoTableCellRenderer(EmpregadoTableCellRenderer.TIPO_HEADER));
		tableColumn.getColumn(1).setHeaderRenderer(new EmpregadoTableCellRenderer(EmpregadoTableCellRenderer.TIPO_HEADER));
		
		tableColumn.getColumn(0).setCellRenderer(new EmpregadoTableCellRenderer(EmpregadoTableCellRenderer.TIPO_COLUMN));
		tableColumn.getColumn(0).setMinWidth(150);
		tableColumn.getColumn(0).setMaxWidth(200);
		
		tableColumn.getColumn(1).setCellRenderer(new EmpregadoTableCellRenderer(EmpregadoTableCellRenderer.TIPO_COLUMN));
	}
	
	private void carregarDigitais() {
		lblD1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblD1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblD1);
        lblD1.setBounds(284, 150, 30, 40);
        lblD1.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.POLEGAR_DIREITO.getKey());     
            }
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.POLEGAR_DIREITO.getKey(), evt);
    			}
    		}
		});
        
        lblD2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblD2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblD2);
        lblD2.setBounds(321, 70, 30, 40);
        lblD2.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.INDICADOR_DIREITO.getKey());     
            }
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.INDICADOR_DIREITO.getKey(), evt);
    			}
    		}
		});
        
        lblD3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblD3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblD3);
        lblD3.setBounds(352, 60, 30, 40);
        lblD3.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.MEDIO_DIREITO.getKey());     
            }
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.MEDIO_DIREITO.getKey(), evt);
    			}
    		}
		});

        lblD4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblD4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblD4);
        lblD4.setBounds(383, 70, 30, 40);
        lblD4.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.ANELAR_DIREITO.getKey());     
            }
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.ANELAR_DIREITO.getKey(), evt);
    			}
    		}
		});

        lblD5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblD5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblD5);
        lblD5.setBounds(412, 108, 30, 40);
        lblD5.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.MINIMO_DIREITO.getKey());     
            }
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.MINIMO_DIREITO.getKey(), evt);
    			}
    		}
		});

        lblE1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblE1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblE1);
        lblE1.setBounds(194, 150, 30, 40);
        lblE1.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.POLEGAR_ESQUERDO.getKey());     
        	}
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.POLEGAR_ESQUERDO.getKey(), evt);
    			}
    		}
		});

        lblE2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblE2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblE2);
        lblE2.setBounds(157, 70, 30, 40);
        lblE2.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.INDICADOR_ESQUERDO.getKey());     
        	}
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.INDICADOR_ESQUERDO.getKey(), evt);
    			}
    		}
		});

        lblE3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblE3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblE3);
        lblE3.setBounds(126, 60, 30, 40);
        lblE3.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.MEDIO_ESQUERDO.getKey());     
        	}
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.MEDIO_ESQUERDO.getKey(), evt);
    			}
    		}
		});

        lblE4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblE4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblE4);
        lblE4.setBounds(95, 70, 30, 40);
        lblE4.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.ANELAR_ESQUERDO.getKey());     
        	}
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.ANELAR_ESQUERDO.getKey(), evt);
    			}
    		}
		});

        lblE5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lblE5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        tabDigital.add(lblE5);
        lblE5.setBounds(66, 108, 30, 40);
        lblE5.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseClicked(MouseEvent e) {
                cadastrarDigital(DedoEnum.MINIMO_ESQUERDO.getKey());     
        	}
        	@Override
        	public void mouseReleased(MouseEvent evt) {
    			if (evt.isPopupTrigger()) {
    				montarPopup(DedoEnum.MINIMO_ESQUERDO.getKey(), evt);
    			}
    		}
		});

        lblMaoDireita.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_MAO_DIREITA))); 
        tabDigital.add(lblMaoDireita);
        lblMaoDireita.setBounds(233, 51, 209, 287);

        lblMaoEsquerda.setIcon(new javax.swing.ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_MAO_ESQUERDA))); 
        tabDigital.add(lblMaoEsquerda);
        lblMaoEsquerda.setBounds(19, 51, 204, 287);

        lblMsgCadastroDigital.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblMsgCadastroDigital.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMsgCadastroDigital.setText("<html><center>Clique nas demarca��es dos dedos para cadastrar as digitais.<br><br>Caso queira anular algum dedo, clique com o bot�o direito do<br>mouse sobre a demarca��o e escolha Anular.</center></html>");
        tabDigital.add(lblMsgCadastroDigital);
        lblMsgCadastroDigital.setBounds(0, 380, 510, 70);
	}
	
	private void carregarTabela() {
    	tabelaEmpregados.setFont(new java.awt.Font("Tahoma", 0, 14));
		tabelaEmpregados.setModel(new EmpregadoTableModel(new ArrayList<>()));
		
		tabelaEmpregados.setRowHeight(25); 
    	tabelaEmpregados.setAutoCreateRowSorter(false);
    	
        tabelaEmpregados.getColumnModel().getColumn(0).setPreferredWidth(100);
        tabelaEmpregados.getColumnModel().getColumn(1).setPreferredWidth(370);
		
        tabelaEmpregados.getSelectionModel().addListSelectionListener(new ListSelectionListener() {    
		   public void valueChanged(ListSelectionEvent evt) {  
			   empregadoSelecionado = getEmpregadoSelecionado();
			   if (empregadoSelecionado != null && empregadoSelecionado.getId() != null) {
				   montarInformacoesEmpregado(empregadoSelecionado);
				   verificarDigitais(empregadoSelecionado);
			   } 
			}
		});
        tabelaEmpregados.addMouseListener(new MouseAdapter() {
        	public void mousePressed(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                	entregar();
                }
            }
        });
        tabelaEmpregados.addKeyListener(new java.awt.event.KeyAdapter() {
        	public void keyPressed(java.awt.event.KeyEvent evt) {
        		if (evt.getKeyCode() == KeyEvent.VK_ENTER) { 
                	entregar();
                } 
            }
        });
        selecionarPrimeiraLinha();
    } 
	
	private EmpregadoVO getEmpregadoSelecionado() {
		try {
	    	if (tabelaEmpregados.getRowSorter() != null) {
		    	int linhaSelecionada = tabelaEmpregados.getSelectedRow();
		    	int linhaSelecionadaModel = tabelaEmpregados.getRowSorter().convertRowIndexToModel(linhaSelecionada == -1 ? 0 : linhaSelecionada);
		    	return ((EmpregadoTableModel) tabelaEmpregados.getModel()).getValue(linhaSelecionadaModel);
	    	}
	    	return ((EmpregadoTableModel) tabelaEmpregados.getModel()).getValue(tabelaEmpregados.getSelectedRow());
		} catch(Exception e) {
			return null;
		}
    }
	
	private void selecionarPrimeiraLinha() {
    	if (tabelaEmpregados.getRowCount() > 0) {
    		tabelaEmpregados.getSelectionModel().setSelectionInterval(0, 0);
    	} else {
    		montarInformacoesEmpregado(null);
    	}
    }
	
	private void filtrarRegistros() {
    	tabelaEmpregados.setRowSorter(null);
		txtPesquisar.setText(null);

		popularTabela();
    }
	
	private void filtrarPesquisa(KeyEvent evt) {
		montarInformacoesEmpregado(null);
		
    	TableRowSorter<EmpregadoTableModel> sorter = new TableRowSorter<EmpregadoTableModel>(((EmpregadoTableModel) tabelaEmpregados.getModel()));
    	
    	String pesquisa = FormatacaoUtils.ajustarBusca(txtPesquisar.getText());
        if (pesquisa != null) {
        	sorter.setRowFilter(RowFilter.regexFilter("(?i)" + pesquisa, 0, 1));
        }
        if (txtPesquisar.getText().length() == 1 && evt.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
	        try {
				Robot r = new Robot();
				r.keyPress(KeyEvent.VK_UP);
			} catch(Exception e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
        }
        tabelaEmpregados.setRowSorter(sorter);
//        lblTotal.setText(MensageiroUtils.getMensagem("label.total")+": " + tblEmpregados.getRowCount());
    }
	
	private void montarInformacoesEmpregado(EmpregadoVO empregadoVO) {
		String textoPadrao = "Selecione um empregado";
        lblEmpresaEmpregado.setText(textoPadrao);
        lblNomeEmpregado.setText(textoPadrao);
        lblMatriculaEmpregado.setText(textoPadrao);
        lblIdadeEmpregado.setText(textoPadrao);
        lblSetorEmpregado.setText(textoPadrao);
        lblCargoEmpregado.setText(textoPadrao);
		
		if (empregadoVO != null) {
			lblEmpresaEmpregado.setText(empregadoVO.getEmpresa());
			lblNomeEmpregado.setText(empregadoVO.getNome());
			lblIdadeEmpregado.setText(empregadoVO.getIdade());
		    lblMatriculaEmpregado.setText(empregadoVO.getMatricula());
		    lblSetorEmpregado.setText(empregadoVO.getSetor());
		    lblCargoEmpregado.setText(empregadoVO.getCargo());
		}
    }
	
	
	private void verificarDigitais(EmpregadoVO empregadoVO) {
		Empregado empregado = empregadoService.buscarPorId(empregadoVO.getId());
		if (empregado != null) {
			List<PessoaDigital> pessoaDigitais = pessoaDigitalService.buscarPorIdPessoa(empregado.getIdPessoa());
			if (pessoaDigitais != null) {
				limparDigitais();
				for (PessoaDigital pessoaDigital : pessoaDigitais) {
					montarIconeDigital(pessoaDigital, DedoEnum.POLEGAR_DIREITO.getKey(), lblD1);
					montarIconeDigital(pessoaDigital, DedoEnum.INDICADOR_DIREITO.getKey(), lblD2);
					montarIconeDigital(pessoaDigital, DedoEnum.MEDIO_DIREITO.getKey(), lblD3);
					montarIconeDigital(pessoaDigital, DedoEnum.ANELAR_DIREITO.getKey(), lblD4);
					montarIconeDigital(pessoaDigital, DedoEnum.MINIMO_DIREITO.getKey(), lblD5);
					
					montarIconeDigital(pessoaDigital, DedoEnum.POLEGAR_ESQUERDO.getKey(), lblE1);
					montarIconeDigital(pessoaDigital, DedoEnum.INDICADOR_ESQUERDO.getKey(), lblE2);
					montarIconeDigital(pessoaDigital, DedoEnum.MEDIO_ESQUERDO.getKey(), lblE3);
					montarIconeDigital(pessoaDigital, DedoEnum.ANELAR_ESQUERDO.getKey(), lblE4);
					montarIconeDigital(pessoaDigital, DedoEnum.MINIMO_ESQUERDO.getKey(), lblE5);
				}
			}
		}
	}
	
	
	private void montarIconeDigital(PessoaDigital pessoaDigital, String posicao, JLabel lblDigital) {
		if (pessoaDigital.getPosicao().equals(posicao)) {
			if (pessoaDigital.isAnulado()) {
				lblDigital.setIcon(new ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_DIGITAL_ANULADA)));
			} else if (pessoaDigital.getDigital() != null) {
				lblDigital.setIcon(new ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_DIGITAL_CADASTRADA)));
			}
			lblDigital.repaint();
		}
	}
	
	
	private void limparDigitais() {
		lblD1.setIcon(null);
		lblD2.setIcon(null);
		lblD3.setIcon(null);
		lblD4.setIcon(null);
		lblD5.setIcon(null);
		lblE1.setIcon(null);
		lblE2.setIcon(null);
		lblE3.setIcon(null);
		lblE4.setIcon(null);
		lblE5.setIcon(null);
	}
	
	private void cadastrarDigital(String dedo) {
		PessoaDigital pessoaDigital = pessoaDigitalService.buscarDigital(empregadoSelecionado.getIdPessoa(), dedo);
		if (pessoaDigital != null && pessoaDigital.isAnulado()) {
			anularDigital(dedo); //chama a tela para apresentar o motivo
		} else {
			Digital confirmacaoDigital = new Digital(this);
			boolean cadastrou = confirmacaoDigital.abrirCaptura(empregadoSelecionado, dedo);
			if (cadastrou) {
				verificarDigitais(empregadoSelecionado);
			}
			confirmacaoDigital = null;
		}
	}
	
	private void anularDigital(String dedo) {
		AnularDigital anularDigital = new AnularDigital(this);
		boolean anulou = anularDigital.abrirTela(empregadoSelecionado.getIdPessoa(), dedo);
		if (anulou) {
			verificarDigitais(empregadoSelecionado);
		}
		anularDigital = null;
	}
	
	private void pesquisarPelaDigital() {
		Long idEmpresa = getIdEmpresaSelecionada();
		Digital confirmacaoDigital = new Digital(this);
		List<EmpregadoVO> empregadosEncontrados = confirmacaoDigital.abrirIdentificacao(idEmpresa);
		if (empregadosEncontrados != null && !empregadosEncontrados.isEmpty()) {
			popularTabela(empregadosEncontrados);
		} else {
			popularTabela();
		}
		confirmacaoDigital = null;
	}
	
	
	private Long getIdEmpresaSelecionada() {
		Long idEmpresa = null;
    	SelectVO empresaSelecionada = (SelectVO) comboEmpresa.getSelectedItem();
    	if (empresaSelecionada != null) {
    		idEmpresa = empresaSelecionada.getId();
    	}
    	return idEmpresa;
	}
	
	
	private void montarPopup(String dedo, MouseEvent evt) {
    	empregadoSelecionado = getEmpregadoSelecionado();
    	PessoaDigital pessoaDigital = pessoaDigitalService.buscarDigital(empregadoSelecionado.getIdPessoa(), dedo);
    	if (pessoaDigital == null || !pessoaDigital.isAnulado()) {
	    	JPopupMenu popup = new JPopupMenu("Popup");
	    	
	    	JMenuItem item = new JMenuItem("Cadastrar");
	    	item.addActionListener(new ActionListener() {
	    		public void actionPerformed(ActionEvent e) {
	    			cadastrarDigital(dedo); 
	    		}
	    	});
		    popup.add(item);
		    
		    item = new JMenuItem("Anular");
		    item.addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent e) {
		    		anularDigital(dedo);
		    	}
		    });
		    popup.add(item);
		    popup.show(evt.getComponent(), evt.getX(), evt.getY());
    	}
    }
	
	public void fecharAplicacao() {
		String opcoes[] = { "Sim", "N�o" };
		String msg = "Voc� tem certeza que deseja sair do "+ConstantUtils.NOME_SISTEMA+"?";
		int confirmacao = MensageiroUtils.confirmMsg(this, msg, opcoes, opcoes[0]);
		if (confirmacao == 0) {
			System.exit(0);
		}
	}

	public Usuario getUsuarioLogado() {
		return usuarioLogado;
	}
}
