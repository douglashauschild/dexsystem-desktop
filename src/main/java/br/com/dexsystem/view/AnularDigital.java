package br.com.dexsystem.view;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.constant.DedoEnum;
import br.com.dexsystem.service.PessoaDigitalService;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.MensageiroUtils;

public class AnularDigital extends JDialog {

	private static final long serialVersionUID = -4118222455854864456L;
	private JButton btnCancelar;
    private JButton btnGravar;
    private JScrollPane jScrollPane1;
    private JLabel lblDigital;
    private JLabel lblDigitalAnular;
    private JLabel lblMotivo;
    private JTextArea txtMotivo;
    
    private boolean anulou;
    private Long idPessoa;
    private String dedo;
    private PessoaDigitalService pessoaDigitalService;

    
    public AnularDigital(JFrame parent) {
        super(parent);
        this.pessoaDigitalService = new PessoaDigitalService();
        initComponents();
    }
    
    
    private void initComponents() {
        btnGravar = new JButton();
        lblDigital = new JLabel();
        btnCancelar = new JButton();
        lblDigitalAnular = new JLabel();
        lblMotivo = new JLabel();
        jScrollPane1 = new JScrollPane();
        txtMotivo = new JTextArea();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Confirma��o de Entrega"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        btnGravar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	gravar();
            }
        });

        lblDigital.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblDigital.setText("Digital");

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cancelar();
            }
        });

        lblDigitalAnular.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblDigitalAnular.setText("Digital");

        lblMotivo.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblMotivo.setText("Motivo");

        txtMotivo.setColumns(20);
        txtMotivo.setRows(5);
        jScrollPane1.setViewportView(txtMotivo);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(btnGravar, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar, GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                        .addGap(90, 90, 90))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblMotivo)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                            .addComponent(lblDigitalAnular, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDigital, GroupLayout.Alignment.LEADING))
                        .addGap(20, 20, 20))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblDigital)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDigitalAnular)
                .addGap(18, 18, 18)
                .addComponent(lblMotivo)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public boolean abrirTela(Long idPessoa, String dedo) {
		setModal(true);
		this.idPessoa = idPessoa;
        this.dedo = dedo;
		
		carregarTela();
		
        setVisible(true);
        
        lblDigitalAnular.setText(null);
        txtMotivo.setText(null);
        this.idPessoa = null;
        this.dedo = null;
        return anulou;
    }
    
    private void carregarTela() {
    	lblDigitalAnular.setText(DedoEnum.getByKey(dedo).getNome());
    	
    	PessoaDigital pessoaDigital = pessoaDigitalService.buscarDigital(idPessoa, dedo);
    	if (pessoaDigital != null && pessoaDigital.isAnulado()) {
    		txtMotivo.setText(pessoaDigital.getMotivoAnulacao());
    		txtMotivo.setEditable(false);
    		btnGravar.setEnabled(false);
    	}
    }
    
    private void gravar() {
    	try {
	    	if (txtMotivo.getText() == null || txtMotivo.getText().trim().isEmpty()) {
	    		MensageiroUtils.warnMsg(this, "Preencha o motivo!");
	    	} else {
	    		String opcoes[] = { "Sim", "N�o" };
	    		String msg = "<html><center>Ao anular este dedo voc� n�o poder� mais utiliz�-lo para cadastrar, confirmar ou identificar o empregado.<br><br>" +
	    					"Voc� tem certeza que deseja anular este dedo?</center><br></html>";
	    		int confirmacao = MensageiroUtils.confirmMsg(this, msg, opcoes, opcoes[0]);
	    		if (confirmacao == 0) {
		    		String motivo = txtMotivo.getText().trim();
		    		
		    		PessoaDigital pessoaDigital = pessoaDigitalService.buscarDigital(idPessoa, dedo);
		    		if (pessoaDigital == null) {
		    			pessoaDigital = new PessoaDigital();
		    			pessoaDigital.setIdPessoa(idPessoa);
		    			pessoaDigital.setPosicao(dedo);
		    		}
		    		pessoaDigital.setAnulado('1');
		    		pessoaDigital.setMotivoAnulacao(motivo);
		    		pessoaDigitalService.gravar(pessoaDigital);
		    		
		    		anulou = true;
		        	dispose();
	    		}
	    	}
    	} catch(Exception e) {
    		e.printStackTrace();
			MensageiroUtils.errorMsg(this, "Ocorreu algum problema durante a grava��o da anula��o da digital! Contate um administradot do sistema.");
    	}
    }
    
    
    private void cancelar() {
    	anulou = false;
    	dispose();
    }
}
