package br.com.dexsystem.view;

import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.constant.TipoConfirmacaoEnum;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.EntregaEpiVO;
import br.com.dexsystem.model.vo.SelectVO;
import br.com.dexsystem.service.EmpregadoEpiService;
import br.com.dexsystem.service.MotivoService;
import br.com.dexsystem.service.ParametroSistemaService;
import br.com.dexsystem.service.PessoaDigitalService;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.tableCellEditor.ComboBoxCellEditor;
import br.com.dexsystem.view.tableCellEditor.InputCellEditor;
import br.com.dexsystem.view.tableCellEditor.TamanhoComboBoxCellEditor;
import br.com.dexsystem.view.tableCellRender.EntregaEpiTableCellRenderer;
import br.com.dexsystem.view.tableModel.EntregaEpiTableModel;

public class EntregaEpi extends JDialog {

	private static final long serialVersionUID = 962242410857720724L;
	
	private JButton btnCancelar;
    private JButton btnContinuar;
    private JScrollPane jScrollPane1;
    private JLabel lblTitle;
    private JPanel panelTabelaEntrega;
    private JTable tblEntregaEpi;
    
    private boolean atualizarLista;
    private CredencialVO credencialVO;
    private EmpregadoVO empregadoSelecionado;
    private List<EntregaEpiVO> episEntrega;
    private MotivoService motivoService;
    private ParametroSistemaService parametroSistemaService;
    private EmpregadoEpiService empregadoEpiService;
    private PessoaDigitalService pessoaDigitalService;
    
    public EntregaEpi(Dialog parent, CredencialVO credencialVO) {
        super(parent);
        initComponents();
        
        this.atualizarLista = false;
        this.credencialVO = credencialVO;
        this.motivoService = new MotivoService();
        this.empregadoEpiService = new EmpregadoEpiService();
        this.parametroSistemaService = new ParametroSistemaService();
        this.pessoaDigitalService = new PessoaDigitalService();
    }
    
    
    private void initComponents() {

        panelTabelaEntrega = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEntregaEpi = new javax.swing.JTable();
        btnContinuar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Entrega de EPIs"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        tblEntregaEpi.setFont(new java.awt.Font("Tahoma", 0, 14));
        jScrollPane1.setViewportView(tblEntregaEpi);

        javax.swing.GroupLayout panelTabelaEntregaLayout = new javax.swing.GroupLayout(panelTabelaEntrega);
        panelTabelaEntrega.setLayout(panelTabelaEntregaLayout);
        panelTabelaEntregaLayout.setHorizontalGroup(
            panelTabelaEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        panelTabelaEntregaLayout.setVerticalGroup(
            panelTabelaEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
        );

        btnContinuar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnContinuar.setText("Continuar");
        btnContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	continuar();
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	dispose();
            }
        });

        lblTitle.setFont(new java.awt.Font("Tahoma", 0, 18));
        lblTitle.setText("Entrega de EPIs");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(450, 450, 450)
                .addComponent(btnContinuar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(450, 450, 450))
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblTitle)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelTabelaEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(20, 20, 20))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblTitle)
                .addGap(35, 35, 35)
                .addComponent(panelTabelaEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnContinuar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public boolean abrirTela(EmpregadoVO empregadoVO, List<EntregaEpiVO> epis) {
		setModal(true);
		
		this.empregadoSelecionado = empregadoVO;
		this.episEntrega = epis;
		popularTabela();
        setVisible(true);
        
        this.motivoService = null;
        this.empregadoEpiService = null;
        this.parametroSistemaService = null;
        this.episEntrega = null;
        this.empregadoSelecionado = null;
        this.pessoaDigitalService = null;
        
        return atualizarLista;
    }
    
    
	private void popularTabela() {
		tblEntregaEpi.setModel(new EntregaEpiTableModel(episEntrega, isInformarMotivo()));
    	tblEntregaEpi.setRowHeight(50); 
    	ajustarColunasEpis();
	}
	
	
	private void ajustarColunasEpis() {
		TableColumnModel tableColumn = tblEntregaEpi.getColumnModel(); 
		tableColumn.getColumn(0).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));
		tableColumn.getColumn(1).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));
		tableColumn.getColumn(2).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));
		tableColumn.getColumn(3).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));
		tableColumn.getColumn(4).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));

		tableColumn.getColumn(0).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
		tableColumn.getColumn(0).setMaxWidth(80);
		
		tableColumn.getColumn(1).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
		tableColumn.getColumn(1).setMaxWidth(80);
		
		tableColumn.getColumn(2).setMaxWidth(isInformarMotivo() ? 500 : 750);
		tableColumn.getColumn(2).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
		
		tableColumn.getColumn(3).setCellEditor(new InputCellEditor());
		tableColumn.getColumn(3).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
		tableColumn.getColumn(3).setMaxWidth(80);
		
		tableColumn.getColumn(4).setCellEditor(new TamanhoComboBoxCellEditor());
		tableColumn.getColumn(4).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
		tableColumn.getColumn(4).setMaxWidth(150);
		
		if (isInformarMotivo()) {
			tableColumn.getColumn(5).setHeaderRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_HEADER));
			tableColumn.getColumn(5).setCellEditor(new ComboBoxCellEditor(getMotivosCombo()));
			tableColumn.getColumn(5).setCellRenderer(new EntregaEpiTableCellRenderer(EntregaEpiTableCellRenderer.TIPO_COLUMN));
			tableColumn.getColumn(5).setMaxWidth(250);
		}
	}

	private boolean isInformarMotivo() {
		return parametroSistemaService.buscar().isInformarMotivoEntrega();
	}
	
	private List<SelectVO> getMotivosCombo() {
		List<SelectVO> motivosCombo = new ArrayList<SelectVO>();
    	try {
    		motivosCombo = motivoService.getMotivosCombo();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    	return motivosCombo;
	}
	
	private void continuar() {
		if (tblEntregaEpi.getCellEditor() != null) {
			tblEntregaEpi.getCellEditor().stopCellEditing();
		}
		boolean confirmar = true;
		List<EntregaEpiVO> entregaEpisVO = ((EntregaEpiTableModel) tblEntregaEpi.getModel()).getDados();
		for (EntregaEpiVO entregaEpiVO : entregaEpisVO) {
			if (entregaEpiVO.getQuantidade() == null || entregaEpiVO.getQuantidade() == 0) {
				MensageiroUtils.warnMsg(this, "Informe a quantidade, m�nimo 1, de todos EPIs desta entrega!");
				confirmar = false;
				break;
			}
			if (entregaEpiVO.getIdGrade() != null && entregaEpiVO.getTamanho().getId() == null) {
				MensageiroUtils.warnMsg(this, "Informe o tamanho de todos EPIs desta entrega!");
				confirmar = false;
				break;
			}
			if (isInformarMotivo() && entregaEpiVO.getMotivo().getId() == null) {
				MensageiroUtils.warnMsg(this, "Informe o motivo de todas entregas!");
				confirmar = false;
				break;
			}
		}
		if (confirmar) {
			abrirEscolhaConfirmacao();
		}
	}
	
	private void abrirEscolhaConfirmacao() {
		EscolhaConfirmacao escolhaConfirmacao = new EscolhaConfirmacao(this);
		Character tipoConfirmacao = escolhaConfirmacao.abrirTela();
		if (tipoConfirmacao != null) {
			if (tipoConfirmacao.equals(TipoConfirmacaoEnum.BIOMETRIA.getKey())) {
				List<PessoaDigital> digitais = pessoaDigitalService.buscarPorIdPessoa(empregadoSelecionado.getIdPessoa());
				if (digitais == null || digitais.isEmpty()) {
					MensageiroUtils.warnMsg(this, "Este empregado n�o possui digitais cadastradas!");
				} else {
					abrirConfirmacaoDigital();
				}
			} else {
				if (empregadoSelecionado.getSenha() == null || empregadoSelecionado.getSenha().isEmpty()) {
					MensageiroUtils.warnMsg(this, "Este empregado n�o possui senha cadastrada!");
				} else {
					abrirConfirmacaoSenha();
				}
			}
		}
		escolhaConfirmacao = null;
	}
	
	private void abrirConfirmacaoSenha() {
		ConfirmacaoSenha confirmacaoSenha = new ConfirmacaoSenha(this, credencialVO);
		boolean senhaCorreta = confirmacaoSenha.abrirTela(empregadoSelecionado);
		if (senhaCorreta) {
			confirmarEntregaDevolucaoEpis(TipoConfirmacaoEnum.SENHA.getKey());
		}
		confirmacaoSenha = null;
	}
	
	private void abrirConfirmacaoDigital() {
		Digital confirmacaoDigital = new Digital(this, credencialVO);
		boolean confirmou = confirmacaoDigital.abrirConfirmacao(empregadoSelecionado);
		if (confirmou) {
			confirmarEntregaDevolucaoEpis(TipoConfirmacaoEnum.BIOMETRIA.getKey());
		}
		confirmacaoDigital = null;
	}
	
	private void confirmarEntregaDevolucaoEpis(Character tipoConfirmacao) {
		try {
			empregadoEpiService.confirmarEntrega(empregadoSelecionado, episEntrega, TipoConfirmacaoEnum.BIOMETRIA.getKey());
			MensageiroUtils.infoMsg(this, "Todos EPIs foram entregues com sucesso!");
			atualizarLista = true;
			dispose();
		} catch(Exception e) {
			e.printStackTrace();
			MensageiroUtils.errorMsg(this, "Ocorreu algum erro ao confirmar os EPIs! Contate um administrador do sistema.");
		}
	}
}
