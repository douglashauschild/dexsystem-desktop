package br.com.dexsystem.view;

import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.constant.TipoConfirmacaoEnum;
import br.com.dexsystem.util.ConstantUtils;

public class EscolhaConfirmacao extends JDialog {

	private static final long serialVersionUID = -6431242363200327337L;
	
	private JButton btnCancelar;
    private JButton btnDigital;
    private JButton btnSenha;
    private JLabel lblTexto;
    
    private Character tipoConfirmacao;
    
    public EscolhaConfirmacao(Dialog parent) {
        super(parent);
        initComponents();
    }

    private void initComponents() {

        btnSenha = new JButton();
        btnCancelar = new JButton();
        lblTexto = new JLabel();
        btnDigital = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Entrega de EPIs"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        btnSenha.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnSenha.setText("Senha");
        btnSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	confirmarSenha();
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelar();
            }
        });

        lblTexto.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblTexto.setText("Escolha uma forma de confirmação");

        btnDigital.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnDigital.setText("Digital");
        btnDigital.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarDigital();
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCancelar, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(lblTexto)
                            .addComponent(btnDigital, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSenha, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblTexto)
                .addGap(20, 20, 20)
                .addComponent(btnSenha, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDigital, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelar, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public Character abrirTela() {
		setModal(true);
        setVisible(true);
        return tipoConfirmacao;
    }
    
    
    private void confirmarSenha() {
    	retornar(TipoConfirmacaoEnum.SENHA.getKey());
    }
    
    private void confirmarDigital() {
    	retornar(TipoConfirmacaoEnum.BIOMETRIA.getKey());
    }
    
    private void cancelar() {
    	retornar(null);
    }
    
    private void retornar(Character tipoConfirmacao) {
    	this.tipoConfirmacao = tipoConfirmacao;
    	dispose();
    }
}
