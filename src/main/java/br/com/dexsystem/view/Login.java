package br.com.dexsystem.view;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.Principal;
import br.com.dexsystem.dao.UsuarioDao;
import br.com.dexsystem.model.Usuario;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.TokenVO;
import br.com.dexsystem.service.TokenService;
import br.com.dexsystem.service.client.UsuarioClientService;
import br.com.dexsystem.thread.ConexaoThread;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.InternetUtils;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.MensageiroUtils;

public class Login extends JDialog {
	
	private static final long serialVersionUID = -3234945376693818817L;
	private JLabel btnConfig;
    private JButton btnEntrar;
    private JButton btnSair;
    private JLabel lblConexao;
    private JLabel lblEmail;
    private JLabel lblIcone;
    private JLabel lblSenha;
    private JLabel lblSubTitulo;
    private JLabel lblTitulo;
    private JTextField txtEmail;
    private JPasswordField txtSenha;
    
	private CredencialVO credencialVO;
	private UsuarioDao usuarioDao;
	
	public Login() {
		setModal(true);
        initComponents();
        
        this.usuarioDao = new UsuarioDao();
        this.credencialVO = new CredencialVO();
        verificaConexoes();
    }
	
	private void initComponents() {
        btnEntrar = new JButton();
        lblEmail = new JLabel();
        lblSenha = new JLabel();
        btnSair = new JButton();
        txtEmail = new JTextField();
        txtSenha = new JPasswordField();
        lblTitulo = new JLabel();
        lblSubTitulo = new JLabel();
        lblIcone = new JLabel();
        btnConfig = new JLabel();
        lblConexao = new JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Login"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        btnEntrar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnEntrar.setText("Entrar");
        btnEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entrar();
            }
        });

        lblEmail.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblEmail.setText("E-mail");

        lblSenha.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblSenha.setText("Senha");

        btnSair.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnSair.setText("Sair");
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sair();
            }
        });

        txtEmail.setFont(new java.awt.Font("Tahoma", 0, 14));

        txtSenha.setFont(new java.awt.Font("Tahoma", 0, 14));
        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
					entrar();
				}
			}
		});

        lblTitulo.setFont(new java.awt.Font("Tahoma", 1, 24));
        lblTitulo.setHorizontalAlignment(SwingConstants.CENTER);
        lblTitulo.setText("DEX System");

        lblSubTitulo.setFont(new java.awt.Font("Tahoma", 0, 13));
        lblSubTitulo.setHorizontalAlignment(SwingConstants.RIGHT);
        lblSubTitulo.setText("M�dulo Desktop");

        lblIcone.setHorizontalAlignment(SwingConstants.CENTER);
        lblIcone.setIcon(new ImageIcon(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA)));

        btnConfig.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnConfig.setText("Configura��es");
        btnConfig.setCursor(new Cursor(Cursor.HAND_CURSOR));
        btnConfig.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                abrirConfiguracoes();
            }
        });

        lblConexao.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblConexao.setText("Verificando conex�o com a internet...");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblSenha)
                    .addComponent(lblEmail)
                    .addComponent(txtSenha)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE)
                    .addComponent(btnConfig, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtEmail)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblIcone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblConexao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(40, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblSubTitulo)
                .addGap(117, 117, 117))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(lblIcone)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSubTitulo)
                .addGap(31, 31, 31)
                .addComponent(lblEmail)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSenha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnConfig, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblConexao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
	
	public CredencialVO abrirTela() {
        setVisible(true);
        this.usuarioDao = null;
		return credencialVO;
    }
	
	
	public void abrirConfiguracoes() {
		Configuracao configuracao = new Configuracao(this);
		configuracao.abrirTela();
		configuracao = null;
		verificaConexoes();
	}
	
	public void entrar() {
		boolean erro = false;
		String email = txtEmail.getText();
		char[] password = txtSenha.getPassword();
		String senha = new String(password);
		
		if ((email == null || email.equals("")) || (senha == null || senha.equals(""))) {
			MensageiroUtils.warnMsg(this, "Preencha o e-mail e a senha!");
		} else {						
			erro = autenticar(email, senha);
			
			if (erro) {
				return;
			}
			this.dispose();
		}
	}
	
	private boolean autenticar(String email, String senha) {
		boolean erro = false;
		boolean usuarioSemPermissao = false;
		boolean usuarioNaoEncontrado = false;
		Usuario usuario = null;
		
		if (!ConexaoThread.temConexao) {
			usuario = usuarioDao.buscarPorEmail(email);
			if (usuario != null) {
				boolean senhaCorreta = new BCryptPasswordEncoder().matches(senha, usuario.getSenha());
				if (!senhaCorreta) {
					MensageiroUtils.warnMsg(this, "E-mail ou senha incorretos!");
					erro = true;
				} else if (!usuario.isPossuiAcessoDesktop()) {
					MensageiroUtils.warnMsg(this, "Usu�rio sem acesso ao "+ConstantUtils.NOME_SISTEMA+"!");
					erro = true;
				} else {
					credencialVO.setId(usuario.getId());
					credencialVO.setNome(usuario.getNome());
					credencialVO.setEmail(usuario.getEmail());
					credencialVO.setSenha(usuario.getSenha());
					credencialVO.setAutenticado(true);
				}
			} else {
				MensageiroUtils.warnMsg(this, "Usu�rio n�o encontrado no banco de dados!");
				erro = true;
			}
		} else {
			try {
				TokenVO tokenVO = null;
				try {
					tokenVO = new TokenService().obterToken(email, senha);
				} catch (HttpClientErrorException e) {
					if (e.getStatusCode() != null) {
						if (e.getStatusCode().equals(HttpStatus.NOT_ACCEPTABLE)) {
							usuarioSemPermissao = true;
						} else if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
							erro = true;
						} else if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
							usuarioNaoEncontrado = true;
						} 
					}
//					e.printStackTrace();
				}
				if (tokenVO != null) {
					credencialVO.setToken(tokenVO.getToken());
					usuario = new UsuarioClientService().obterUsuarioLogado(credencialVO.getToken());
					if (usuario != null) {
						credencialVO.setId(usuario.getId());
						credencialVO.setNome(usuario.getNome());
						credencialVO.setEmail(usuario.getEmail());
						credencialVO.setSenha(usuario.getSenha());
						credencialVO.setAutenticado(true);
					} else {
						usuarioNaoEncontrado = true;
						LoggerUtils.logText("Usu�rio n�o encontrado no banco de dados!\n");
					}
				}
			} catch(Exception e) {
				erro = true;
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
			if (!credencialVO.isAutenticado()) {
				erro = true;
			}
			if (usuarioNaoEncontrado) {
				MensageiroUtils.warnMsg(this, "Usu�rio n�o encontrado no banco de dados!");
				erro = true;
			} else if (usuarioSemPermissao) {
				MensageiroUtils.warnMsg(this, "Usu�rio sem acesso ao "+ConstantUtils.NOME_SISTEMA+"!");
			} else if (erro) {
				MensageiroUtils.warnMsg(this, "E-mail ou senha incorretos!");
			}
		}
		return erro;
	}
	
	public void sair() {
		dispose();
	}
	
	
	public boolean verificaConexoes() {
		boolean conexaoAtiva = false;
		
		String mensagem = "Verificando conex�o com a internet...";
		String icone = null;
		if (InternetUtils.isConexaoInternet()) {
			
			ConexaoThread.temConexao = true;
			if (InternetUtils.isConexaoServico()) {
				mensagem = "Conex�o estabelecida!";
				icone = ConstantUtils.ICONE_CONEXAO_SUCESSO;
				conexaoAtiva = true;
			} else {
				mensagem = "N�o foi poss�vel se conectar ao servidor!";
				icone = ConstantUtils.ICONE_CONEXAO_ERRO;
				conexaoAtiva = false;
				ConexaoThread.temConexao = false;
			}
		} else {
			mensagem = "Sem conex�o com a internet!";
			icone = ConstantUtils.ICONE_CONEXAO_ERRO;
			conexaoAtiva = false;
			ConexaoThread.temConexao = false;
		}
		
		lblConexao.setIcon(new ImageIcon(Principal.class.getClassLoader().getResource(icone))); 
		lblConexao.setText(mensagem);
		lblConexao.repaint();
		return conexaoAtiva;
	}
	
}
