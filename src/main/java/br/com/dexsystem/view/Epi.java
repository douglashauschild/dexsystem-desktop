package br.com.dexsystem.view;

import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.text.MaskFormatter;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.SelectVO;
import br.com.dexsystem.service.EmpregadoEpiService;
import br.com.dexsystem.service.EpiService;
import br.com.dexsystem.service.GradeItemService;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.FormatacaoUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.componente.ComboBoxLarge;

public class Epi extends JDialog {

	private static final long serialVersionUID = -5408036680891744565L;
	
	private JButton btnCancelar;
    private JButton btnGravar;
    private ComboBoxLarge comboEpi;
    private JComboBox<SelectVO> comboTamanho;
    private JLabel lblGradeTitle;
    private JFormattedTextField txtData;
    private JLabel lblDataTitle;
    private JLabel lblEpiTitle;
    private JLabel lblGrade;
    private JLabel lblQuantidadeTitle;
    private JLabel lblTamanhoTitle;
    private JTextField txtQtd;
    private JLabel lblCaTitle;
    private JLabel lblCa;
    private JPanel panelCA;
    private JPanel panelGrade;
    
    private boolean cadastroOk;
    private CredencialVO credencialVO;
    private EmpregadoVO empregadoVO;
    private EpiService epiService;
    private GradeItemService gradeItemService;
    private EmpregadoEpiService empregadoEpiService;
    
    public Epi(Dialog parent, CredencialVO credencialVO) {
        super(parent);
        initComponents();
        
        this.cadastroOk = true;
        this.credencialVO = credencialVO;
        this.epiService = new EpiService();
        this.gradeItemService = new GradeItemService();
        this.empregadoEpiService = new EmpregadoEpiService();
    }

    
    private void initComponents() {
    	lblDataTitle = new JLabel();
        lblEpiTitle = new JLabel();
        btnGravar = new JButton();
        btnCancelar = new JButton();
        lblTamanhoTitle = new JLabel();
        comboTamanho = new JComboBox<>();
        txtData = new JFormattedTextField();
        lblGradeTitle = new JLabel();
        lblGrade = new JLabel();
        comboEpi = new ComboBoxLarge();
        lblQuantidadeTitle = new JLabel();
        txtQtd = new JTextField();
        lblCa = new JLabel();
        lblCaTitle = new JLabel();
        panelCA = new JPanel();
        panelGrade = new JPanel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Incluir EPI"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        lblDataTitle.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblDataTitle.setText("Previs�o de Entrega");

        lblEpiTitle.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblEpiTitle.setText("EPI");

        btnGravar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gravar();
            }
        });

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cancelar();
            }
        });

        lblTamanhoTitle.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblTamanhoTitle.setText("Tamanho");

        comboTamanho.setFont(new java.awt.Font("Tahoma", 0, 14));
        comboTamanho.setModel(new javax.swing.DefaultComboBoxModel<>());
        
        lblCaTitle.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblCaTitle.setText("CA");
        
        lblCa.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblCa.setText("N�o informado");

        MaskFormatter ftmData = null;
        try {
        	ftmData = new MaskFormatter("##/##/####");
        } catch(Exception e) {
        	e.printStackTrace();
        }
      	ftmData.setValidCharacters("0123456789");
      	txtData = new JFormattedTextField(ftmData);
      	txtData.setFont(new java.awt.Font("Tahoma", 0, 14));
      	txtData.setColumns(6);

        lblGradeTitle.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblGradeTitle.setText("Grade");

        lblGrade.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblGrade.setText("N�o informado");

        comboEpi.setFont(new java.awt.Font("Tahoma", 0, 14));
        comboEpi.setModel(new javax.swing.DefaultComboBoxModel<>());
        comboEpi.setMinimumSize(new java.awt.Dimension(120, 23));
        comboEpi.setPreferredSize(new java.awt.Dimension(355, 23));
        comboEpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selecionarEpi();
            }
        });

        lblQuantidadeTitle.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblQuantidadeTitle.setText("Quantidade");

        txtQtd = new JTextField();
        txtQtd.setFont(new java.awt.Font("Tahoma", 0, 14));
        txtQtd.setMinimumSize(new java.awt.Dimension(110, 23));
        txtQtd.setPreferredSize(new java.awt.Dimension(110, 23));
        txtQtd.addKeyListener(new KeyAdapter() {
        	public void keyTyped(KeyEvent e) {
        		char c = e.getKeyChar();
        		if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
        			getToolkit().beep();
        			e.consume();
        		}
        	}
        });
        
        javax.swing.GroupLayout panelCALayout = new javax.swing.GroupLayout(panelCA);
        panelCA.setLayout(panelCALayout);
        panelCALayout.setHorizontalGroup(
            panelCALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCALayout.createSequentialGroup()
                .addGroup(panelCALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCaTitle)
                    .addComponent(lblCa))
                .addGap(360, 360, 360))
        );
        panelCALayout.setVerticalGroup(
            panelCALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCALayout.createSequentialGroup()
                .addComponent(lblCaTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCa)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout panelGradeLayout = new javax.swing.GroupLayout(panelGrade);
        panelGrade.setLayout(panelGradeLayout);
        panelGradeLayout.setHorizontalGroup(
            panelGradeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGradeLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panelGradeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblGradeTitle)
                    .addComponent(lblGrade)
                    .addComponent(lblTamanhoTitle)
                    .addComponent(comboTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelGradeLayout.setVerticalGroup(
            panelGradeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelGradeLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(lblGradeTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGrade)
                .addGap(18, 18, 18)
                .addComponent(lblTamanhoTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboTamanho, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(panelGrade, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(panelCA, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(comboEpi, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(20, 20, 20))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblQuantidadeTitle)
                            .addComponent(lblDataTitle)
                            .addComponent(lblEpiTitle)
                            .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(342, 342, 342))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(143, 143, 143))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblDataTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblEpiTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboEpi, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelCA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelGrade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(lblQuantidadeTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtQtd, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public boolean abrirTela(EmpregadoVO empregadoVO) {
		setModal(true);
		
		this.empregadoVO = empregadoVO;
		carregarDados();
        setVisible(true);
        
        this.epiService = null;
        this.gradeItemService = null;
        this.empregadoEpiService = null;
        
        return cadastroOk;
    }
    
    
    private void gravar() {
    	try {
	    	if (!validarGravacao()) {
	    		return;
	    	}
	    	SelectVO epiSelecionado = (SelectVO) comboEpi.getSelectedItem();
	    	SelectVO tamanhoSelecionado = (SelectVO) comboTamanho.getSelectedItem();
	    	Long idEmpregado = empregadoVO.getId();
	    	Long idEpi = epiSelecionado.getId();
	    	Date dataPrevistaEntrega = FormatacaoUtils.getData(txtData.getText());
	    	Long idGradeItem = tamanhoSelecionado != null && tamanhoSelecionado.getId() != null ? tamanhoSelecionado.getId() : null;
	    	Integer qtd = Integer.valueOf(txtQtd.getText());
	    	Long idUsuario = credencialVO.getId();
	    	
	    	EmpregadoEpi empregadoEpi = new EmpregadoEpi();
	    	empregadoEpi.setIdEmpregado(idEmpregado);
	    	empregadoEpi.setIdEpi(idEpi);
			empregadoEpi.setDataPrevistaEntrega(dataPrevistaEntrega);
			empregadoEpi.setIdGradeItem(idGradeItem);
			empregadoEpi.setQuantidade(qtd);
			empregadoEpi.setIdUsuario(idUsuario);
			empregadoEpi.setInclusaoManual('1');
			
			empregadoEpiService.gravar(empregadoEpi);
			MensageiroUtils.infoMsg(this, "Entrega salva com sucesso!");
			cadastroOk = true;
    	} catch(Exception e) {
    		e.printStackTrace();
    		MensageiroUtils.errorMsg(this, "Ocorreu algum problema ao salvar! Tente novamente mais tarde.");
    		cadastroOk = false;
    	}
    	dispose();
    }
    
    private boolean validarGravacao() {
    	if (txtData.getText() == null || txtData.getText().isEmpty()) {
    		MensageiroUtils.warnMsg(this, "Informe a data!");
    		return false;
    	}
    	if (!FormatacaoUtils.isDataValida(txtData.getText())) {
    		MensageiroUtils.warnMsg(this, "Informe uma data v�lida!");
    		return false;
    	}
    	SelectVO epiSelecionado = (SelectVO) comboEpi.getSelectedItem();
    	if (epiSelecionado == null || epiSelecionado.getId() == null) {
    		MensageiroUtils.warnMsg(this, "Selecione o EPI!");
    		return false;
    	}
    	if (panelGrade.isVisible()) {
    		SelectVO tamanhoSelecionado = (SelectVO) comboTamanho.getSelectedItem();
    		if (tamanhoSelecionado == null || tamanhoSelecionado.getId() == null) {
    			MensageiroUtils.warnMsg(this, "Selecione o tamanho!");
    			return false;
    		}
    	}
    	if (txtQtd.getText() == null || txtQtd.getText().isEmpty()) {
    		MensageiroUtils.warnMsg(this, "Informe a quantidade!");
    		return false;
    	}
    	return true;
    }
    
    private void cancelar() {
    	dispose();
    }
    
    private void carregarDados() {
    	limparDados();
    	
    	txtData.setText(FormatacaoUtils.getDataString(new Date()));
    	txtQtd.setText("1");
    	popularComboEpis();
    	
    	repaint();
    	pack();
    }
    
    private void limparDados() {
    	panelCA.setVisible(false);
    	panelGrade.setVisible(false);
    	comboTamanho.removeAllItems();
    	panelCA.setVisible(false);
    }
    
    private void popularComboEpis() {
		comboEpi.removeAllItems();
		
		List<SelectVO> epis = epiService.getEpisCombo();
		comboEpi.addItem(new SelectVO(null, "-- Selecione --"));
		for (SelectVO epi : epis) {
			comboEpi.addItem(epi);
		}
	}
    
    private void selecionarEpi() {
    	limparDados();
    	SelectVO epiSelecionado = (SelectVO) comboEpi.getSelectedItem();
    	if (epiSelecionado != null && epiSelecionado.getId() != null) {
    		br.com.dexsystem.model.Epi epi = epiService.buscarPorId(epiSelecionado.getId());
    		if (epi != null) {
    			if (epi.getCa() != null && !epi.getCa().isEmpty()) {
    				panelCA.setVisible(true);
    		    	lblCa.setText(epi.getCa());
    			}
    			if (epi.getIdGrade() != null) {
    				panelGrade.setVisible(true);
    		    	lblGrade.setText(epi.getGrade().getNome());
    		    	
    		    	popularComboTamanhos(epi.getIdGrade());
    			}
    		}
    	}
    	repaint();
    	pack();
    }
    
    
    private void popularComboTamanhos(Long idGrade) {
    	lblTamanhoTitle.setVisible(true);
    	comboTamanho.setVisible(true);
		comboTamanho.removeAllItems();
		
		List<SelectVO> tamanhos = gradeItemService.getGradeItensCombo(idGrade);
		comboTamanho.addItem(new SelectVO(null, "-- Selecione --"));
		for (SelectVO tamanho : tamanhos) {
			comboTamanho.addItem(tamanho);
		}
	}
}
