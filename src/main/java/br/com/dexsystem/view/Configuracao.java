package br.com.dexsystem.view;

import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import br.com.dexsystem.Principal;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.util.PropertiesUtils;

public class Configuracao extends JDialog {

	private static final long serialVersionUID = -5204555834292278411L;
	private JButton btnCancelar;
    private JButton btnGravar;
    private JLabel lblTempo;
    private JLabel lblUrl;
    private JTextField txtTempoSincronizacao;
    private JTextField txtUrl;
    private JLabel lblTempoExplicativo;
    private JLabel lblUrlExplicativo;
    private JLabel lblMinutos;
	
    
    public Configuracao(Dialog parent) {
        super(parent);
        initComponents();
    }

    
    private void initComponents() {
    	btnGravar = new JButton();
        lblUrl = new JLabel();
        lblTempo = new JLabel();
        btnCancelar = new JButton();
        txtUrl = new JTextField();
        txtTempoSincronizacao = new JTextField();
        lblTempoExplicativo = new JLabel();
        lblUrlExplicativo = new JLabel();
        lblMinutos = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Configurações"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);
        
        btnGravar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnGravar.setText("Gravar");
        btnGravar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gravar();
            }
        });

        lblUrl.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblUrl.setText("URL");

        lblTempo.setFont(new java.awt.Font("Tahoma", 1, 14));
        lblTempo.setText("Sincronização de dados");
        
        lblTempoExplicativo.setText("Defina o tempo para a sincronização de dados rodar automaticamente");

        lblUrlExplicativo.setText("Defina a URL de acesso ao serviço");
        
        lblMinutos.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblMinutos.setText("minutos");

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cancelar();
            }
        });

        txtUrl.setFont(new java.awt.Font("Tahoma", 0, 14));

        txtTempoSincronizacao.setFont(new java.awt.Font("Tahoma", 0, 14));
        txtTempoSincronizacao.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTempoSincronizacao.addKeyListener(new KeyAdapter() {
        	public void keyTyped(KeyEvent e) {
        		char c = e.getKeyChar();
        		if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
        			getToolkit().beep();
        			e.consume();
        		}
        	}
        });
        
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblTempo)
                        .addComponent(lblUrl)
                        .addComponent(lblTempoExplicativo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblUrlExplicativo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(txtTempoSincronizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(lblMinutos))
                        .addComponent(txtUrl, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblUrl)
                .addGap(3, 3, 3)
                .addComponent(lblUrlExplicativo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUrl, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(lblTempo)
                .addGap(3, 3, 3)
                .addComponent(lblTempoExplicativo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTempoSincronizacao, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMinutos))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGravar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public void abrirTela() {
		setModal(true);
		carregarTela();
		
        setVisible(true);
    }
    
    private void carregarTela() {
    	txtUrl.setText(PropertiesUtils.getUrl());
    	txtTempoSincronizacao.setText(PropertiesUtils.getTempoSincronizacao(false).toString());
    }
    
    private void gravar() {
    	if (txtUrl.getText() == null || txtUrl.getText().isEmpty()) {
    		MensageiroUtils.warnMsg(this, "A URL deve ser preenchida!");
    		return;
    	}
    	if (txtTempoSincronizacao.getText() == null || txtTempoSincronizacao.getText().isEmpty()) {
    		MensageiroUtils.warnMsg(this, "O tempo de sincronização deve ser preenchido!");
    		return;
    	} else {
    		Long tempo = Long.valueOf(txtTempoSincronizacao.getText());
    		if (tempo <= 1) {
    			MensageiroUtils.warnMsg(this, "O tempo de sincronização deve ser superior a um minuto!");
    			return;
    		}
    	}
    	try {
    		PropertiesUtils.gravarConfiguracoes(txtUrl.getText(), txtTempoSincronizacao.getText());
    	} catch(Exception e) {
    		e.printStackTrace();
    		LoggerUtils.logErro(e);
    		MensageiroUtils.errorMsg(this, "Ocorreu algum erro ao tentar gravar as configurações! Contate um administrador do sistema.");
    	}
    	dispose();
    }
    
    
    private void cancelar() {
    	dispose();
    }
}
