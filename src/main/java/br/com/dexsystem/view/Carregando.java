package br.com.dexsystem.view;

import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import br.com.dexsystem.Principal;
import br.com.dexsystem.util.ConstantUtils;

public class Carregando extends JDialog {
	
	private static final long serialVersionUID = -8265704331287768016L;
	private JProgressBar barraProgresso;
    private JLabel lblMensagem;

    private int qtdItens;
    private int progresso;
    
	public Carregando(Frame parent) {
        super(parent);
    }
	
	private void initComponents() {
        barraProgresso = new JProgressBar();
        lblMensagem = new JLabel();
        
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Carregando"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        lblMensagem.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblMensagem.setHorizontalAlignment(SwingConstants.CENTER);
        
        if (qtdItens == 1) {
        	barraProgresso.setIndeterminate(true);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(barraProgresso, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                .addGap(25, 25, 25))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(lblMensagem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(barraProgresso, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        pack();
        this.setLocationRelativeTo(getParent());
    }
	
	
	public void abrirTela(String texto, int qtdItens) {
		this.qtdItens = qtdItens;
        initComponents();
        atualizar(texto);
        
        pack();
        setVisible(true);      
    }
	
	
	public void fecharTela() {
		lblMensagem = null;
		qtdItens = 0;
		progresso = 0;
        dispose();      
    }
	
	
	public void atualizar(String texto) {
    	lblMensagem.setText(texto);
    	atualizarProgresso();
    	repaint();
    }
	
	public void atualizarProgresso() {
		progresso = progresso + (100 / qtdItens);
		barraProgresso.setValue(progresso);
	}
}
