package br.com.dexsystem.view.tableModel;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.table.AbstractTableModel;

import br.com.dexsystem.model.vo.EntregaEpiVO;
import br.com.dexsystem.model.vo.SelectVO;
import br.com.dexsystem.util.MensageiroUtils;

public class EntregaEpiTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1911587060750551451L;
	private final int DEVOLVER = 0;
	private final int SUBSTITUIR = 1;
    private final int EPI = 2;
    private final int QTD = 3;
    private final int TAMANHO = 4;
    private final int MOTIVO = 5;
    
 
    private String colunas[] = { "Devolver", "Substituir", "EPI", "Qtd.", "Tamanho", "Motivo" };
    private List<EntregaEpiVO> dados;

    
	public EntregaEpiTableModel(List<EntregaEpiVO> dados, boolean informarMotivo) {
        this.dados=dados;
        if (!informarMotivo) {
			colunas = new String[]{ "Devolver", "Substituir", "EPI", "Qtd.", "Tamanho" };
		}
    }
 
    @Override
    public int getColumnCount() {
        return colunas.length;
    }
 
    @Override
    public int getRowCount() {
    	if (dados != null) {
    		return dados.size();
    	}
    	return 0; 
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
	        case DEVOLVER:
	        	return Boolean.class;
	        case SUBSTITUIR:
	        	return Boolean.class;
	        case EPI:
	            return String.class;
	        case QTD:
	            return Integer.class;
	        case TAMANHO:
	            return Long.class;
	        case MOTIVO:
	            return Long.class;
	        default:
	        	throw new IndexOutOfBoundsException("Coluna inv�lida");
        }
    }
 
    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	EntregaEpiVO entregaEpiVO = dados.get(rowIndex);
        switch (columnIndex) {
        	case DEVOLVER:
	        	return entregaEpiVO.isDevolver();
	        case SUBSTITUIR:
	        	return entregaEpiVO.isSubstituir();
	        case EPI:
	            return "<html><body>"+entregaEpiVO.getEpi()+"<font size='3'><br/>CA: "+entregaEpiVO.getCa()+"<br/>A��o: "+entregaEpiVO.getAcao()+"</font></body></html>";
	        case QTD:
	            return entregaEpiVO.getQuantidade();
	        case TAMANHO:
	            return entregaEpiVO.getTamanho();
	        case MOTIVO:
	            return entregaEpiVO.getMotivo();
	        default:
	        	throw new IndexOutOfBoundsException("Coluna inv�lida");
        }
    }
 
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	EntregaEpiVO entregaEpiVO = dados.get(rowIndex);
    	if (entregaEpiVO.getAcao().equals("Primeira Entrega") && (columnIndex == DEVOLVER || columnIndex == SUBSTITUIR || columnIndex == QTD)) {
    		return false;
    	}
    	if (entregaEpiVO.getAcao().equals("Devolu��o") && (columnIndex == QTD || columnIndex == TAMANHO)) {
    		return false;
    	}
        return (columnIndex == DEVOLVER || columnIndex == SUBSTITUIR || columnIndex == QTD || columnIndex == TAMANHO || columnIndex == MOTIVO);
    }
 
    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
    	EntregaEpiVO entregaEpiVO = dados.get(rowIndex);
    	
    	 switch (columnIndex) {
    	 	case DEVOLVER:
    	 		entregaEpiVO.setDevolver(validarAcoes(entregaEpiVO, false, (boolean) value));
    	 		validarValores(entregaEpiVO);
    	 		fireTableDataChanged();
    	 		break;
	        case SUBSTITUIR:
	        	entregaEpiVO.setSubstituir(validarAcoes(entregaEpiVO, true, (boolean) value));
	        	validarValores(entregaEpiVO);
        		fireTableDataChanged();
	        	break;
	        case QTD:
	        	entregaEpiVO.setQuantidade(Integer.parseInt(value.toString()));
	        	break;
	        case TAMANHO:
	        	entregaEpiVO.setTamanho((SelectVO) value);
	        	break;
	        case MOTIVO:
	        	entregaEpiVO.setMotivo((SelectVO) value);
	        	break;
    	 }
    }
 
    public EntregaEpiVO getValue(int rowIndex){
    	if (rowIndex == -1) {
    		return new EntregaEpiVO();
    	}
    	return dados.get(rowIndex);
    }
 
    public int indexOf(EntregaEpiVO entregaEpiVO) {
        return dados.indexOf(entregaEpiVO);
    }
 
    public void add(EntregaEpiVO entregaEpiVO) {
        dados.add(entregaEpiVO);
        fireTableRowsInserted(indexOf(entregaEpiVO), indexOf(entregaEpiVO));
    }
 
    public void addAll(List<EntregaEpiVO> dados) {
        dados.addAll(dados);
        fireTableDataChanged();
    }
 
    public void remove(int rowIndex) {
        dados.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
 
    public void remove(EntregaEpiVO entregaEpiVO) {
        int indexBefore = indexOf(entregaEpiVO);
        dados.remove(entregaEpiVO);  
        fireTableRowsDeleted(indexBefore, indexBefore);
    }
 
    public void removeAll() {
        dados.clear();
        fireTableDataChanged();
    }
    
	public List<EntregaEpiVO> getDados() {
		return dados;
	}

	public void setDados(List<EntregaEpiVO> dados) {
		this.dados = dados;
		fireTableDataChanged();
	}
	
	private boolean validarAcoes(EntregaEpiVO entregaEpiVO, boolean substituicao, boolean valor) {
		boolean retorno = true;
		if (substituicao && !valor && !entregaEpiVO.isDevolver()) {
			retorno = false;
		} else if (!substituicao && !valor && !entregaEpiVO.isSubstituir()) {
			retorno = false;
		}
		if (!retorno) {
			JFrame frame = new JFrame();
			frame.setAlwaysOnTop(true);
			MensageiroUtils.warnMsg(frame, "Voc� deve ter no m�nimo uma a��o selecionada para este registro!");
		}
		return retorno ? valor: !valor;
	}
	
	private void validarValores(EntregaEpiVO entregaEpiVO) {
		if (entregaEpiVO.isDevolver() && !entregaEpiVO.isSubstituir()) {
 			entregaEpiVO.setQuantidade(entregaEpiVO.getQuantidadeOriginal());
 			entregaEpiVO.setTamanho(entregaEpiVO.getTamanhoOriginal());
 		}
	}
}