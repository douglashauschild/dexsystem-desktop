package br.com.dexsystem.view.tableModel;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.vo.EmpregadoVO;

public class EmpregadoTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 7091510632931020136L;
	private final int MATRICULA = 0;
	private final int NOME = 1;
    
 
    private final String colunas[] = { "Matr�cula", "Nome" };
    private List<EmpregadoVO> dados;
 
    public EmpregadoTableModel(List<EmpregadoVO> dados) {
        this.dados=dados;
    }
 
    @Override
    public int getColumnCount() {
        return colunas.length;
    }
 
    @Override
    public int getRowCount() {
    	if (dados != null) {
    		return dados.size();
    	}
    	return 0; 
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
	        case MATRICULA:
	            return String.class;
	        case NOME:
	            return String.class;
	        default:
	            throw new IndexOutOfBoundsException("Coluna Inv�lida");
        }
    }
 
    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	EmpregadoVO empregado = dados.get(rowIndex);
 
        //retorna o valor da coluna
        switch (columnIndex) {
        	case MATRICULA:
            	return empregado.getMatricula();
	        case NOME:
	            return empregado.getNome();
	        default:
	        	throw new IndexOutOfBoundsException("Coluna Inv�lida");
        }
    }
 
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
 
 
    public EmpregadoVO getValue(int rowIndex){
    	if (rowIndex == -1) {
    		return new EmpregadoVO();
    	}
    	return dados.get(rowIndex);
    }
 
    public int indexOf(EmpregadoVO empregado) {
        return dados.indexOf(empregado);
    }
 
    public void add(EmpregadoVO empregado) {
        dados.add(empregado);
        fireTableRowsInserted(indexOf(empregado), indexOf(empregado));
    }
 
    public void addAll(List<Empregado> dados) {
        dados.addAll(dados);
        fireTableDataChanged();
    }
 
    public void remove(int rowIndex) {
        dados.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
 
    public void remove(EmpregadoVO empregado) {
        int indexBefore = indexOf(empregado);
        dados.remove(empregado);  
        fireTableRowsDeleted(indexBefore, indexBefore);
    }
 
    public void removeAll() {
        dados.clear();
        fireTableDataChanged();
    }
    
    public void setDados(List<EmpregadoVO> empregados) {
    	this.dados = empregados;
    	fireTableDataChanged();
    }
}