package br.com.dexsystem.view.tableModel;

import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.com.dexsystem.model.vo.EntregaVO;

public class EntregaTableModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 1911587060750551451L;
	private final int SELECAO = 0;
    private final int EPI = 1;
    private final int CA = 2;
    private final int QTD = 3;
    private final int PREV_ENTREGA = 4;
    private final int ENTREGA = 5;
    private final int PREV_TROCA = 6;
    private final int STATUS = 7;
    
 
    private final String colunas[] = { "", "EPI", "CA", "Qtd.", "Prev. Entrega", "Entrega", "Prev. Troca", "Status" };
    private List<EntregaVO> dados;
 
    public EntregaTableModel(List<EntregaVO> dados) {
        this.dados=dados;
    }
 
    @Override
    public int getColumnCount() {
        return colunas.length;
    }
 
    @Override
    public int getRowCount() {
    	if (dados != null) {
    		return dados.size();
    	}
    	return 0; 
    }
 
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
	        case SELECAO:
	        	return Boolean.class;
	        case EPI:
	            return String.class;
	        case CA:
	            return String.class;    
	        case QTD:
	            return Integer.class;
	        case PREV_ENTREGA:
	            return Date.class;
	        case ENTREGA:
	            return Date.class;
	        case PREV_TROCA:
	            return Date.class;
	        case STATUS:
	            return String.class;
	        default:
	        	throw new IndexOutOfBoundsException("Coluna inv�lida");
        }
    }
 
    @Override
    public String getColumnName(int columnIndex) {
        return colunas[columnIndex];
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	EntregaVO entregaVO = dados.get(rowIndex);
 
        //retorna o valor da coluna
        switch (columnIndex) {
	        case SELECAO:
	        	return entregaVO.isSelecionado();
	        case EPI:
	            return entregaVO.getEpi();
	        case CA:
	            return entregaVO.getCa();    
	        case QTD:
	            return entregaVO.getQuantidade();
	        case PREV_ENTREGA:
	            return entregaVO.getDataPrevistaEntrega();
	        case ENTREGA:
	        	return entregaVO.getDataEntrega();
	        case PREV_TROCA:
	        	return entregaVO.getDataPrevistaTroca();
	        case STATUS:
	        	return entregaVO.getStatus();
	        default:
	        	throw new IndexOutOfBoundsException("Coluna inv�lida");
        }
    }
 
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	EntregaVO entregaEpiVO = dados.get(rowIndex);
        if (columnIndex == SELECAO && !entregaEpiVO.isSubstituido() && !entregaEpiVO.isDevolvido() && !entregaEpiVO.isCaVencido()) {
            return true;
        }
        return false;
    }
 
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    	EntregaVO entregaVO = dados.get(rowIndex);
 
        if(columnIndex == SELECAO){
        	entregaVO.setSelecionado(((boolean)aValue));
        }
    }
 
    public EntregaVO getValue(int rowIndex){
    	if (rowIndex == -1) {
    		return new EntregaVO();
    	}
    	return dados.get(rowIndex);
    }
 
    public int indexOf(EntregaVO entregaVO) {
        return dados.indexOf(entregaVO);
    }
 
    public void add(EntregaVO entregaVO) {
        dados.add(entregaVO);
        fireTableRowsInserted(indexOf(entregaVO), indexOf(entregaVO));
    }
 
    public void addAll(List<EntregaVO> dados) {
        dados.addAll(dados);
        fireTableDataChanged();
    }
 
    public void remove(int rowIndex) {
        dados.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
 
    public void remove(EntregaVO entregaVO) {
        int indexBefore = indexOf(entregaVO);
        dados.remove(entregaVO);  
        fireTableRowsDeleted(indexBefore, indexBefore);
    }
 
    public void removeAll() {
        dados.clear();
        fireTableDataChanged();
    }
    
    public void selectAll() {
    	for (int i=0;i<dados.size();i++) {
    		dados.get(i).setSelecionado(true);
    	}
    	fireTableDataChanged();
    }
    
    public void deselectAll() {
    	for (int i=0;i<dados.size();i++) {
    		dados.get(i).setSelecionado(false);
    	}
    	fireTableDataChanged();
    }

	public List<EntregaVO> getDados() {
		return dados;
	}

	public void setDados(List<EntregaVO> dados) {
		this.dados = dados;
		fireTableDataChanged();
	}
    
}