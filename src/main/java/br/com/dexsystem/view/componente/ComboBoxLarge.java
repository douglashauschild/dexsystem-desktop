package br.com.dexsystem.view.componente;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.util.Collections;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
 
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ComboBoxLarge extends JComboBox<Object> {
 
    private static final long serialVersionUID = 1L;
    protected int popupWidth;
    
    public ComboBoxLarge() {
        super();
        setUI(new ComboBoxUI());
        popupWidth = 0;
    }
 
    public ComboBoxLarge(int widthPopup) {
        super(Collections.EMPTY_LIST.toArray());
        setUI(new ComboBoxUI());
        popupWidth = 0;
        setPopupWidth(widthPopup);
    }
 
    public ComboBoxLarge(ComboBoxModel aModel) {
        super(aModel);
        setUI(new ComboBoxUI());
        popupWidth = 0;
    }
 
    public ComboBoxLarge(final Object[] items) {
        super(items);
        setUI(new ComboBoxUI());
        popupWidth = 0;
    }
 
    public ComboBoxLarge(Vector items) {
        super(items);
        setUI(new ComboBoxUI());
        popupWidth = 0;
    }
 
    public void setPopupWidth(int width) {
        popupWidth = width;
    }
 
    public Dimension getPopupSize() {
        Dimension size = getSize();
        if (popupWidth < 1)
            popupWidth = size.width;
 
        for (int i = 0; i < super.getItemCount(); i++) {
            FontMetrics font = getGraphics().getFontMetrics();
            if (font.stringWidth(super.getItemAt(i).toString()) > popupWidth) {
                popupWidth = font.stringWidth(super.getItemAt(i).toString());
            }
        }
        return new Dimension((size.getWidth() > popupWidth ? (int) size.getWidth() : (int) popupWidth), size.height);
    }
 
}
