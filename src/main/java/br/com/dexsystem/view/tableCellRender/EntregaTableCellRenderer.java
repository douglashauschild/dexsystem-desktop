package br.com.dexsystem.view.tableCellRender;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import br.com.dexsystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dexsystem.util.FormatacaoUtils;
import br.com.dexsystem.util.LoggerUtils;

public class EntregaTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -7406456013602166946L;
	public static final int TIPO_HEADER = 0;
	public static final int TIPO_COLUMN = 1;
	
	private int tipo;
	private JCheckBox renderer;
	
	public EntregaTableCellRenderer(int tipo) {
		this.tipo = tipo;
		
		renderer = new JCheckBox();
        renderer.setHorizontalAlignment(SwingConstants.CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (tipo == TIPO_HEADER) {
			Component c = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (JLabel.class.isAssignableFrom(c.getClass())) {
	            JLabel label = (JLabel) c;
	            
	            if (column == 2 || column == 3 || column == 4 || column == 5 || column == 6) {
	            	label.setHorizontalAlignment(SwingConstants.CENTER);
	            } else {
	            	label.setHorizontalAlignment(SwingConstants.LEFT);
	            }
	        }
	        return c;
		} else {
			try {
				if (column == 0) {
					Color bg = isSelected ? table.getSelectionBackground() : table.getBackground();
					boolean podeEditar = table.isCellEditable(row, column);
			        renderer.setBackground(bg);
					renderer.setEnabled(podeEditar);
			        renderer.setSelected(value != null && (Boolean)value);
			        
			        if (!podeEditar) {
			        	String tooltip = null;
			        	String valor = table.getModel().getValueAt(row, 7).toString();
			        	if (valor != null && !valor.isEmpty()) {
			        		if (valor.equals(StatusEmpregadoEpiEnum.SUBSTITUIDO.getDescricao()) || valor.equals(StatusEmpregadoEpiEnum.DEVOLVIDO.getDescricao())) {
			        			tooltip = "Esta entrega j� foi realizada e este EPI j� foi substitu�do e/ou devolvido!";
			        		} else {
			        			tooltip = "Este EPI est� com o CA vencido e n�o poder� ser entregue ou substitu�do!";
			        		}
			        	}
			        	if (tooltip != null) {
			        		renderer.setToolTipText(tooltip);
			        	}
			        }
			        return renderer;
				}
				if (column == 4 || column == 5 || column == 6) {
					setText(value != null ? FormatacaoUtils.getDataString((Date) value) : null);
				} else {
					setText(value != null ? value.toString() : null);
				}
				setHorizontalAlignment(SwingConstants.CENTER);
				
				if (isSelected) {
					setForeground(Color.WHITE);
					setBackground(new Color(0,120,215));
	            } else {
	            	Font font = new Font(table.getFont().getName(), table.getFont().getStyle(), table.getFont().getSize());
	            	setFont(font);
	            	setForeground(Color.BLACK);
	            	setBackground(Color.WHITE);
	            }
			} catch(Exception e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
			return this;
		}
    }
}