package br.com.dexsystem.view.tableCellRender;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import br.com.dexsystem.util.LoggerUtils;

public class EmpregadoTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -759533687904771690L;
	public static final int TIPO_HEADER = 0;
	public static final int TIPO_COLUMN = 1;
	
	private int tipo;
	
	public EmpregadoTableCellRenderer(int tipo) {
		this.tipo = tipo;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (tipo == TIPO_HEADER) {
			Component c = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (JLabel.class.isAssignableFrom(c.getClass())) {
	            JLabel label = (JLabel) c;
	            
	            if (column == 0) {
	            	label.setHorizontalAlignment(SwingConstants.CENTER);
	            } else {
	            	label.setHorizontalAlignment(SwingConstants.LEFT);
	            }
	        }
	        return c;
		} else {
			try {
				Font font = new Font(table.getFont().getName(), table.getFont().getStyle(), table.getFont().getSize());
            	setFont(font);
            	
				setText(value != null ? value.toString() : null);
				
				if (column == 0) {
					setHorizontalAlignment(SwingConstants.CENTER);
	            } else {
	            	setHorizontalAlignment(SwingConstants.LEFT);
	            }
				
				if (isSelected) {
					setForeground(Color.WHITE);
	                setBackground(new Color(0,120,215));
	            } else {
	            	setForeground(Color.BLACK);
	            	setBackground(Color.WHITE);
	            }
			} catch(Exception e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
			return this;
		}
    }
}