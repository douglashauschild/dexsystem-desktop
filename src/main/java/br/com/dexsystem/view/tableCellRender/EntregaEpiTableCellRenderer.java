package br.com.dexsystem.view.tableCellRender;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import br.com.dexsystem.model.vo.EntregaEpiVO;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.view.tableModel.EntregaEpiTableModel;

public class EntregaEpiTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = -7406456013602166946L;
	public static final int TIPO_HEADER = 0;
	public static final int TIPO_COLUMN = 1;
	
	private int tipo;
	private JCheckBox renderer;
	
	public EntregaEpiTableCellRenderer(int tipo) {
		this.tipo = tipo;
		
		renderer = new JCheckBox();
        renderer.setHorizontalAlignment(SwingConstants.CENTER);
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (tipo == TIPO_HEADER) {
			Component c = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if (JLabel.class.isAssignableFrom(c.getClass())) {
	            JLabel label = (JLabel) c;
	            
	            if (column == 0 || column == 1 || column == 3) {
	            	label.setHorizontalAlignment(SwingConstants.CENTER);
	            } else {
	            	label.setHorizontalAlignment(SwingConstants.LEFT);
	            }
	        }
	        return c;
		} else {
			try {
				if (column == 0 || column == 1) { 
					Color bg = isSelected ? table.getSelectionBackground() : table.getBackground();
			        renderer.setBackground(bg);
					renderer.setEnabled(table.isCellEditable(row, column));
			        renderer.setSelected(value != null && (Boolean)value);
			        return renderer;
				}
				if (column == 2) {
					String valorColuna = value != null ? value.toString() : null;
					if (valorColuna != null) {
						EntregaEpiVO entregaEpiVO = ((EntregaEpiTableModel) table.getModel()).getDados().get(row);
						String acao = entregaEpiVO.getAcao();
						if (acao.equals("Substitui��o")) {
							if (entregaEpiVO.isDevolver() && !entregaEpiVO.isSubstituir()) {
								entregaEpiVO.setAcao("Devolu��o");
								valorColuna = valorColuna.replaceAll("A��o: Substitui��o", "A��o: Devolu��o");
							} else if (!entregaEpiVO.isDevolver() && !entregaEpiVO.isSubstituir()) {
								valorColuna = valorColuna.replaceAll("A��o: Substitui��o", "A��o: -");
							}
						} else {
							if ((!entregaEpiVO.isDevolver() && entregaEpiVO.isSubstituir()) || (entregaEpiVO.isDevolver() && entregaEpiVO.isSubstituir())) {
								entregaEpiVO.setAcao("Substitui��o");
								valorColuna = valorColuna.replaceAll("A��o: Devolu��o", "A��o: Substitui��o");
							} else if (!entregaEpiVO.isDevolver() && !entregaEpiVO.isSubstituir()) {
								valorColuna = valorColuna.replaceAll("A��o: Devolu��o", "A��o: -");
							}
						}
						setText(valorColuna);
					}
				} else {
					setText(value != null ? value.toString() : null);
				}
				if (column == 3) {
					setHorizontalAlignment(SwingConstants.CENTER);
	            } else {
	            	setHorizontalAlignment(SwingConstants.LEFT);
	            }
				
				if (isSelected) {
					setForeground(Color.WHITE);
	                setBackground(new Color(0,120,215));
	            } else {
	            	Font font = new Font(table.getFont().getName(), table.getFont().getStyle(), table.getFont().getSize());
	            	setFont(font);
	            	setForeground(Color.BLACK);
	            	setBackground(Color.WHITE);
	            }
			} catch(Exception e) {
				LoggerUtils.logErro(e);
				e.printStackTrace();
			}
			return this;
		}
    }
}