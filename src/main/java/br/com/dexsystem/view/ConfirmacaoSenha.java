package br.com.dexsystem.view;

import java.awt.Dialog;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.service.EmpregadoService;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.MensageiroUtils;

public class ConfirmacaoSenha extends JDialog {

	private static final long serialVersionUID = 3072695338494458647L;
	private JButton btnCancelar;
    private JButton btnConfirmar;
    private JLabel lblEmpregado;
    private JLabel lblSenha;
    private JLabel lblTitulo;
    private JTextField txtEmpregado;
    private JPasswordField txtSenha;
    
    private boolean senhaCorreta;
    private CredencialVO credencialVO;
    private EmpregadoVO empregadoSelecionado;
    private EmpregadoService empregadoService;
    
    public ConfirmacaoSenha(Dialog parent, CredencialVO credencialVO) {
        super(parent);
        initComponents();
        
        this.senhaCorreta = false;
        this.credencialVO = credencialVO;
        this.empregadoService = new EmpregadoService();
    }
    
    
    private void initComponents() {
        btnConfirmar = new JButton();
        lblEmpregado = new JLabel();
        lblSenha = new JLabel();
        btnCancelar = new JButton();
        txtEmpregado = new JTextField();
        txtSenha = new JPasswordField();
        lblTitulo = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Confirmação de Entrega"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        setResizable(false);

        btnConfirmar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	confirmar();
            }
        });

        lblEmpregado.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblEmpregado.setText("Empregado");

        lblSenha.setFont(new java.awt.Font("Tahoma", 1, 14)); 
        lblSenha.setText("Senha");

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	cancelar();
            }
        });

        txtEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 

        txtSenha.setFont(new java.awt.Font("Tahoma", 0, 14)); 

        lblTitulo.setFont(new java.awt.Font("Tahoma", 0, 18)); 
        lblTitulo.setText("Confirmação de Entrega");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(btnConfirmar, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                        .addGap(64, 64, 64))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblEmpregado)
                        .addComponent(lblTitulo)
                        .addComponent(lblSenha)
                        .addComponent(txtEmpregado, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                        .addComponent(txtSenha)))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lblTitulo)
                .addGap(25, 25, 25)
                .addComponent(lblEmpregado)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtEmpregado, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblSenha)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSenha, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirmar, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
    
    
    public boolean abrirTela(EmpregadoVO empregadoVO) {
		setModal(true);
		
		this.empregadoSelecionado = empregadoVO;
		carregarTela();
        setVisible(true);
        
        this.empregadoService = null;
        this.empregadoSelecionado = null;
        return senhaCorreta;
    }
    
    private void carregarTela() {
    	txtEmpregado.setText(empregadoSelecionado.getNome());
		txtEmpregado.setEditable(false);
		txtSenha.requestFocus();
    }
    
    private void confirmar() {
		char[] password = txtSenha.getPassword();
		String senha = new String(password);
		
		if (senha == null || senha.isEmpty()) {
			MensageiroUtils.warnMsg(this, "Informe a senha!");
		} else {
			Empregado empregado = empregadoService.buscarPorId(empregadoSelecionado.getId());
			if (empregado != null) {
				senhaCorreta = new BCryptPasswordEncoder().matches(senha, empregado.getSenha());
				if (!senhaCorreta) {
					MensageiroUtils.warnMsg(this, "Senha incorreta!");
					return;
				}
			}
			this.dispose();
		}
    }

    private void cancelar() {
    	senhaCorreta = false;
    	dispose();
    }
}
