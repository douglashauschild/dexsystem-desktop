package br.com.dexsystem.view;

import java.awt.Component;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.Serializable;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

import br.com.dexsystem.Principal;
import br.com.dexsystem.util.ConstantUtils;

public class SeletorDiretorios implements Serializable {

	private static final long serialVersionUID = 6834655739090359140L;

	public String selecionar() {
		String diretorio = null;
		JFileChooser jfc = new JFileChooser() {
			private static final long serialVersionUID = -1048441654258321037L;

			@Override
			   protected JDialog createDialog(Component parent) throws HeadlessException {
			       // intercept the dialog created by JFileChooser
			       JDialog dialog = super.createDialog(parent);
			       dialog.setModal(true); 
			       dialog.setAlwaysOnTop(true);
			       dialog.setTitle(ConstantUtils.NOME_SISTEMA);
			        
			       Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
			       dialog.setIconImage(image);
			       return dialog;
			   }
		};
		jfc.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
		jfc.setDialogTitle("Escolha um diretório para salvar seu arquivo");
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnValue = jfc.showSaveDialog(null);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			diretorio = jfc.getSelectedFile().getAbsolutePath();
		}
		return diretorio;
	}
}
