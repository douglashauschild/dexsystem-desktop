package br.com.dexsystem.view;

import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import br.com.dexsystem.Principal;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.GradeItem;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.EntregaEpiVO;
import br.com.dexsystem.model.vo.EntregaVO;
import br.com.dexsystem.model.vo.EstatisticasEntregaVO;
import br.com.dexsystem.service.EmpregadoEpiService;
import br.com.dexsystem.service.EpiService;
import br.com.dexsystem.service.FichaEpiService;
import br.com.dexsystem.service.GradeItemService;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.tableCellRender.EntregaTableCellRenderer;
import br.com.dexsystem.view.tableModel.EntregaTableModel;

public class Entrega extends JDialog {

	private static final long serialVersionUID = 4942996613181730008L;
	
	private JButton btnEntregar;
    private JButton btnFichaEpi;
    private JButton btnIncluir;
    private JScrollPane jScrollPane1;
    private JLabel lblAguardandoEntrega;
    private JLabel lblEmDia;
    private JLabel lblEpisVencidos;
    private JLabel lblCargoEmpregado;
    private JLabel lblIdadeEmpregado;
    private JLabel lblMatriculaEmpregado;
    private JLabel lblNomeEmpregado;
    private JLabel lblQtdAguardandoEntrega;
    private JLabel lblQtdEmDia;
    private JLabel lblQtdVencidos;
    private JLabel lblSetorEmpregado;
    private JPanel panelEstatisticas;
    private JPanel panelInformacoes;
    private JPanel panelTabelaEntrega;
    private JTable tblEmpregadoEpi;
    private JLabel lblFiltros;
    private JCheckBox chkDevolvidos;
    private JCheckBox chkSubstituidos;
    private JLabel lblEntregasAtrasadas;
    private JLabel lblQtdEntregasAtrasadas;
    
    private CredencialVO credencialVO;
    private EpiService epiService;
    private GradeItemService gradeItemService;
    private EmpregadoEpiService empregadoEpiService;
    private FichaEpiService fichaEpiService;
    private EmpregadoVO empregadoSelecionado;
    

    public Entrega(Frame parent, CredencialVO credencialVO) {
        super(parent);
        initComponents();
        
        this.credencialVO = credencialVO;
        this.epiService = new EpiService();
        this.gradeItemService = new GradeItemService();
        this.empregadoEpiService = new EmpregadoEpiService();
        this.fichaEpiService = new FichaEpiService(parent);
    }

    
	private void initComponents() {
        panelTabelaEntrega = new JPanel();
        jScrollPane1 = new JScrollPane();
        tblEmpregadoEpi = new JTable();
        panelEstatisticas = new JPanel();
        lblAguardandoEntrega = new JLabel();
        lblEmDia = new JLabel();
        lblEpisVencidos = new JLabel();
        lblQtdAguardandoEntrega = new JLabel();
        lblQtdEmDia = new JLabel();
        lblQtdVencidos = new JLabel();
        btnIncluir = new JButton();
        btnEntregar = new JButton();
        btnFichaEpi = new JButton();
        panelInformacoes = new JPanel();
        lblNomeEmpregado = new JLabel();
        lblIdadeEmpregado = new JLabel();
        lblMatriculaEmpregado = new JLabel();
        lblSetorEmpregado = new JLabel();
        lblCargoEmpregado = new JLabel();
        chkDevolvidos = new JCheckBox();
        chkSubstituidos = new JCheckBox();
        lblFiltros = new JLabel();
        lblEntregasAtrasadas = new JLabel();
        lblQtdEntregasAtrasadas = new JLabel();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
        	public void windowClosing(WindowEvent e) {
        		fecharTela();
        	}
        });
        
        setAlwaysOnTop(true);
        setMaximumSize(new java.awt.Dimension(321, 275));
        setTitle(ConstantUtils.NOME_SISTEMA);
        setName("Entrega"); 
        
        Image image = Toolkit.getDefaultToolkit().getImage(Principal.class.getClassLoader().getResource(ConstantUtils.ICONE_SISTEMA));
        setIconImage(image);
        
        tblEmpregadoEpi.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        tblEmpregadoEpi.setModel(new EntregaTableModel(new ArrayList<>()));
        jScrollPane1.setViewportView(tblEmpregadoEpi);

        javax.swing.GroupLayout panelTabelaEntregaLayout = new javax.swing.GroupLayout(panelTabelaEntrega);
        panelTabelaEntrega.setLayout(panelTabelaEntregaLayout);
        panelTabelaEntregaLayout.setHorizontalGroup(
            panelTabelaEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        panelTabelaEntregaLayout.setVerticalGroup(
            panelTabelaEntregaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
        );

        panelEstatisticas.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Estat�sticas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); 

        lblAguardandoEntrega.setFont(new java.awt.Font("Tahoma", 0, 12)); 
        lblAguardandoEntrega.setText("Aguardando entrega");

        lblEmDia.setFont(new java.awt.Font("Tahoma", 0, 12)); 
        lblEmDia.setText("EPIs em dia");

        lblEpisVencidos.setFont(new java.awt.Font("Tahoma", 0, 12)); 
        lblEpisVencidos.setText("EPIs vencidos");

        lblQtdAguardandoEntrega.setFont(new java.awt.Font("Tahoma", 0, 24)); 
        lblQtdAguardandoEntrega.setText("0");

        lblQtdEmDia.setFont(new java.awt.Font("Tahoma", 0, 24)); 
        lblQtdEmDia.setText("0");

        lblQtdVencidos.setFont(new java.awt.Font("Tahoma", 0, 24)); 
        lblQtdVencidos.setText("0");
        
        lblEntregasAtrasadas.setFont(new java.awt.Font("Tahoma", 0, 12));
        lblEntregasAtrasadas.setText("Entregas atrasadas");

        lblQtdEntregasAtrasadas.setFont(new java.awt.Font("Tahoma", 0, 24));
        lblQtdEntregasAtrasadas.setText("0");

        javax.swing.GroupLayout panelEstatisticasLayout = new javax.swing.GroupLayout(panelEstatisticas);
        panelEstatisticas.setLayout(panelEstatisticasLayout);
        panelEstatisticasLayout.setHorizontalGroup(
            panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEstatisticasLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAguardandoEntrega)
                    .addComponent(lblQtdAguardandoEntrega))
                .addGap(32, 32, 32)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblQtdEntregasAtrasadas)
                    .addComponent(lblEntregasAtrasadas))
                .addGap(32, 32, 32)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblQtdVencidos)
                    .addComponent(lblEpisVencidos))
                .addGap(32, 32, 32)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblEmDia)
                    .addComponent(lblQtdEmDia))
                .addGap(20, 20, 20))
        );
        panelEstatisticasLayout.setVerticalGroup(
            panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEstatisticasLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAguardandoEntrega)
                    .addComponent(lblEmDia)
                    .addComponent(lblEpisVencidos)
                    .addComponent(lblEntregasAtrasadas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelEstatisticasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblQtdAguardandoEntrega)
                    .addComponent(lblQtdEmDia)
                    .addComponent(lblQtdVencidos)
                    .addComponent(lblQtdEntregasAtrasadas))
                .addContainerGap())
        );

        btnIncluir.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnIncluir.setText("Incluir");
        btnIncluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                incluirEpi();
            }
        });

        btnEntregar.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnEntregar.setText("Entregar/Devolver");
        btnEntregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entregar();
            }
        });

        btnFichaEpi.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        btnFichaEpi.setText("Gerar Ficha de EPI");
        btnFichaEpi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gerarFichaEpi();
            }
        });

        panelInformacoes.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Empregado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); 

        lblNomeEmpregado.setFont(new java.awt.Font("Tahoma", 0, 18)); 
        lblNomeEmpregado.setText("Nome");

        lblIdadeEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblIdadeEmpregado.setText("Idade:");

        lblMatriculaEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblMatriculaEmpregado.setText("Matr�cula:");

        lblSetorEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblSetorEmpregado.setText("Setor:");

        lblCargoEmpregado.setFont(new java.awt.Font("Tahoma", 0, 14)); 
        lblCargoEmpregado.setText("Cargo:");

        javax.swing.GroupLayout panelInformacoesLayout = new javax.swing.GroupLayout(panelInformacoes);
        panelInformacoes.setLayout(panelInformacoesLayout);
        panelInformacoesLayout.setHorizontalGroup(
            panelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInformacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNomeEmpregado)
                    .addComponent(lblIdadeEmpregado)
                    .addComponent(lblMatriculaEmpregado)
                    .addComponent(lblSetorEmpregado)
                    .addComponent(lblCargoEmpregado))
                .addContainerGap(513, Short.MAX_VALUE))
        );
        panelInformacoesLayout.setVerticalGroup(
            panelInformacoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelInformacoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNomeEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblIdadeEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMatriculaEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSetorEmpregado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(lblCargoEmpregado)
                .addContainerGap())
        );

        chkDevolvidos.setFont(new java.awt.Font("Tahoma", 0, 14));
        chkDevolvidos.setText("EPIs devolvidos");
        chkDevolvidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popularTabela();
            }
        });

        chkSubstituidos.setFont(new java.awt.Font("Tahoma", 0, 14));
        chkSubstituidos.setText("EPIs substitu�dos");
        chkSubstituidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                popularTabela();
            }
        });

        lblFiltros.setFont(new java.awt.Font("Tahoma", 0, 14));
        lblFiltros.setText("Filtros:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEntregar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFichaEpi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 356, Short.MAX_VALUE)
                        .addComponent(lblFiltros)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(chkSubstituidos)
                        .addGap(6, 6, 6)
                        .addComponent(chkDevolvidos))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(panelInformacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelEstatisticas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelTabelaEntrega, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelEstatisticas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelInformacoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnIncluir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEntregar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFichaEpi, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkDevolvidos)
                    .addComponent(chkSubstituidos)
                    .addComponent(lblFiltros))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelTabelaEntrega, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        pack();
        this.setLocationRelativeTo(null);
    }
	
	
	public void abrirTela(EmpregadoVO empregadoVO) {
		setModal(true);
		
		this.empregadoSelecionado = empregadoVO;
		montarInformacoesEmpregado();
		carregarInformacoes();
		
        setVisible(true);
        
        this.epiService = null;
        this.gradeItemService = null;
        this.empregadoEpiService = null;
        this.empregadoSelecionado = null;
        this.fichaEpiService = null;
    }

	
	private void carregarInformacoes() {
		carregarEstatisticas();
		popularTabela();
	}
	
	private void montarInformacoesEmpregado() {
		lblNomeEmpregado.setText(empregadoSelecionado.getNome());
		lblIdadeEmpregado.setText("Idade: " + empregadoSelecionado.getIdade());
	    lblMatriculaEmpregado.setText("Matr�cula: " + empregadoSelecionado.getMatricula());
	    lblSetorEmpregado.setText("Setor: " + empregadoSelecionado.getSetor());
	    lblCargoEmpregado.setText("Cargo: " + empregadoSelecionado.getCargo());
    }
	
	private void carregarEstatisticas() {
		EstatisticasEntregaVO estatisticasEntregaVO = empregadoEpiService.buscarEstatisticas(empregadoSelecionado.getId());
		if (estatisticasEntregaVO != null) {
			lblQtdAguardandoEntrega.setText(estatisticasEntregaVO.getAguardandoEntrega().toString());
			lblQtdEmDia.setText(estatisticasEntregaVO.getEpiDia().toString());
			lblQtdVencidos.setText(estatisticasEntregaVO.getEpiVencido().toString());
			lblQtdEntregasAtrasadas.setText(estatisticasEntregaVO.getEntregasAtrasadas().toString());
		}
	}
	
	private void popularTabela() {
		List<EntregaVO> entregaVO = empregadoEpiService.buscarParaTabela(empregadoSelecionado.getId(), chkSubstituidos.isSelected(), chkDevolvidos.isSelected());
    	((EntregaTableModel) tblEmpregadoEpi.getModel()).setDados(entregaVO);
    	tblEmpregadoEpi.setRowHeight(25); 
    	tblEmpregadoEpi.setAutoCreateRowSorter(true);
    	
    	ajustarColunasEpis(tblEmpregadoEpi);
	}
	
	private void ajustarColunasEpis(JTable tabela) {
		tabela.getColumnModel().getColumn(1).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(2).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(3).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(4).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(5).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(6).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		tabela.getColumnModel().getColumn(7).setHeaderRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_HEADER));
		
		tabela.getColumnModel().getColumn(0).setPreferredWidth(5);
		tabela.getColumnModel().getColumn(0).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(1).setPreferredWidth(260);
		tabela.getColumnModel().getColumn(2).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(2).setPreferredWidth(20);
		tabela.getColumnModel().getColumn(3).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(3).setPreferredWidth(5);
		tabela.getColumnModel().getColumn(4).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(4).setPreferredWidth(15);
		tabela.getColumnModel().getColumn(5).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(5).setPreferredWidth(15);
		tabela.getColumnModel().getColumn(6).setCellRenderer(new EntregaTableCellRenderer(EntregaTableCellRenderer.TIPO_COLUMN));
		tabela.getColumnModel().getColumn(6).setPreferredWidth(15);
		tabela.getColumnModel().getColumn(7).setPreferredWidth(50);
	}
	
	private void incluirEpi() {
		Epi epi = new Epi(this, credencialVO);
		boolean cadastrou = epi.abrirTela(empregadoSelecionado);
		if (cadastrou) {
			carregarInformacoes();
		}
		epi = null;
	}
	
	private void entregar() {
		EntregaEpi entregaEpi = new EntregaEpi(this, credencialVO);
		List<EntregaEpiVO> epis = getEpisSelecionadosParaEntrega();
		if (epis == null || epis.isEmpty()) {
			MensageiroUtils.warnMsg(this, "Selecione pelo menos um registro para a entrega!");
			return;
		}
		boolean atualizarLista = entregaEpi.abrirTela(empregadoSelecionado, epis);
		if (atualizarLista) {
			carregarInformacoes();
		}
		entregaEpi = null;
	}
	
	private void gerarFichaEpi() {
		fichaEpiService.gerarFichaEpi(empregadoSelecionado, credencialVO.getNome());
	}

	private List<EntregaEpiVO> getEpisSelecionadosParaEntrega() {
		List<EntregaEpiVO> episSelecionados = new ArrayList<EntregaEpiVO>();
		try {
			List<EntregaVO> episEntrega = ((EntregaTableModel) tblEmpregadoEpi.getModel()).getDados();
			for (EntregaVO entregaVO : episEntrega) {
				if (entregaVO.isSelecionado()) {
					EmpregadoEpi empregadoEpi = empregadoEpiService.buscarPorId(entregaVO.getId());
					if (empregadoEpi != null) {
						br.com.dexsystem.model.Epi epi = epiService.buscarPorId(entregaVO.getIdEpi());
						GradeItem gradeItem = null;
						if (empregadoEpi.getIdGradeItem() != null) {
							gradeItem = gradeItemService.buscarPorId(empregadoEpi.getIdGradeItem());
						}
						episSelecionados.add(new EntregaEpiVO(entregaVO.getId(), entregaVO.getIdEpi(), entregaVO.getEpi(), entregaVO.getCa(), empregadoEpi.getAcaoEntrega(), entregaVO.getQuantidade(), epi.getIdGrade(), gradeItem));
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return episSelecionados;
	}
	
	private void fecharTela() {
		String opcoes[] = { "Sim", "N�o" };
		String msg = "Voc� tem certeza que deseja encerrar as entregas para este empregado?";
		int confirmacao = MensageiroUtils.confirmMsg(this, msg, opcoes, opcoes[0]);
		if (confirmacao == 0) {
			dispose();
		} else {
			return;
		}
	}
}
