package br.com.dexsystem.service;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.model.vo.TokenVO;
import br.com.dexsystem.service.client.TokenClientService;
import br.com.dexsystem.util.CriptografiaUtils;

public class TokenService {
	
	public TokenVO obterToken(String email, String senha) throws HttpClientErrorException, Exception {
		System.out.println("Obtendo token...");
		senha = CriptografiaUtils.encrypt(senha, CriptografiaUtils.CHAVE);
		return new TokenClientService().obterToken(new TokenVO(email, senha));
	}
}
