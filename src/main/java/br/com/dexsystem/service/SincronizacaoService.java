package br.com.dexsystem.service;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.model.vo.SincronizacaoVO;
import br.com.dexsystem.service.client.SincronizacaoClientService;

public class SincronizacaoService {
	
	
	public SincronizacaoVO sincronizar(String token, SincronizacaoVO sincronizacaoVO) throws HttpClientErrorException, Exception {
		return new SincronizacaoClientService().sincronizar(token, sincronizacaoVO);
	}
}
