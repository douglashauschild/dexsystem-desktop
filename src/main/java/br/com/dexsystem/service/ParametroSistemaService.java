package br.com.dexsystem.service;

import br.com.dexsystem.dao.ParametroSistemaDao;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.Empresa;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.ParametroSistema;
import br.com.dexsystem.model.PessoaFisica;

public class ParametroSistemaService {

	private ParametroSistemaDao parametroSistemaDao;
	

	public ParametroSistemaService() {
		super();
		this.parametroSistemaDao = new ParametroSistemaDao();
	}

	public ParametroSistema buscar() {
		return parametroSistemaDao.buscarUnico();
	}
	

	public String getSelecaoEmpresa(Empresa empresa) {
		ParametroSistema parametroSistema = buscar();
		String nome = empresa.getPessoaJuridica().getPessoa().getNome();
		if (!parametroSistema.isSelecaoEmpresaSimples()) {
			nome = "CNPJ: "+ empresa.getPessoaJuridica().getCnpjFormatado() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoEmpregado(Empregado empregado) {
		ParametroSistema parametroSistema = buscar();
		String nome = empregado.getPessoaFisica().getPessoa().getNome();
		if (!parametroSistema.isSelecaoEmpregadoSimples()) {
			nome = "Matr.: "+ empregado.getMatricula() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoPessoaFisica(PessoaFisica pessoaFisica) {
		ParametroSistema parametroSistema = buscar();
		String nome = pessoaFisica.getPessoa().getNome();
		if (!parametroSistema.isSelecaoPessoaFisicaSimples()) {
			nome = "CPF: "+ pessoaFisica.getCpfFormatado() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoEpi(Epi epi) {
		ParametroSistema parametroSistema = buscar();
		String nome = epi.getNome();
		String ca = epi.getCa() != null && !epi.getCa().isEmpty() ? epi.getCa() : " ";
		if (!parametroSistema.isSelecaoEpiSimples()) {
			nome = "CA: "+ ca + " | " + nome;
		}
		return nome;
	}
}
