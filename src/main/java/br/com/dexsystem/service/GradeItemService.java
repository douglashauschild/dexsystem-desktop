package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.List;

import br.com.dexsystem.dao.GradeItemDao;
import br.com.dexsystem.model.GradeItem;
import br.com.dexsystem.model.vo.SelectVO;

public class GradeItemService {

	private GradeItemDao gradeItemDao;

	public GradeItemService() {
		super();
		this.gradeItemDao = new GradeItemDao();
	}
	
	public GradeItem buscarPorId(Long idGradeItem) {
		return gradeItemDao.buscarPorId(idGradeItem);
	}
	
	public List<SelectVO> getGradeItensCombo(Long idGrade) {
		List<SelectVO> motivosVO = new ArrayList<SelectVO>();
		List<GradeItem> gradeItens = gradeItemDao.buscarPorIdGrade(idGrade);
		for (GradeItem gradeItem : gradeItens) {
			motivosVO.add(new SelectVO(gradeItem.getId(), gradeItem.getTamanho()));
		}
		return motivosVO;
	}
	
}

