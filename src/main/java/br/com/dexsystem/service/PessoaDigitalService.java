package br.com.dexsystem.service;

import java.util.List;

import br.com.dexsystem.dao.PessoaDigitalDao;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.service.sincronizacao.SincronizacaoDadosAbstract;

public class PessoaDigitalService extends SincronizacaoDadosAbstract<PessoaDigital> {

	private PessoaDigitalDao pessoaDigitalDao;

	public PessoaDigitalService() {
		super();
		this.pessoaDigitalDao = new PessoaDigitalDao();
	}
	
	public void gravar(PessoaDigital pessoaDigital) throws Exception {
		pessoaDigitalDao.gravar((PessoaDigital) gerarChecksum(pessoaDigital.getId() == null, pessoaDigital));
	}
	
	public List<PessoaDigital> buscarPorIdPessoa(Long idPessoa) {
		return pessoaDigitalDao.buscarPorIdPessoa(idPessoa);
	}
	
	public PessoaDigital buscarDigital(Long idPessoa, String posicao) {
		return pessoaDigitalDao.buscarDigital(idPessoa, posicao);
	}
	
	public boolean isPossuiDigital(Long idPessoa) {
		boolean possui = false;
		List<PessoaDigital> digitais = buscarPorIdPessoa(idPessoa);
		if (digitais != null) {
			for (PessoaDigital pessoaDigital : digitais) {
				if (pessoaDigital.getDigital() != null && !pessoaDigital.isAnulado()) {
					possui = true;
					break;
				}
			}
		}
		return possui;
	}
}
