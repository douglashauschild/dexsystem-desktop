package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.List;

import br.com.dexsystem.dao.MotivoDao;
import br.com.dexsystem.model.Motivo;
import br.com.dexsystem.model.vo.SelectVO;

public class MotivoService  {

	private MotivoDao motivoDao;

	public MotivoService() {
		super();
		this.motivoDao = new MotivoDao();
	}
	
	public List<SelectVO> getMotivosCombo() throws Exception {
		List<SelectVO> motivosVO = new ArrayList<SelectVO>();
		List<Motivo> motivos = motivoDao.buscarTodos();
		for (Motivo motivo : motivos) {
			motivosVO.add(new SelectVO(motivo.getId(), motivo.getDescricao()));
		}
		return motivosVO;
	}
}
