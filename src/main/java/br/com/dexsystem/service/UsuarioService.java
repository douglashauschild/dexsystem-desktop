package br.com.dexsystem.service;

import br.com.dexsystem.dao.UsuarioDao;
import br.com.dexsystem.model.Usuario;

public class UsuarioService {
	
	private UsuarioDao usuarioDao;
	
	public UsuarioService() {
		super();
		this.usuarioDao = new UsuarioDao();
	}

	public Usuario getUsuario(String email) {
		return usuarioDao.buscarPorEmail(email);
	}
}
