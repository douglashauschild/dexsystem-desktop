package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.List;

import br.com.dexsystem.dao.AtividadeDao;
import br.com.dexsystem.dao.EmpregadoDao;
import br.com.dexsystem.model.Atividade;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.vo.DigitalVO;
import br.com.dexsystem.model.vo.EmpregadoVO;

public class EmpregadoService {

	private EmpregadoDao empregadoDao;
	private AtividadeDao atividadeDao;

	public EmpregadoService() {
		super();
		this.empregadoDao = new EmpregadoDao();
		this.atividadeDao = new AtividadeDao();
	}
	
	
	public Empregado buscarPorId(Long idEmpregado) {
		return empregadoDao.buscarPorId(idEmpregado);
	}
	
	
	public List<Empregado> buscarAtivos(Long idEmpresa) {
		return empregadoDao.buscarAtivosPorEmpresa(idEmpresa);
	}
	
	
	public List<EmpregadoVO> buscarParaTabela(Long idEmpresa) {
		List<Empregado> empregados = buscarAtivos(idEmpresa);
		return montarListaEmpregadoVO(empregados);
	}
	
	
	public List<EmpregadoVO> buscarPosPesquisaDigital(List<DigitalVO> empregadosEncontrados) {
		List<Long> ids = new ArrayList<Long>();
		for (DigitalVO digital : empregadosEncontrados) {
			ids.add(digital.getIdEmpregado());
		}
		List<Empregado> empregados = empregadoDao.buscarAtivosPorIds(ids);
		return montarListaEmpregadoVO(empregados);
	}
	
	
	private List<EmpregadoVO> montarListaEmpregadoVO(List<Empregado> empregados) {
		List<EmpregadoVO> empregadosVO = new ArrayList<EmpregadoVO>();
		for (Empregado empregado : empregados) {
			
			String setor = "Empregado sem atividade cadastrada";
			String cargo = "Empregado sem atividade cadastrada";
			Atividade atividade = atividadeDao.buscarAtivaPorEmpregado(empregado);
			if (atividade != null) {
				setor = atividade.getCargo().getSetor().getNome();
				cargo = atividade.getCargo().getNome();
			}
			empregadosVO.add(
					new EmpregadoVO(
							empregado.getId(), 
							empregado.getIdPessoa(),
							empregado.getEmpresa().getPessoaJuridica().getPessoa().getNome(), 
							empregado.getPessoaFisica().getPessoa().getNome(), 
							empregado.getPessoaFisica().getIdade(), 
							empregado.getMatricula(),
							empregado.getPessoaFisica().getCpfFormatado(), 
							empregado.getPessoaFisica().getRgFormatado(), 
							empregado.getDataAdmissaoFormatada(), 
							empregado.getDataDemissaoFormatada(), 
							setor, 
							cargo,
							empregado.getSenha()
					)
			);
		}
		return empregadosVO;
	}
}
