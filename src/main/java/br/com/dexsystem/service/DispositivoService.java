package br.com.dexsystem.service;

import java.util.Date;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.DispositivoDao;
import br.com.dexsystem.model.Dispositivo;
import br.com.dexsystem.service.client.DispositivoClientService;
import br.com.dexsystem.util.VersaoUtils;

public class DispositivoService {
	
	private DispositivoDao dispositivoDao;
	
	
	public DispositivoService() {
		super();
		this.dispositivoDao = new DispositivoDao();
	}

	
	public Dispositivo gerarDispositivo(String token) throws HttpClientErrorException, Exception {
		return new DispositivoClientService().gerarDispositivo(token, VersaoUtils.getVersao());
	}
	

	public Dispositivo geDispositivo() {
		return dispositivoDao.buscarUnico();
	}
	
	
	public void atualizarDataSincronizacao() {
		Dispositivo dispositivo = geDispositivo();
		if (dispositivo != null) {
			dispositivo.setDataSincronizacao(new Date());
			dispositivoDao.salvar(dispositivo);
		}
		dispositivo = null;
	}
}
