package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.dexsystem.dao.EmpregadoEpiDao;
import br.com.dexsystem.dao.EpiDao;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dexsystem.model.constant.VidaUtilEnum;
import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.EntregaEpiVO;
import br.com.dexsystem.model.vo.EntregaVO;
import br.com.dexsystem.model.vo.EstatisticasEntregaVO;
import br.com.dexsystem.service.sincronizacao.SincronizacaoDadosAbstract;
import br.com.dexsystem.util.FormatacaoUtils;

public class EmpregadoEpiService extends SincronizacaoDadosAbstract<EmpregadoEpi> {

	private EpiDao epiDao;
	private EmpregadoEpiDao empregadoEpiDao;

	public EmpregadoEpiService() {
		super();
		this.epiDao = new EpiDao();
		this.empregadoEpiDao = new EmpregadoEpiDao();
	}
	
	public void gravar(EmpregadoEpi empregadoEpi) throws Exception {
		empregadoEpiDao.gravar((EmpregadoEpi) gerarChecksum(empregadoEpi.getId() == null, empregadoEpi));
	}
	
	public EmpregadoEpi buscarPorId(Long id) throws Exception {
		return empregadoEpiDao.buscarPorId(id);
	}
	
	public EstatisticasEntregaVO buscarEstatisticas(Long idEmpregado) {
		int aguardando = 0;
		int emDia = 0;
		int vencido = 0;
		int entregasAtrasadas = 0;
		
		try {
			List<EmpregadoEpi> lista = empregadoEpiDao.buscarParaEstatisticas(idEmpregado);
			for (EmpregadoEpi empregadoEpi : lista) {
				if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.AGUARDANDO.getDescricao())) {
					aguardando++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.EM_DIA.getDescricao())) {
					emDia++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.VENCIDO.getDescricao())) {
					vencido++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getDescricao())) {
					entregasAtrasadas++;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new EstatisticasEntregaVO(aguardando, emDia, vencido, entregasAtrasadas);
	}
	
	
	public List<EntregaVO> buscarParaTabela(Long idEmpregado, boolean substituidos, boolean devolvidos) {
		List<EntregaVO> entregaVO = new ArrayList<EntregaVO>();
		List<EmpregadoEpi> empregadoEpis = empregadoEpiDao.buscarParaTabela(idEmpregado, substituidos, devolvidos, true, true, true, true);
		for (EmpregadoEpi empregadoEpi : empregadoEpis) {
			String status = "";
			try {
				status = empregadoEpi.getStatusDescricao();
			} catch(Exception e) {
				e.printStackTrace();
			}
			entregaVO.add(new EntregaVO(
							empregadoEpi.getId(), 
							empregadoEpi.getIdEpi(), 
							empregadoEpi.getEpi().getNome(), 
							empregadoEpi.getEpi().getCa(), 
							empregadoEpi.getQuantidade(), 
							empregadoEpi.getDataPrevistaEntrega(), 
							empregadoEpi.getDataEntrega(), 
							empregadoEpi.getDataPrevistaTroca(), 
							status,
							empregadoEpi.getDataSubstituicao(),
							empregadoEpi.getDataDevolucao(),
							empregadoEpi.getEpi().isCaVencido()));
		}
		return entregaVO;
	}
	
	
	public List<EmpregadoEpi> buscarParaFichaEpi(Long idEmpregado) {
		return empregadoEpiDao.buscarParaFichaEpi(idEmpregado);
	}
	
	
	public void confirmarEntrega(EmpregadoVO empregadoVO, List<EntregaEpiVO> epis, Character tipoConfirmacao) throws Exception {
		Date dataAtual = new Date();
		for (EntregaEpiVO empregadoEpiLista : epis) {
			EmpregadoEpi empregadoEpi = buscarPorId(empregadoEpiLista.getId());
			if (empregadoEpi.getDataConfirmacao() == null) {
				confirmar(empregadoEpi, empregadoEpiLista, dataAtual, tipoConfirmacao);
			} else if (empregadoEpiLista.isSubstituir()) {
				substituir(empregadoEpi, empregadoEpiLista, dataAtual, tipoConfirmacao);
			} else if (empregadoEpiLista.isDevolver() && !empregadoEpiLista.isSubstituir()) {
				devolver(empregadoEpi, dataAtual);
			}
		}
	}
	
	private void confirmar(EmpregadoEpi empregadoEpi, EntregaEpiVO empregadoEpiLista, Date dataAtual, Character tipoConfirmacao) throws Exception {
		empregadoEpi.setDataConfirmacao(dataAtual);
		empregadoEpi.setDataEntrega(dataAtual);
		empregadoEpi.setTipoConfirmacao(tipoConfirmacao);
		empregadoEpi.setDataPrevistaTroca(getDataPrevistaTroca(empregadoEpi));
		if (empregadoEpiLista.getTamanho().getId() != null && empregadoEpiLista.getTamanho().getId() != 0L) {
			empregadoEpi.setIdGradeItem(empregadoEpiLista.getTamanho().getId());
		}
		if (empregadoEpiLista.getMotivo().getId() != null && empregadoEpiLista.getMotivo().getId() != 0L) {
			empregadoEpi.setIdMotivo(empregadoEpiLista.getMotivo().getId());
		}
		gravar(empregadoEpi);
	}
	
	private void substituir(EmpregadoEpi empregadoEpi, EntregaEpiVO empregadoEpiLista, Date dataAtual, Character tipoConfirmacao) throws Exception {
		EmpregadoEpi novoEmpregadoEpi = empregadoEpi.clone();
		novoEmpregadoEpi.setId(null);
		novoEmpregadoEpi.setDataPrevistaEntrega(dataAtual);
		novoEmpregadoEpi.setDataEntrega(null);
		novoEmpregadoEpi.setDataPrevistaTroca(null);
		novoEmpregadoEpi.setInclusaoManual(null);
		
		empregadoEpi.setDataSubstituicao(dataAtual);
		if (empregadoEpiLista.isDevolver()) {
			empregadoEpi.setDataDevolucao(dataAtual);
		}
		gravar(empregadoEpi);
		confirmar(novoEmpregadoEpi, empregadoEpiLista, dataAtual, tipoConfirmacao);
	}
	
	public void devolver(EmpregadoEpi empregadoEpi, Date dataAtual) throws Exception {
		empregadoEpi.setDataDevolucao(dataAtual);
		gravar(empregadoEpi);
	}
	
	public Date getDataPrevistaTroca(EmpregadoEpi empregadoEpi) {
		return getDataPrevistaTroca(empregadoEpi.getIdEpi(), empregadoEpi.getQuantidade(), empregadoEpi.getDataEntrega());
	}
	public Date getDataPrevistaTroca(Long idEpi, Integer qtd, Date dataBase) {
		Epi epi = epiDao.buscarPorId(idEpi);
		if (epi != null) {
			Integer vidaUtil = epi.getVidaUtil();
			Character vidaUtilUn = epi.getVidaUtilUnidade();
			if (vidaUtilUn != null && vidaUtil != null) {
				vidaUtil = vidaUtil * qtd;
				if (vidaUtilUn.equals(VidaUtilEnum.DIA.getKey())) {
					return FormatacaoUtils.incluirDia(dataBase, vidaUtil);
				} else if (vidaUtilUn.equals(VidaUtilEnum.MES.getKey())) {
					return FormatacaoUtils.incluirMes(dataBase, vidaUtil);
				} else if (vidaUtilUn.equals(VidaUtilEnum.ANO.getKey())) {
					return FormatacaoUtils.incluirAno(dataBase, vidaUtil);
				}
			}
		}
		return null;
	}
	
}
