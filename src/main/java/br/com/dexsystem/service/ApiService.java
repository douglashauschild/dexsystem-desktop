package br.com.dexsystem.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;

import org.hibernate.type.BinaryType;
import org.hibernate.type.ByteType;
import org.hibernate.type.CharacterType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.hibernate.type.Type;

import br.com.dexsystem.dao.ApiDao;
import br.com.dexsystem.model.Sincronizar;
import br.com.dexsystem.model.vo.ColunaVO;

public class ApiService {
	
	private ApiDao apiDao;
	
	
	public ApiService() {
		super();
		this.apiDao = new ApiDao();
	}


	public List<Object> buscarDados(String tabela, Class clazz) {
		List<Object> objects = apiDao.buscarDados(tabela, getColunas(clazz), clazz);
		return objects;
	}

	private List<ColunaVO> getColunas(Class clazz) {
		List<ColunaVO> colunaVO = new ArrayList<ColunaVO>();
		return getColunas(clazz, colunaVO);
	}
	
	private List<ColunaVO> getColunas(Class clazz, List<ColunaVO> colunasVO) {
		try {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.isAnnotationPresent(Sincronizar.class)) {
					colunasVO.add(new ColunaVO(field.getAnnotation(Column.class).name(), field.getName(), getTipo(field)));
				}
				if (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class)) {
		            getColunas(clazz.getSuperclass(), colunasVO);
		        }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return colunasVO;
	}
	
	private Type getTipo(Field field) {
		Type tipo = StringType.INSTANCE;
		if (field.isAnnotationPresent(Temporal.class)) {
			if (field.getAnnotation(Temporal.class).value().name().equals("DATE")) {
				tipo = DateType.INSTANCE;
			} else if (field.getAnnotation(Temporal.class).value().name().equals("TIMESTAMP")) {
				tipo = TimestampType.INSTANCE;
			}
		} else if (field.getType().equals(Character.class)) {
			tipo = CharacterType.INSTANCE;
		} else if (field.getType().equals(Long.class)) {
			tipo = LongType.INSTANCE;
		} else if (field.getType().equals(Integer.class)) {
			tipo = IntegerType.INSTANCE;
		} else if (field.getType().equals(Double.class)) {
			tipo = DoubleType.INSTANCE;
		} else if (field.getType().equals(Float.class)) {
			tipo = FloatType.INSTANCE;
		} else if (field.getType().equals(Byte.class)) {
			tipo = ByteType.INSTANCE;
		} else if (field.getType().equals(byte[].class)) {
			tipo = BinaryType.INSTANCE;
		}
		return tipo;
	}

}
