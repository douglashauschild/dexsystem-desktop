package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.List;

import br.com.dexsystem.dao.EpiDao;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.vo.SelectVO;

public class EpiService {

	private EpiDao epiDao;
	private ParametroSistemaService parametroSistemaService;

	public EpiService() {
		super();
		this.epiDao = new EpiDao();
		this.parametroSistemaService = new ParametroSistemaService();
	}
	
	public Epi buscarPorId(Long id) {
		return epiDao.buscarPorId(id);
	}
	
	public List<SelectVO> getEpisCombo() {
		List<SelectVO> episVO = new ArrayList<SelectVO>();
		List<Epi> epis = epiDao.buscarTodos();
		for (Epi epi : epis) {
			episVO.add(new SelectVO(epi.getId(), parametroSistemaService.getSelecaoEpi(epi)));
		}
		return episVO;
	}
	
}
