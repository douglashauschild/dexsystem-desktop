package br.com.dexsystem.service;

import java.awt.Frame;

import br.com.dexsystem.model.vo.EmpregadoVO;
import br.com.dexsystem.model.vo.FichaEpiVO;
import br.com.dexsystem.util.FileUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.SeletorDiretorios;
import br.com.dexsystem.view.pdfView.FichaEpiView;

public class FichaEpiService {
	
	private Frame parent;
	private EmpregadoEpiService empregadoEpiService;
	
	public FichaEpiService(Frame parent) {
		super();
		this.parent = parent;
		this.empregadoEpiService = new EmpregadoEpiService();
	}
	
	
	public void gerarFichaEpi(EmpregadoVO empregado, String usuario) {
		try {
			FichaEpiVO fichaEpiVO = new FichaEpiVO();
			fichaEpiVO = new FichaEpiVO();
			fichaEpiVO.setUsuario(usuario);
			
			fichaEpiVO.setEmpresa(empregado.getEmpresa());
			fichaEpiVO.setEmpregado(empregado.getNome());
			fichaEpiVO.setMatricula(empregado.getMatricula());
			fichaEpiVO.setCpf(empregado.getCpf());
			fichaEpiVO.setRg(empregado.getRg());
			fichaEpiVO.setDataAdmissao(empregado.getDataAdmissao());
			fichaEpiVO.setDataDemissao(empregado.getDataDemissao());
			fichaEpiVO.setSetor(empregado.getSetor());
			fichaEpiVO.setCargo(empregado.getCargo());
			fichaEpiVO.setEpis(empregadoEpiService.buscarParaFichaEpi(empregado.getId()));
			
			SeletorDiretorios seletorDiretorios = new SeletorDiretorios();
			String diretorio = seletorDiretorios.selecionar();
			if (diretorio != null && !diretorio.isEmpty()) {
				FichaEpiView fichaEpiView = new FichaEpiView();
				String path = fichaEpiView.gerarFichaEpi(fichaEpiVO, diretorio);
				
				String opcoes[] = { "Abrir arquivo", "Fechar"};
				String msg = "Ficha de EPI gerada com sucesso!";
				int confirmacao = MensageiroUtils.confirmMsg(parent, msg, opcoes, opcoes[0]);
				if (confirmacao == 0) {
					FileUtils.abrirPdf(path);
				}
				fichaEpiView = null;
			}
			seletorDiretorios = null;
		} catch(Exception e) {
			e.printStackTrace();
			MensageiroUtils.errorMsg(parent, "Ocorreu algum problema ao gerar a Ficha de EPI! Contate um administrador do sistema.");
		}
	}

}
