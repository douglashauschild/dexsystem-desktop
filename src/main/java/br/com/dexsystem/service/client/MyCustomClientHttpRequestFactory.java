package br.com.dexsystem.service.client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.http.client.SimpleClientHttpRequestFactory;

public class MyCustomClientHttpRequestFactory extends SimpleClientHttpRequestFactory {

	private final String protocolo;
	private final HostnameVerifier hostNameVerifier;

	public MyCustomClientHttpRequestFactory(final HostnameVerifier hostNameVerifier) {
		this.hostNameVerifier = hostNameVerifier;
		this.protocolo = "TLSv1";
	}
	
	public MyCustomClientHttpRequestFactory(final HostnameVerifier hostNameVerifier, final String protocolo) {
		this.hostNameVerifier = hostNameVerifier;
		this.protocolo = protocolo;
	}

	@Override
	protected void prepareConnection(final HttpURLConnection connection, final String httpMethod) throws IOException {
		if (connection instanceof HttpsURLConnection) {
			((HttpsURLConnection) connection).setHostnameVerifier(hostNameVerifier);
			((HttpsURLConnection) connection).setSSLSocketFactory(initSSLContext().getSocketFactory());
		}
		super.prepareConnection(connection, httpMethod);
	}

	private SSLContext initSSLContext() {
		try {
			System.setProperty("https.protocols", protocolo);

			final SSLContext ctx = SSLContext.getInstance(protocolo);
			ctx.init(null, new TrustManager[] { new X509TrustManager() {
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) { }
				public void checkServerTrusted(X509Certificate[] certs, String authType) { }
			} }, new SecureRandom());
			return ctx;
		} catch (final Exception ex) {
			return null;
		}
	}
}