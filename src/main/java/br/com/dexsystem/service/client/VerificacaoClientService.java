package br.com.dexsystem.service.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class VerificacaoClientService extends ClientService {


	public boolean isConexaoServico() {
		boolean retorno = false;
		try {
			RestTemplate rt = getNewRestTemplate();
			String uri = getUrlServico() + "/health";
			HttpEntity<String> entity = new HttpEntity<String>(new HttpHeaders());
			ResponseEntity<String> ret = rt.exchange(uri, HttpMethod.GET, entity, new ParameterizedTypeReference<String>() {});
			String body = ret.getBody();
			if (ret.getBody() != null) {
				
				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode rootNode = objectMapper.readTree(body);
				JsonNode status = rootNode.path("status");
				retorno = status.asText().equals("UP");
			} else {
				retorno = false;
			}
		} catch (Exception e) {
//			LoggerUtils.logErro(e);
		} 
		return retorno;
	}
	
}
