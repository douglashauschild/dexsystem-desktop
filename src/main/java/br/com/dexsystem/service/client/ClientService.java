package br.com.dexsystem.service.client;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.PropertiesUtils;

public class ClientService {
	
	public String getUrlServico() {
		return PropertiesUtils.getUrl();
	}

	public RestTemplate getNewRestTemplate() throws Exception {
		RestTemplate toReturn = new RestTemplate();
		toReturn.setRequestFactory(new MyCustomClientHttpRequestFactory(NoopHostnameVerifier.INSTANCE, ConstantUtils.PROTOCOLO_TLSv1_2));
		return toReturn;
    }

	public static HttpHeaders getHeaders() throws Exception {
		return getHeaders(null);
	}
	public static HttpHeaders getHeaders(String token) throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		if (token != null && !token.isEmpty()) {
			headers.set("Authorization", "Bearer "+ token);
		}
		return headers;
	}
}
