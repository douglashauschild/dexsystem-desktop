package br.com.dexsystem.service.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.dexsystem.model.Usuario;

public class UsuarioClientService extends ClientService {


	public Usuario obterUsuarioLogado(String token) throws HttpClientErrorException, Exception  {
		String uri = getUrlServico() + "/api/usuario";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<String> entity = new HttpEntity<String>(getHeaders(token));
		ResponseEntity<Usuario> ret = rt.exchange(uri, HttpMethod.GET, entity, new ParameterizedTypeReference<Usuario>() {});
		return ret.getBody();
	}
	
}
