package br.com.dexsystem.service.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.dexsystem.model.vo.TokenVO;

public class TokenClientService extends ClientService {


	public TokenVO obterToken(TokenVO tokenVO) throws HttpClientErrorException, Exception {
		String uri = getUrlServico() + "/api/autenticar";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<TokenVO> entity = new HttpEntity<TokenVO>(tokenVO, getHeaders());
		ResponseEntity<TokenVO> ret = rt.exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<TokenVO>() {}, tokenVO);
		return ret.getBody();
	}
	
}
