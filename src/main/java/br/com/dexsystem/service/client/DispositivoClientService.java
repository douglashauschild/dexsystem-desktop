package br.com.dexsystem.service.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.dexsystem.model.Dispositivo;

public class DispositivoClientService extends ClientService {


	public Dispositivo gerarDispositivo(String token, String versao) throws HttpClientErrorException, Exception {
		if (versao == null || versao.isEmpty()) {
			versao = "1.0";
		}
		String uri = getUrlServico() + "/api/dispositivo/novo?versao='"+versao+"'";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<Dispositivo> entity = new HttpEntity<Dispositivo>(getHeaders(token));
		ResponseEntity<Dispositivo> ret = rt.exchange(uri, HttpMethod.GET, entity, new ParameterizedTypeReference<Dispositivo>() {});
		return ret.getBody();
	}
	
}
