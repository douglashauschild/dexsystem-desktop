package br.com.dexsystem.service.client;


import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.dexsystem.model.vo.SincronizacaoVO;

public class SincronizacaoClientService extends ClientService {


	public SincronizacaoVO sincronizar(String token, SincronizacaoVO sincronizacaoVO) throws HttpClientErrorException, Exception {
		String uri = getUrlServico() + "/api/sincronizar";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<SincronizacaoVO> entity = new HttpEntity<SincronizacaoVO>(sincronizacaoVO, getHeaders(token));
		ResponseEntity<SincronizacaoVO> ret = rt.exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<SincronizacaoVO>() {}, sincronizacaoVO);
		return ret.getBody();
	}
	
	
	public boolean confirmarSincronizacao(String token, SincronizacaoVO sincronizacaoVO) throws HttpClientErrorException, Exception {
		String uri = getUrlServico() + "/api/sincronizar/confirmar";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<SincronizacaoVO> entity = new HttpEntity<SincronizacaoVO>(sincronizacaoVO, getHeaders(token));
		ResponseEntity<SincronizacaoVO> ret = rt.exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<SincronizacaoVO>() {}, sincronizacaoVO);
		return ret.getStatusCode().equals(HttpStatus.OK);
	}
	
	
	public SincronizacaoVO enviarSincronizacao(String cadastro, String token, SincronizacaoVO sincronizacaoVO) throws HttpClientErrorException, Exception {
		String uri = getUrlServico() + "/api/"+cadastro+"/sincronizar/receber";
		RestTemplate rt = getNewRestTemplate();
		HttpEntity<SincronizacaoVO> entity = new HttpEntity<SincronizacaoVO>(sincronizacaoVO, getHeaders(token));
		ResponseEntity<SincronizacaoVO> ret = rt.exchange(uri, HttpMethod.POST, entity, new ParameterizedTypeReference<SincronizacaoVO>() {}, sincronizacaoVO);
		return ret.getBody();
	}
	
}
