package br.com.dexsystem.service;

import java.util.ArrayList;
import java.util.List;

import br.com.dexsystem.dao.EmpresaDao;
import br.com.dexsystem.model.Empresa;
import br.com.dexsystem.model.vo.SelectVO;

public class EmpresaService {

	private EmpresaDao empresaDao;
	private ParametroSistemaService parametroSistemaService;

	public EmpresaService() {
		super();
		this.empresaDao = new EmpresaDao();
		this.parametroSistemaService = new ParametroSistemaService();
	}
	
	
	public List<SelectVO> getEmpresasCombo() {
		List<SelectVO> empresasVO = new ArrayList<SelectVO>();
		List<Empresa> empresas = empresaDao.buscarTodas();
		for (Empresa empresa : empresas) {
			empresasVO.add(new SelectVO(empresa.getIdPessoa(), parametroSistemaService.getSelecaoEmpresa(empresa)));
		}
		return empresasVO;
	}
	
}
