package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.EmpregadoDao;
import br.com.dexsystem.model.Empregado;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class EmpregadoSincronizacao extends SincronizacaoDadosAbstract<Empregado> implements SincronizacaoDadosInterface {

	
	public EmpregadoSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.EMPREGADO.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Empregado> empregados = super.sincronizar(Empregado[].class);
		if (empregados != null && !empregados.isEmpty()) {
			EmpregadoDao empregadoDao = new EmpregadoDao();
			for (Empregado empregado : empregados) {
				super.salvarControleSincronizacao(empregado);
				empregadoDao.salvar(empregado);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}