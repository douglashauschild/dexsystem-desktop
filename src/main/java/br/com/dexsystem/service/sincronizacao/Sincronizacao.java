package br.com.dexsystem.service.sincronizacao;

import java.awt.Frame;

import javax.swing.SwingWorker;

import br.com.dexsystem.service.DispositivoService;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.Carregando;
import br.com.dexsystem.view.Dashboard;

public class Sincronizacao {
	
	private Frame parent;
	private Carregando carregando;
	private boolean mostrarCarregando;
	private DispositivoService dispositivoService;
	
	public static boolean sincronizando;
	
	public Sincronizacao(Frame parent) {
		this.parent = parent;
		this.dispositivoService = new DispositivoService();
	}
	
	public Sincronizacao(Frame parent, boolean mostrarCarregando) {
		this.parent = parent;
		this.mostrarCarregando = mostrarCarregando;
		this.dispositivoService = new DispositivoService();
	}

	public void sincronizar(String token) {
		if (mostrarCarregando) {
			carregando = new Carregando(parent);
			carregando.abrirTela("Sincronizando...", 18);
		}
		try {
			new SwingWorker() {
				
				@Override
                protected Object doInBackground() throws Exception {
					sincronizando = true;
					
					atualizarTexto("Usu�rios");
					new UsuarioSincronizacao(token).sincronizarDados();

					atualizarTexto("Motivos");
					new MotivoSincronizacao(token).sincronizarDados();

					atualizarTexto("Unidades de Medidas");
					new UnidadeMedidaSincronizacao(token).sincronizarDados();

					atualizarTexto("Grades");
					new GradeSincronizacao(token).sincronizarDados();

					atualizarTexto("Tamanhos de Grade");
					new GradeItemSincronizacao(token).sincronizarDados();

					atualizarTexto("EPIs");
					new EpiSincronizacao(token).sincronizarDados();

					atualizarTexto("Pessoas");
					new PessoaSincronizacao(token).sincronizarDados();

					atualizarTexto("Pessoas Jur�dicas");
					new PessoaJuridicaSincronizacao(token).sincronizarDados();

					atualizarTexto("Empresas");
					new EmpresaSincronizacao(token).sincronizarDados();

					atualizarTexto("Setores");
					new SetorSincronizacao(token).sincronizarDados();

					atualizarTexto("Cargos");
					new CargoSincronizacao(token).sincronizarDados();

					atualizarTexto("EPIs do Cargo");
					new CargoEpiSincronizacao(token).sincronizarDados();

					atualizarTexto("Pessoas F�sicas");
					new PessoaFisicaSincronizacao(token).sincronizarDados();
					
					atualizarTexto("Digitais");
					PessoaDigitalSincronizacao pessoaDigitalSincronizacao = new PessoaDigitalSincronizacao(token);
					pessoaDigitalSincronizacao.enviarDados();
					pessoaDigitalSincronizacao.sincronizarDados();

					atualizarTexto("Empregados");
					new EmpregadoSincronizacao(token).sincronizarDados();

					atualizarTexto("Atividades");
					new AtividadeSincronizacao(token).sincronizarDados();

					atualizarTexto("EPIs dos Empregados");
					EmpregadoEpiSincronizacao empregadoEpiSincronizacao = new EmpregadoEpiSincronizacao(token);
					empregadoEpiSincronizacao.enviarDados();
					empregadoEpiSincronizacao.sincronizarDados();

					atualizarTexto("Par�metros do Sistema");
					new ParametroSistemaSincronizacao(token).sincronizarDados();
					return null;
				}
				
				@Override
                protected void done() {
					if (dispositivoService == null) {
						dispositivoService = new DispositivoService();
					}
					dispositivoService.atualizarDataSincronizacao();
					
					if (parent instanceof Dashboard) {
						Dashboard dashboard = ((Dashboard) parent);
						if (dashboard.getUsuarioLogado() == null) {
							dashboard.carregarInformacoes();
						}
						dashboard.atualizarDataSincronizacao();
						dashboard.popularTabela();
						dashboard = null;
					}
					if (mostrarCarregando) {
						carregando.fecharTela();
					}
					sincronizando = false;
					
					dispositivoService = null;
					carregando = null;
                }
			}.execute();
		} catch (Exception e) {
			e.printStackTrace();
			carregando = null;
			MensageiroUtils.errorMsg(parent, "Ocorreu algum problema durante a sincroniza��o de dados! Tente novamente mais tarde.");
		}
	}
	
	
	private void atualizarTexto(String registro) {
		if (mostrarCarregando) {
			carregando.atualizar("Sincronizando " + registro + "...");
		}
	}
}