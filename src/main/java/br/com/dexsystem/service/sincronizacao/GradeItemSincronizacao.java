package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.GradeItemDao;
import br.com.dexsystem.model.GradeItem;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class GradeItemSincronizacao extends SincronizacaoDadosAbstract<GradeItem> implements SincronizacaoDadosInterface {
	
	public GradeItemSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.GRADE_ITEM.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<GradeItem> gradeItens = super.sincronizar(GradeItem[].class);
		if (gradeItens != null && !gradeItens.isEmpty()) {
			GradeItemDao gradeItemDao = new GradeItemDao();
			for (GradeItem gradeItem : gradeItens) {
				super.salvarControleSincronizacao(gradeItem);
				gradeItemDao.salvar(gradeItem);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}