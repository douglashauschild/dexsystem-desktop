package br.com.dexsystem.service.sincronizacao;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import br.com.dexsystem.dao.ControleSincronizacaoDao;
import br.com.dexsystem.dao.DispositivoDao;
import br.com.dexsystem.model.ControleSincronizacao;
import br.com.dexsystem.model.Dispositivo;
import br.com.dexsystem.model.SincronizacaoAbstract;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;
import br.com.dexsystem.model.vo.SincronizacaoVO;
import br.com.dexsystem.service.ApiService;
import br.com.dexsystem.service.DispositivoService;
import br.com.dexsystem.service.client.SincronizacaoClientService;

public abstract class SincronizacaoDadosAbstract<T extends Serializable> {
	
	private String token;
	private String tabela;
	protected Long idDispositivo;
	private SincronizacaoVO sincronizacaoRetornoVO;
	private ControleSincronizacaoDao controleSincronizacaoDao;
	private SincronizacaoClientService sincronizacaoClientService;
	private ApiService apiService;
	
	
	public SincronizacaoDadosAbstract() {
		super();
	}


	public SincronizacaoDadosAbstract(String token, String tabela) {
		super();
		this.token = token;
		this.tabela = tabela;
		this.controleSincronizacaoDao = new ControleSincronizacaoDao();
		this.sincronizacaoClientService = new SincronizacaoClientService();
		this.apiService = new ApiService();
		
		try {
			this.idDispositivo = getDispositivo().getId();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public SincronizacaoAbstract gerarChecksum(boolean inclusao, SincronizacaoAbstract sincronizacaoAbstract) throws Exception {
		if (inclusao) {
			sincronizacaoAbstract.setChecksum(sincronizacaoAbstract.getHash(true));
		}
		sincronizacaoAbstract.setChecksumAlteracao(sincronizacaoAbstract.getHash(false));
		return sincronizacaoAbstract;
	}
	

	public List<T> sincronizar(Class<T[]> classe) throws HttpClientErrorException, Exception {
		System.out.println("Sincronizando " + tabela);
		SincronizacaoVO sincronizacaoVO = new SincronizacaoVO(idDispositivo, tabela);
		sincronizacaoRetornoVO = sincronizacaoClientService.sincronizar(token, sincronizacaoVO);
		T[] array = getGson().fromJson(sincronizacaoRetornoVO.getJson(), classe);
		System.out.println("Sincronizou " + tabela + ": " + array.length + " registros");
		return Arrays.asList(array);
	}
	
	
	protected void salvarControleSincronizacao(SincronizacaoAbstract sincronizacaoAbstract) {
		controleSincronizacaoDao.salvar(new ControleSincronizacao(idDispositivo, tabela, sincronizacaoAbstract.getChecksum(), sincronizacaoAbstract.getChecksumAlteracao()));
	}

	
	protected boolean confirmarSincronizacao() throws HttpClientErrorException, Exception {
		return sincronizacaoClientService.confirmarSincronizacao(token, sincronizacaoRetornoVO);
	}

	
	protected List<T> enviarDados(Class<T[]> classe, String cadastro) throws HttpClientErrorException, Exception {
		List<Object> objects = apiService.buscarDados(tabela, TabelaSincronizacaoEnum.getByTabela(tabela).getClasse());
		if (objects != null && !objects.isEmpty()) {
			SincronizacaoVO sincronizacaoVO = new SincronizacaoVO(idDispositivo, tabela, getGson().toJson(objects));
			sincronizacaoRetornoVO = sincronizacaoClientService.enviarSincronizacao(cadastro, token, sincronizacaoVO);
			T[] array = getGson().fromJson(sincronizacaoRetornoVO.getJson(), classe);
			System.out.println("Enviou " + tabela + ": " + array.length + " registros");
			return Arrays.asList(array);
		} 
		return null;
	}

	
	private Dispositivo getDispositivo() throws HttpClientErrorException, Exception {
		DispositivoDao dispositivoDao = new DispositivoDao();
		Dispositivo dispositivo = dispositivoDao.buscarUnico();
		if (dispositivo == null) {
			dispositivo = new DispositivoService().gerarDispositivo(token);
			dispositivoDao.salvar(dispositivo);
		}
		return dispositivo;
	}
	
	
	protected Gson getGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		gsonBuilder.registerTypeAdapter(java.util.Date.class, new JsonSerializer<Date>() {
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(src.getTime());
			}
		});
		gsonBuilder.registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {
		    public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
		        return new JsonPrimitive(Base64.encodeBase64String(src));
		    }
		});
		gsonBuilder.registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
			@Override
			public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				return Base64.decodeBase64(json.getAsString());
			}
		});
		return gsonBuilder.create();
	}

}
