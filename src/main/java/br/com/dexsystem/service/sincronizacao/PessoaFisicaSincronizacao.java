package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.PessoaFisicaDao;
import br.com.dexsystem.model.PessoaFisica;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class PessoaFisicaSincronizacao extends SincronizacaoDadosAbstract<PessoaFisica> implements SincronizacaoDadosInterface {

	public PessoaFisicaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.PESSOA_FISICA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<PessoaFisica> pessoasFisica = super.sincronizar(PessoaFisica[].class);
		if (pessoasFisica != null && !pessoasFisica.isEmpty()) {
			PessoaFisicaDao pessoaFisicaDao = new PessoaFisicaDao();
			for (PessoaFisica pessoaFisica : pessoasFisica) {
				super.salvarControleSincronizacao(pessoaFisica);
				pessoaFisicaDao.salvar(pessoaFisica);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}