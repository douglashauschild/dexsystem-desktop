package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.AtividadeDao;
import br.com.dexsystem.model.Atividade;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class AtividadeSincronizacao extends SincronizacaoDadosAbstract<Atividade> implements SincronizacaoDadosInterface {

	
	public AtividadeSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.ATIVIDADE.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Atividade> atividades = super.sincronizar(Atividade[].class);
		if (atividades != null && !atividades.isEmpty()) {
			AtividadeDao atividadeDao = new AtividadeDao();
			for (Atividade atividade : atividades) {
				super.salvarControleSincronizacao(atividade);
				atividadeDao.salvar(atividade);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}