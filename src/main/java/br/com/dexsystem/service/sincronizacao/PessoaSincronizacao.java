package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.PessoaDao;
import br.com.dexsystem.model.Pessoa;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class PessoaSincronizacao extends SincronizacaoDadosAbstract<Pessoa> implements SincronizacaoDadosInterface {

	
	public PessoaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.PESSOA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Pessoa> pessoas = super.sincronizar(Pessoa[].class);
		if (pessoas != null && !pessoas.isEmpty()) {
			PessoaDao pessoaDao = new PessoaDao();
			for (Pessoa pessoa : pessoas) {
				super.salvarControleSincronizacao(pessoa);
				pessoaDao.salvar(pessoa);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}
