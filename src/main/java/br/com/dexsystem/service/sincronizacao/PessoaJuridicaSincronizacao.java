package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.PessoaJuridicaDao;
import br.com.dexsystem.model.PessoaJuridica;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class PessoaJuridicaSincronizacao extends SincronizacaoDadosAbstract<PessoaJuridica> implements SincronizacaoDadosInterface {

	public PessoaJuridicaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.PESSOA_JURIDICA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<PessoaJuridica> pessoaJuridicas = super.sincronizar(PessoaJuridica[].class);
		if (pessoaJuridicas != null && !pessoaJuridicas.isEmpty()) {
			PessoaJuridicaDao pessoaJuridicaDao = new PessoaJuridicaDao();
			for (PessoaJuridica pessoaJuridica : pessoaJuridicas) {
				super.salvarControleSincronizacao(pessoaJuridica);
				pessoaJuridicaDao.salvar(pessoaJuridica);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}