package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.EpiDao;
import br.com.dexsystem.model.Epi;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class EpiSincronizacao extends SincronizacaoDadosAbstract<Epi> implements SincronizacaoDadosInterface {

	
	public EpiSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.EPI.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Epi> epis = super.sincronizar(Epi[].class);
		if (epis != null && !epis.isEmpty()) {
			EpiDao epiDao = new EpiDao();
			for (Epi epi : epis) {
				super.salvarControleSincronizacao(epi);
				epiDao.salvar(epi);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}