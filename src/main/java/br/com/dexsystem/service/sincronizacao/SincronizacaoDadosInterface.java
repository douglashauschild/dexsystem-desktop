package br.com.dexsystem.service.sincronizacao;

import org.springframework.web.client.HttpClientErrorException;

public interface SincronizacaoDadosInterface {
	
	public boolean sincronizarDados() throws HttpClientErrorException, Exception;
	
}
