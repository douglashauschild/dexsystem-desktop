package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.CargoEpiDao;
import br.com.dexsystem.model.CargoEpi;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class CargoEpiSincronizacao extends SincronizacaoDadosAbstract<CargoEpi> implements SincronizacaoDadosInterface {

	
	public CargoEpiSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.CARGO_EPI.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<CargoEpi> cargoEpis = super.sincronizar(CargoEpi[].class);
		if (cargoEpis != null && !cargoEpis.isEmpty()) {
			CargoEpiDao cargoEpiDao = new CargoEpiDao();
			for (CargoEpi cargoEpi : cargoEpis) {
				super.salvarControleSincronizacao(cargoEpi);
				cargoEpiDao.salvar(cargoEpi);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}