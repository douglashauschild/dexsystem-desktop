package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.GradeDao;
import br.com.dexsystem.model.Grade;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class GradeSincronizacao extends SincronizacaoDadosAbstract<Grade> implements SincronizacaoDadosInterface {

	
	public GradeSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.GRADE.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Grade> grades = super.sincronizar(Grade[].class);
		if (grades != null && !grades.isEmpty()) {
			GradeDao gradeDao = new GradeDao();
			for (Grade grade : grades) {
				super.salvarControleSincronizacao(grade);
				gradeDao.salvar(grade);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}