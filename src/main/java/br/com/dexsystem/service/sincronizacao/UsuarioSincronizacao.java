package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.UsuarioDao;
import br.com.dexsystem.model.Usuario;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class UsuarioSincronizacao extends SincronizacaoDadosAbstract<Usuario> implements SincronizacaoDadosInterface {

	
	public UsuarioSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.USUARIO.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Usuario> usuarios = super.sincronizar(Usuario[].class);
		if (usuarios != null && !usuarios.isEmpty()) {
			UsuarioDao usuarioDao = new UsuarioDao();
			for (Usuario usuario : usuarios) {
				super.salvarControleSincronizacao(usuario);
				usuarioDao.salvar(usuario);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}