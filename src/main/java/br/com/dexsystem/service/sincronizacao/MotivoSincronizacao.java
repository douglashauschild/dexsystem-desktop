package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.MotivoDao;
import br.com.dexsystem.model.Motivo;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class MotivoSincronizacao extends SincronizacaoDadosAbstract<Motivo> implements SincronizacaoDadosInterface {

	public MotivoSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.MOTIVO.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Motivo> motivos = super.sincronizar(Motivo[].class);
		if (motivos != null && !motivos.isEmpty()) {
			MotivoDao motivoDao = new MotivoDao();
			for (Motivo motivo : motivos) {
				super.salvarControleSincronizacao(motivo);
				motivoDao.salvar(motivo);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}