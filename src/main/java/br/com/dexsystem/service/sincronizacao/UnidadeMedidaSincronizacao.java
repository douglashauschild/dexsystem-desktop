package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.UnidadeMedidaDao;
import br.com.dexsystem.model.UnidadeMedida;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class UnidadeMedidaSincronizacao extends SincronizacaoDadosAbstract<UnidadeMedida> implements SincronizacaoDadosInterface {

	public UnidadeMedidaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.UNIDADE_MEDIDA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<UnidadeMedida> unidadeMedidas = super.sincronizar(UnidadeMedida[].class);
		if (unidadeMedidas != null && !unidadeMedidas.isEmpty()) {
			UnidadeMedidaDao unidadeMedidaDao = new UnidadeMedidaDao();
			for (UnidadeMedida unidadeMedida : unidadeMedidas) {
				super.salvarControleSincronizacao(unidadeMedida);
				unidadeMedidaDao.salvar(unidadeMedida);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}