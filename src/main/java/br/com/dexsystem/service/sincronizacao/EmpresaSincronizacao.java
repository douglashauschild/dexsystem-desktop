package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.EmpresaDao;
import br.com.dexsystem.model.Empresa;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class EmpresaSincronizacao extends SincronizacaoDadosAbstract<Empresa> implements SincronizacaoDadosInterface {

	
	public EmpresaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.EMPRESA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Empresa> empresas = super.sincronizar(Empresa[].class);
		if (empresas != null && !empresas.isEmpty()) {
			EmpresaDao empresaDao = new EmpresaDao();
			for (Empresa empresa : empresas) {
				super.salvarControleSincronizacao(empresa);
				empresaDao.salvar(empresa);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}