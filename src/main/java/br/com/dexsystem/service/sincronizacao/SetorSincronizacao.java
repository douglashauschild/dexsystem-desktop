package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.SetorDao;
import br.com.dexsystem.model.Setor;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class SetorSincronizacao extends SincronizacaoDadosAbstract<Setor> implements SincronizacaoDadosInterface {

	public SetorSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.SETOR.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Setor> setores = super.sincronizar(Setor[].class);
		if (setores != null && !setores.isEmpty()) {
			SetorDao setorDao = new SetorDao();
			for (Setor setor : setores) {
				super.salvarControleSincronizacao(setor);
				setorDao.salvar(setor);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}