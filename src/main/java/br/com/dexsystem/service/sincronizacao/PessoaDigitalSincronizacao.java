package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.PessoaDigitalDao;
import br.com.dexsystem.model.PessoaDigital;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class PessoaDigitalSincronizacao extends SincronizacaoDadosAbstract<PessoaDigital> implements SincronizacaoDadosInterface {

	public PessoaDigitalSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.PESSOA_DIGITAL.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<PessoaDigital> pessoas = super.sincronizar(PessoaDigital[].class);
		if (pessoas != null && !pessoas.isEmpty()) {
			PessoaDigitalDao pessoaDigitalDao = new PessoaDigitalDao();
			for (PessoaDigital PessoaDigital : pessoas) {
				super.salvarControleSincronizacao(PessoaDigital);
				pessoaDigitalDao.gravarSincronizacao(PessoaDigital);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
	
	
	public void enviarDados() throws HttpClientErrorException, Exception {
		List<PessoaDigital> pessoas = super.enviarDados(PessoaDigital[].class, "pessoaDigital");
		if (pessoas != null && !pessoas.isEmpty()) {
			for (PessoaDigital pessoaDigital : pessoas) {
				super.salvarControleSincronizacao(pessoaDigital);
			}
		}
	}
}