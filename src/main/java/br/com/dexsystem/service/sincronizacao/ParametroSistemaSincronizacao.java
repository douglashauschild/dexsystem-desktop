package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.ParametroSistemaDao;
import br.com.dexsystem.model.ParametroSistema;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class ParametroSistemaSincronizacao extends SincronizacaoDadosAbstract<ParametroSistema> implements SincronizacaoDadosInterface {

	public ParametroSistemaSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.PARAMETRO_SISTEMA.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<ParametroSistema> parametroSistemas = super.sincronizar(ParametroSistema[].class);
		if (parametroSistemas != null && !parametroSistemas.isEmpty()) {
			ParametroSistemaDao parametroSistemaDao = new ParametroSistemaDao();
			for (ParametroSistema parametroSistema : parametroSistemas) {
				super.salvarControleSincronizacao(parametroSistema);
				parametroSistemaDao.salvar(parametroSistema);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}