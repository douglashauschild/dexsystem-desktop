package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.EmpregadoEpiDao;
import br.com.dexsystem.model.EmpregadoEpi;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class EmpregadoEpiSincronizacao extends SincronizacaoDadosAbstract<EmpregadoEpi> implements SincronizacaoDadosInterface {

	
	public EmpregadoEpiSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.EMPREGADO_EPI.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<EmpregadoEpi> empregadoEpis = super.sincronizar(EmpregadoEpi[].class);
		if (empregadoEpis != null && !empregadoEpis.isEmpty()) {
			EmpregadoEpiDao empregadoEpiDao = new EmpregadoEpiDao();
			for (EmpregadoEpi empregadoEpi : empregadoEpis) {
				super.salvarControleSincronizacao(empregadoEpi);
				empregadoEpiDao.gravarSincronizacao(empregadoEpi);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
	
	
	public void enviarDados() throws HttpClientErrorException, Exception {
		List<EmpregadoEpi> empregadoEpis = super.enviarDados(EmpregadoEpi[].class, "empregadoEpi");
		if (empregadoEpis != null && !empregadoEpis.isEmpty()) {
			for (EmpregadoEpi empregadoEpi : empregadoEpis) {
				super.salvarControleSincronizacao(empregadoEpi);
			}
		}
	}
}