package br.com.dexsystem.service.sincronizacao;

import java.util.List;

import org.springframework.web.client.HttpClientErrorException;

import br.com.dexsystem.dao.CargoDao;
import br.com.dexsystem.model.Cargo;
import br.com.dexsystem.model.constant.TabelaSincronizacaoEnum;

public class CargoSincronizacao extends SincronizacaoDadosAbstract<Cargo> implements SincronizacaoDadosInterface {

	
	public CargoSincronizacao(String token) {
		super(token, TabelaSincronizacaoEnum.CARGO.getTabela());
	}
	

	public boolean sincronizarDados() throws HttpClientErrorException, Exception {
		List<Cargo> cargos = super.sincronizar(Cargo[].class);
		if (cargos != null && !cargos.isEmpty()) {
			CargoDao cargoDao = new CargoDao();
			for (Cargo cargo : cargos) {
				super.salvarControleSincronizacao(cargo);
				cargoDao.salvar(cargo);
			}
			return confirmarSincronizacao();
		}
		return true;
	}
}