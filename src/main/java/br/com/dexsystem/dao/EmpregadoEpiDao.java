package br.com.dexsystem.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.com.dexsystem.model.EmpregadoEpi;

public class EmpregadoEpiDao extends GenericDao<EmpregadoEpi> {

	public EmpregadoEpiDao() {
        super();
    }
	
	
	public void gravar(EmpregadoEpi empregadoEpi) {
		EmpregadoEpi empregadoEpiAux = buscarPorId(empregadoEpi.getId());
		if (empregadoEpiAux != null) {
			empregadoEpi.setId(empregadoEpiAux.getId());
			super.atualizar(empregadoEpi);
		} else {
			empregadoEpi.setId(getProximoId("empregadoEpi.max"));
			super.salvar(empregadoEpi);
		}
	}
	
	
	public void gravarSincronizacao(EmpregadoEpi empregadoEpi) {
		EmpregadoEpi empregadoEpiAux = buscarPorChecksum(empregadoEpi.getChecksum());
		if (empregadoEpiAux != null) {
			empregadoEpi.setId(empregadoEpiAux.getId());
			super.atualizar(empregadoEpi);
		} else {
			empregadoEpi.setId(getProximoId("empregadoEpi.max"));
			super.salvar(empregadoEpi);
		}
	}
	
	
	public List<EmpregadoEpi> buscarParaEstatisticas(Long idEmpregado) {
		getSession().clear();
		Criteria criteria = getSession().createCriteria(EmpregadoEpi.class);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		
		Criterion vencidos = Restrictions.or(
										Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.lt("dataPrevistaEntrega", new Date())), 
										Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaTroca"), Restrictions.lt("dataPrevistaTroca", new Date()))
									);
		Criterion ativos = Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"));
		Criterion aguardando = Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.ge("dataPrevistaEntrega", new Date()));
		criteria.add(Restrictions.or(vencidos, ativos, aguardando));
		return criteria.list();
	}
	
	
	public List<EmpregadoEpi> buscarParaTabela(Long idEmpregado, boolean substituidos, boolean devolvidos, boolean entregaVencida, boolean vencidos, boolean ativos, boolean aguardando) {
		getSession().clear();
		Criteria criteria = getSession().createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		
		if (!substituidos || !devolvidos) {
			Criterion filtrosCheckbox = null;
			if (substituidos) {
				filtrosCheckbox = Restrictions.isNotNull("dataSubstituicao");
			}
			if (devolvidos) {
				Criterion fAux = Restrictions.and(Restrictions.isNotNull("dataDevolucao"), Restrictions.isNull("dataSubstituicao"));
				if (filtrosCheckbox != null) {
					filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
				} else {
					filtrosCheckbox = fAux;
				}
			}
			if (entregaVencida) {
				Criterion fAux = Restrictions.and(Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"), Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.lt("dataPrevistaEntrega", new Date()));
				if (filtrosCheckbox != null) {
					filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
				} else {
					filtrosCheckbox = fAux;
				}
			}
			if (vencidos) {
				Criterion fAux = Restrictions.and(Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"), Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaTroca"), Restrictions.lt("dataPrevistaTroca", new Date()));
				if (filtrosCheckbox != null) {
					filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
				} else {
					filtrosCheckbox = fAux;
				}
			}
			if (ativos) {
				Criterion fAux = Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"));
				if (filtrosCheckbox != null) {
					filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
				} else {
					filtrosCheckbox = fAux;
				}
			}
			if (aguardando) {
				Criterion fAux = Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.ge("dataPrevistaEntrega", new Date()));
				if (filtrosCheckbox != null) {
					filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
				} else {
					filtrosCheckbox = fAux;
				}
			}
			if (filtrosCheckbox != null) {
				criteria.add(filtrosCheckbox);
			}
		}
		return criteria.list();
	}
	
	
	public List<EmpregadoEpi> buscarParaFichaEpi(Long idEmpregado) {
		Criteria criteria = getSession().createCriteria(EmpregadoEpi.class);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		criteria.add(Restrictions.isNotNull("dataConfirmacao"));
		criteria.addOrder(Order.asc("dataEntrega"));
		return criteria.list();
	}
}