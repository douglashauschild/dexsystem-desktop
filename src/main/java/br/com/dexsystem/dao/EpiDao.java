package br.com.dexsystem.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.com.dexsystem.model.Epi;

public class EpiDao extends GenericDao<Epi> {

	public EpiDao() {
        super();
    }
	
	
	public List<Epi> buscarTodos() {
		Criteria criteria = getSession().createCriteria(Epi.class);
		criteria.createAlias("grade", "grade", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("unidadeMedida", "unidadeMedida", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}
}