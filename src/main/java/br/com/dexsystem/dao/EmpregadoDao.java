package br.com.dexsystem.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.com.dexsystem.model.Empregado;

public class EmpregadoDao extends GenericDao<Empregado> {
	
	public EmpregadoDao() {
        super();
    }
	
	
	public List<Empregado> buscarAtivosPorEmpresa(Long idEmpresa) {
		Criteria crit = getSession().createCriteria(Empregado.class);
		crit.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		crit.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		crit.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		crit.createAlias("empresa.pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		crit.createAlias("pessoaJuridica.pessoa", "pessoaEmp", JoinType.INNER_JOIN);
		
		if (idEmpresa != null) {
			crit.add(Restrictions.eq("idEmpresa", idEmpresa));
		}
		crit.add(RestrictionEmpregadoAtivo());
		crit.addOrder(Order.asc("pessoa.nome"));
		return crit.list();
	}
	
	
	public List<Empregado> buscarAtivosPorIds(List<Long> idsEmpregado) {
		Criteria crit = getSession().createCriteria(Empregado.class);
		crit.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		crit.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		crit.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		crit.createAlias("empresa.pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		crit.createAlias("pessoaJuridica.pessoa", "pessoaEmp", JoinType.INNER_JOIN);
		
		crit.add(Restrictions.in("id", idsEmpregado));
		crit.add(RestrictionEmpregadoAtivo());
		crit.addOrder(Order.asc("pessoa.nome"));
		return crit.list();
	}
	
	
	private Criterion RestrictionEmpregadoAtivo() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataAdmissao", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataDemissao"), Restrictions.gt("dataDemissao", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
