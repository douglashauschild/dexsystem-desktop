package br.com.dexsystem.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.dexsystem.model.Usuario;

public class UsuarioDao extends GenericDao<Usuario> {
	
	public UsuarioDao() {
        super();
    }
	
	
	public Usuario buscarPorEmail(String email) {
		Criteria crit = getSession().createCriteria(Usuario.class);
		crit.add(Restrictions.eq("email", email));
		crit.setMaxResults(1);
		return (Usuario) crit.uniqueResult();
	}
    	
}