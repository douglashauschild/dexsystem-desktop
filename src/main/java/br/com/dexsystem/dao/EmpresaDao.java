package br.com.dexsystem.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.com.dexsystem.model.Empresa;

public class EmpresaDao extends GenericDao<Empresa> {

	public EmpresaDao() {
        super();
    }
	
	
	public List<Empresa> buscarTodas() {
		Criteria crit = getSession().createCriteria(Empresa.class);
		crit.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		crit.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		crit.add(RestrictionEmpresaAtiva());
		return crit.list();
	}

	
	private Criterion RestrictionEmpresaAtiva() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("pessoaJuridica.dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("pessoaJuridica.dataFim"), Restrictions.gt("pessoaJuridica.dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("pessoaJuridica.excluido"), Restrictions.eq("pessoaJuridica.excluido", '0'))
		);
	}
}