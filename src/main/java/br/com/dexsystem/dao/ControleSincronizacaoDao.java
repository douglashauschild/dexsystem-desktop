package br.com.dexsystem.dao;

import br.com.dexsystem.model.ControleSincronizacao;

public class ControleSincronizacaoDao extends GenericDao<ControleSincronizacao> {
	
	public ControleSincronizacaoDao() {
        super();
    }
	
	@Override
	public void salvar(ControleSincronizacao controleSincronizacao) {
		ControleSincronizacao controleSincronizacaoAux = buscarPorChecksum(controleSincronizacao.getChecksum());
		if (controleSincronizacaoAux != null) {
			controleSincronizacao.setId(controleSincronizacaoAux.getId());
			super.atualizar(controleSincronizacao);
		} else {
			controleSincronizacao.setId(getProximoId("controleSincronizacao.max"));
			super.salvar(controleSincronizacao);
		}
	}
}
