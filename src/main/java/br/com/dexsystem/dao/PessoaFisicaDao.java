package br.com.dexsystem.dao;

import br.com.dexsystem.model.PessoaFisica;

public class PessoaFisicaDao extends GenericDao<PessoaFisica> {

	public PessoaFisicaDao() {
        super();
    }
}