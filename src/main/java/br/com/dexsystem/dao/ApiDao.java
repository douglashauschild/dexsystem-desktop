package br.com.dexsystem.dao;


import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import br.com.dexsystem.model.ControleSincronizacao;
import br.com.dexsystem.model.vo.ColunaVO;

public class ApiDao extends GenericDao<ControleSincronizacao> {

	public ApiDao() {
        super();
    }
	
	public List<Object> buscarDados(String tabela, List<ColunaVO> colunas, Class classe) {
		String colunasSql = "";
		for (ColunaVO colunaVO : colunas) {
			if (!colunasSql.isEmpty()) {
				colunasSql += ", ";
			}
			colunasSql += colunaVO.getColunas();
		}
		SQLQuery sqlQuery = getSession().createSQLQuery("select "+colunasSql+" from "+tabela+" where (checksum not in (select checksum from controle_sincronizacao where tabela = '"+tabela+"') or checksum in (select checksum from controle_sincronizacao where tabela = '"+tabela+"' and checksum_alteracao != "+tabela+".checksum_alteracao))");
		
		for (ColunaVO colunaVO : colunas) {
			sqlQuery.addScalar(colunaVO.getColuna(), colunaVO.getTipo());
		}
		sqlQuery.setResultTransformer(Transformers.aliasToBean(classe));
		return sqlQuery.list();
	}
}
