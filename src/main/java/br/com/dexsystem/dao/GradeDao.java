package br.com.dexsystem.dao;

import br.com.dexsystem.model.Grade;

public class GradeDao extends GenericDao<Grade> {

	public GradeDao() {
        super();
    }
}