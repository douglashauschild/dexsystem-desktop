package br.com.dexsystem.dao;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import br.com.dexsystem.model.Atividade;
import br.com.dexsystem.model.Empregado;

public class AtividadeDao extends GenericDao<Atividade> {
	
	public AtividadeDao() {
        super();
    }
	
	
	public Atividade buscarAtivaPorEmpregado(Empregado empregado) {
		Criteria crit = getSession().createCriteria(Atividade.class);
		crit.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		crit.createAlias("cargo.setor", "setor", JoinType.INNER_JOIN);
		crit.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		crit.add(Restrictions.eq("idEmpregado", empregado.getId()));
		crit.add(RestrictionAtividadeAtiva());
		crit.addOrder(Order.asc("dataInicio"));
		crit.setMaxResults(1);
		return (Atividade) crit.uniqueResult();
	}
	
	
	private Criterion RestrictionAtividadeAtiva() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataFim"), Restrictions.gt("dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
