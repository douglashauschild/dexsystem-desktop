package br.com.dexsystem.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.dexsystem.util.HibernateUtil;

public class GenericDao<T extends Serializable> {

    private Session session;
    private final Class<T> persistentClass;

    public GenericDao() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Session getSession() {
        if (this.session == null || !this.session.isOpen()) {
            this.session = HibernateUtil.getSession();
        }
        return this.session;
    }

    public void salvar(T entity) {
        getSession().getTransaction().begin();
        getSession().saveOrUpdate(entity);
        getSession().getTransaction().commit();
        getSession().close();
    }

    public void atualizar(T entity) {
        getSession().getTransaction().begin();
        getSession().merge(entity);
        getSession().getTransaction().commit();
        getSession().close();
    }

    public void excluir(T entity) {
        getSession().getTransaction().begin();
        getSession().delete(entity);
        getSession().getTransaction().commit();
        getSession().close();
    }

    public List<T> buscarTodos() throws Exception {
        return getSession().createCriteria(persistentClass).add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))).list();
    }
    
    public T buscarUnico() {
        return (T) getSession().createCriteria(persistentClass).uniqueResult();
    }

    public T buscarPorId(Long id) {
        return (T) getSession().createCriteria(persistentClass).add(Restrictions.eq("id", id)).add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))).uniqueResult();
    }
    
    public T buscarPorChecksum(String checksum) {
        return (T) getSession().createCriteria(persistentClass).add(Restrictions.eq("checksum", checksum)).uniqueResult();
    }

    protected Long getProximoId(String sql) {
		Query query = getSession().getNamedQuery(sql);
		Long i = (Long) query.uniqueResult();
		if (i == null) {
			i = 0L;
		}
		return i + 1;
	}
  
}
