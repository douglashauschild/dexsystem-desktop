package br.com.dexsystem.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.dexsystem.model.PessoaDigital;

public class PessoaDigitalDao extends GenericDao<PessoaDigital> {

	public PessoaDigitalDao() {
        super();
    }
	
	public void gravar(PessoaDigital pessoaDigital) {
		PessoaDigital pessoaDigitalAux = buscarDigital(pessoaDigital.getIdPessoa(), pessoaDigital.getPosicao());
		if (pessoaDigitalAux != null) {
			pessoaDigital.setId(pessoaDigitalAux.getId());
			super.atualizar(pessoaDigital);
		} else {
			pessoaDigital.setId(getProximoId("pessoaDigital.max"));
			super.salvar(pessoaDigital);
		}
	}
	
	
	public void gravarSincronizacao(PessoaDigital pessoaDigital) {
		PessoaDigital pessoaDigitalAux = buscarPorChecksum(pessoaDigital.getChecksum());
		if (pessoaDigitalAux != null) {
			pessoaDigital.setId(pessoaDigitalAux.getId());
			super.atualizar(pessoaDigital);
		} else {
			pessoaDigital.setId(getProximoId("pessoaDigital.max"));
			super.salvar(pessoaDigital);
		}
	}
	
	
	public PessoaDigital buscarDigital(Long idPessoa, String posicao) {
		getSession().clear();
		Criteria crit = getSession().createCriteria(PessoaDigital.class);
		crit.add(Restrictions.eq("idPessoa", idPessoa));
		crit.add(Restrictions.eq("posicao", posicao));
		crit.setMaxResults(1);
		return (PessoaDigital) crit.uniqueResult();
	}
	
	
	public List<PessoaDigital> buscarPorIdPessoa(Long idPessoa) {
		getSession().clear();
		Criteria crit = getSession().createCriteria(PessoaDigital.class);
		crit.add(Restrictions.eq("idPessoa", idPessoa));
		return crit.list();
	}
}