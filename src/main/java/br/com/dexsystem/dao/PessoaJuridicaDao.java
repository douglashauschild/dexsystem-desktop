package br.com.dexsystem.dao;

import br.com.dexsystem.model.PessoaJuridica;

public class PessoaJuridicaDao extends GenericDao<PessoaJuridica> {

	public PessoaJuridicaDao() {
        super();
    }
}