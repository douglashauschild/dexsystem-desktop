package br.com.dexsystem.dao;

import br.com.dexsystem.model.Dispositivo;

public class DispositivoDao extends GenericDao<Dispositivo> {
	
	public DispositivoDao() {
        super();
    }
}
