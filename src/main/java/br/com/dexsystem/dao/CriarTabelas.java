package br.com.dexsystem.dao;

import java.io.Serializable;

import javax.persistence.Query;

import org.hibernate.Session;

import br.com.dexsystem.util.HibernateUtil;

public class CriarTabelas implements Serializable {
	
	private static final long serialVersionUID = 1102552160150400784L;
	private Session session;

	public CriarTabelas() {
		super();
	}
	
	public void criarTabelas() throws Exception {
		try {
			criarTabelaUsuario();
			criarTabelaMotivo();
			criarTabelaUnidadeMedida();
			criarTabelaGrade();
			criarTabelaGradeItem();
			criarTabelaEpi();
			criarTabelaPessoa();
			criarTabelaPessoaJuridica();
			criarTabelaEmpresa();
			criarTabelaSetor();
			criarTabelaCargo();
			criarTabelaPessoaFisica();
			criarTabelaCargoEpi();
			criarTabelaEmpregado();
			criarTabelaAtividade();
			criarTabelaParametrosSistema();
			criarTabelaEmpregadoEpi();
			criarTabelaPessoaDigital();
			criarTabelaDispositivo();
			criarTabelaControleSincronizacao();
		} catch(Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		} finally {
			fecharSession();
		}
	}
		
	private void criarTabelaUsuario() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS usuario ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "nome VARCHAR(255) NOT NULL, "
						+ "senha VARCHAR(255) NOT NULL, "
						+ "email VARCHAR(255) NOT NULL, "
						+ "acesso_desktop CHAR(1) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL);";
		criarTabela("usuario", sql);
	}
	
	private void criarTabelaMotivo() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS motivo ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "descricao VARCHAR(200), "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL);";
		criarTabela("motivo", sql);
	}
	
	private void criarTabelaUnidadeMedida() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS unidade_medida ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "descricao VARCHAR(200) NOT NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL);";
		criarTabela("unidade_medida", sql);
	}
	
	private void criarTabelaGrade() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS grade ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "nome VARCHAR(200) NOT NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL);";
		criarTabela("grade", sql);
	}

	private void criarTabelaGradeItem() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS grade_item ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "grade_id NUMERIC(10,0) NOT NULL, "  
						+ "tamanho VARCHAR(50) NOT NULL, " 
						+ "ordem INTEGER NOT NULL,"
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_grade_item_0 FOREIGN KEY (grade_id) REFERENCES grade (id))";
		criarTabela("grade_item", sql);
	}
	
	private void criarTabelaEpi() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS epi ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "grade_id NUMERIC(10,0) NULL, "
						+ "unidade_id NUMERIC(10,0) NULL, "
						+ "nome VARCHAR(200) NOT NULL, "
						+ "ca VARCHAR(10) NULL, "
						+ "data_validade_ca DATE NULL, "
						+ "vida_util INTEGER NULL, "
						+ "vida_util_unidade CHAR(1) NULL, "
						+ "descartavel CHAR(1) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_epi_0 FOREIGN KEY (grade_id) REFERENCES grade (id), "
						+ "CONSTRAINT FK_epi_1 FOREIGN KEY (unidade_id) REFERENCES unidade_medida (id))";
		criarTabela("epi", sql);
	}
	
	private void criarTabelaPessoa() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS pessoa ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "nome VARCHAR(500) NOT NULL, "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL)";
		criarTabela("pessoa", sql);
	}
	
	private void criarTabelaPessoaJuridica() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS pessoa_juridica ("
						+ "pessoa_id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "nome_fantasia VARCHAR(500) NULL, "
						+ "cnpj CHAR(14) NOT NULL, "
						+ "inscricao_estadual VARCHAR(50) NULL, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_pessoa_juridica_0 FOREIGN KEY (pessoa_id) REFERENCES pessoa (id))";
		criarTabela("pessoa_juridica", sql);
	}
	
	private void criarTabelaEmpresa() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS empresa ("
						+ "pessoa_id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "tipo CHAR(1) NULL, "
						+ "matriz_id INTEGER NULL, "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_empresa_0 FOREIGN KEY (pessoa_id) REFERENCES pessoa_juridica (pessoa_id), "
						+ "CONSTRAINT FK_empresa_1 FOREIGN KEY (matriz_id) REFERENCES empresa (pessoa_id))";
		criarTabela("empresa", sql);
	}
	
	private void criarTabelaSetor() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS setor ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "empresa_id INT NOT NULL, "
						+ "nome VARCHAR(200) NOT NULL, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_setor_0 FOREIGN KEY (empresa_id) REFERENCES empresa (pessoa_id))";
		criarTabela("setor", sql);
	}
	
	private void criarTabelaCargo() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS cargo ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "setor_id INT NOT NULL, "
						+ "nome VARCHAR(200) NOT NULL, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_cargo_0 FOREIGN KEY (setor_id) REFERENCES setor (id))";
		criarTabela("cargo", sql);
	}
	
	private void criarTabelaPessoaFisica() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS pessoa_fisica ("
						+ "pessoa_id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "cpf CHAR(11) NOT NULL, "
						+ "rg VARCHAR(50) NULL, "
						+ "data_nascimento DATE NULL, "
						+ "sexo CHAR(1) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_pessoa_fisica_0 FOREIGN KEY (pessoa_id) REFERENCES pessoa (id))";
		criarTabela("pessoa_fisica", sql);
	}
	
	private void criarTabelaCargoEpi() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS cargo_epi ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "cargo_id INT NOT NULL, "
						+ "epi_id INT NOT NULL, "
						+ "quantidade INT NOT NULL DEFAULT 1, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_cargo_epi_0 FOREIGN KEY (cargo_id) REFERENCES cargo (id), "
						+ "CONSTRAINT FK_cargo_epi_1 FOREIGN KEY (epi_id) REFERENCES epi (id))";
		criarTabela("cargo_epi", sql);
	}
	
	private void criarTabelaEmpregado() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS empregado ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "pessoa_id INT NOT NULL, "
						+ "empresa_id INT NOT NULL, "
						+ "data_admissao DATE NOT NULL, "
						+ "data_demissao DATE NULL, "
						+ "matricula VARCHAR(30) NULL, "
						+ "senha VARCHAR(255) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_empregado_0 FOREIGN KEY (pessoa_id) REFERENCES pessoa (id), "
						+ "CONSTRAINT FK_empregado_1 FOREIGN KEY (empresa_id) REFERENCES empresa (pessoa_id))";
		criarTabela("empregado", sql);
	}
	
	private void criarTabelaAtividade() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS atividade ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "empregado_id INT NOT NULL, "
						+ "cargo_id INT NOT NULL, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "funcao VARCHAR(200) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_atividade_0 FOREIGN KEY (empregado_id) REFERENCES empregado (id), "
						+ "CONSTRAINT FK_atividade_1 FOREIGN KEY (cargo_id) REFERENCES cargo (id))";
		criarTabela("atividade", sql);
	}
	
	private void criarTabelaParametrosSistema() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS parametro_sistema ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "selecao_empresa CHAR(1) NOT NULL, "
						+ "selecao_epi CHAR(1) NOT NULL, "
						+ "selecao_pessoa_fisica CHAR(1) NOT NULL, "
						+ "selecao_empregado CHAR(1) NOT NULL, "
						+ "informar_motivo CHAR(1) NOT NULL, "
						+ "texto_ficha_epi VARCHAR(1000) NULL, "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL)";
		criarTabela("parametro_sistema", sql);
	}
	
	private void criarTabelaEmpregadoEpi() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS empregado_epi ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "empregado_id INT NOT NULL, "
						+ "epi_id INT NOT NULL, "
						+ "usuario_id INT NOT NULL, "
						+ "grade_item_id INT NULL, "
						+ "quantidade INT NOT NULL, "
						+ "data_prevista_entrega DATE NOT NULL, "
						+ "data_entrega DATE NULL, "
						+ "data_prevista_troca DATE NULL, "
						+ "data_confirmacao DATETIME NULL, "
						+ "data_substituicao DATE NULL, "
						+ "data_devolucao DATE NULL, "
						+ "motivo_id INT NULL, "
						+ "tipo_confirmacao CHAR(1) NULL, "
						+ "inclusao_manual CHAR(1) NULL, "
						+ "excluido CHAR(1) NOT NULL DEFAULT '0', "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_empregado_epi_0 FOREIGN KEY (empregado_id) REFERENCES empregado (id), "
						+ "CONSTRAINT FK_empregado_epi_1 FOREIGN KEY (motivo_id) REFERENCES motivo (id), "
						+ "CONSTRAINT FK_empregado_epi_2 FOREIGN KEY (epi_id) REFERENCES epi (id), "
						+ "CONSTRAINT FK_empregado_epi_3 FOREIGN KEY (grade_item_id) REFERENCES grade_item (id), "
						+ "CONSTRAINT FK_empregado_epi_4 FOREIGN KEY (usuario_id) REFERENCES usuario (id))";
		criarTabela("empregado_epi", sql);
	}
	
	private void criarTabelaPessoaDigital() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS pessoa_digital ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "pessoa_id INT NOT NULL, "
						+ "posicao CHAR(2) NOT NULL, "
						+ "digital IMAGE NULL, "
						+ "anulado CHAR(1) NULL, "
						+ "motivo_anulacao VARCHAR(100) NULL, "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_pessoa_digital_0 FOREIGN KEY (pessoa_id) REFERENCES pessoa_fisica (pessoa_id))";
		criarTabela("pessoa_digital", sql);
	}
	
	private void criarTabelaDispositivo() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS dispositivo ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "versao VARCHAR(10) NOT NULL, "
						+ "data_inicio DATE NOT NULL, "
						+ "data_fim DATE NULL, "
						+ "data_sincronizacao DATETIME NULL)";
		criarTabela("dispositivo", sql);
	}
	
	private void criarTabelaControleSincronizacao() throws Exception {
		String sql = "CREATE TABLE IF NOT EXISTS controle_sincronizacao ("
						+ "id NUMERIC(10,0) NOT NULL PRIMARY KEY, "
						+ "dispositivo_id INT NOT NULL, "
						+ "tabela VARCHAR(50) NOT NULL, "
						+ "data_hora DATETIME NOT NULL, "
						+ "checksum VARCHAR(50) NOT NULL, "
						+ "checksum_alteracao VARCHAR(50) NOT NULL, "
						+ "CONSTRAINT FK_controle_sincronizacao_0 FOREIGN KEY (dispositivo_id) REFERENCES dispositivo (id))";
		criarTabela("controle_sincronizacao", sql);
	}
	
	
	private void criarTabela(String tabela, String sql) throws Exception {
		try {
			getSession().getTransaction().begin();
			Query query = getSession().createNativeQuery(sql);
			query.executeUpdate();
			getSession().getTransaction().commit();
			getSession().close();
		} catch(Exception e) {
			throw new Exception("Ocorreu um erro ao criar a tabela "+tabela+"!", e);
		}
	}
	
	private Session getSession() {
        if (this.session == null || !this.session.isOpen()) {
            this.session = HibernateUtil.getSession();
        }
        return this.session;
    }
	
	private void fecharSession() {
		this.session = null;
	}

}
