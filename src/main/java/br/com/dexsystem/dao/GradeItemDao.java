package br.com.dexsystem.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.dexsystem.model.GradeItem;

public class GradeItemDao extends GenericDao<GradeItem> {

	public GradeItemDao() {
        super();
    }
	
	
	public List<GradeItem> buscarPorIdGrade(Long idGrade) {
		Criteria criteria = getSession().createCriteria(GradeItem.class);
		criteria.add(Restrictions.eq("idGrade", idGrade));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.addOrder(Order.asc("ordem"));
		return criteria.list();
	}
}