package br.com.dexsystem;

import java.awt.Frame;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import javax.swing.SwingWorker;

import br.com.dexsystem.dao.CriarTabelas;
import br.com.dexsystem.model.vo.CredencialVO;
import br.com.dexsystem.service.sincronizacao.Sincronizacao;
import br.com.dexsystem.thread.ConexaoThread;
import br.com.dexsystem.thread.SincronizacaoThread;
import br.com.dexsystem.util.ConstantUtils;
import br.com.dexsystem.util.FileUtils;
import br.com.dexsystem.util.LoggerUtils;
import br.com.dexsystem.util.MensageiroUtils;
import br.com.dexsystem.view.Carregando;
import br.com.dexsystem.view.Dashboard;
import br.com.dexsystem.view.Login; 

public class Principal {
	
	public static void main(String[] args) {
		adicionarEstiloSistema();
		bloquearArquivo();
		iniciar();
	}
	
	public static void iniciar() {
		ConexaoThread conexaoThread = new ConexaoThread();
		
		new SwingWorker() {
			@Override
            protected Object doInBackground() throws Exception {
				conexaoThread.verificaConexoes();
				return null;
			}
		}.execute();
		
		Login login = new Login();
		CredencialVO credencial = login.abrirTela();
		if (!credencial.isAutenticado()) {
			System.exit(0);
		}
		
		Dashboard principal = new Dashboard(credencial);
		principal.abrirTela();
		
		criarTabelas(principal);
		
		if (ConexaoThread.temConexao) {
			Sincronizacao sincronizacao = new Sincronizacao(principal, true);
			sincronizacao.sincronizar(credencial.getToken());
			sincronizacao = null;
		} else {
			principal.carregarInformacoes();
			principal.atualizarDataSincronizacao();
			principal.popularTabela();
		}
		
		SincronizacaoThread sincronizacaoThread = new SincronizacaoThread(principal, credencial.getToken());
		new Thread(sincronizacaoThread).start();
		new Thread(conexaoThread).start();
		
		credencial = null;
	}
	

	private static void criarTabelas(Frame parent) {
		Carregando carregando = new Carregando(parent);
		carregando.abrirTela("Mantendo banco de dados", 1);
		try {
			CriarTabelas criarTabelas = new CriarTabelas();
			criarTabelas.criarTabelas();
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			e.printStackTrace();
			MensageiroUtils.errorMsg(null, "Ocorreu algum problema ao criar o banco de dados e o "+ConstantUtils.NOME_SISTEMA+" ser� fechado! Contate um administrador.");
			System.exit(0);
		}
		carregando.fecharTela();
		carregando = null;
	}
	
	private static void adicionarEstiloSistema() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			LoggerUtils.logErro(e);
			e.printStackTrace();
		}
	}

	private static void bloquearArquivo() {
		try {
			// verificar pasta no usu�rio
			FileUtils.verificarCriarDiretorio(ConstantUtils.getDiretorioUsuario());
			
			String env = null;
			File lockFile = new File(env, ConstantUtils.getDiretorioUsuario() + File.separator + "dexsystem.lock");
			if (lockFile.exists()) {
				lockFile.delete();
			}
			FileOutputStream lockFileOS = new FileOutputStream(lockFile);
			lockFileOS.close();
			FileChannel lockChannel = new RandomAccessFile(lockFile, "rw").getChannel();
			FileLock lock = lockChannel.tryLock();
			if (lock == null) {
				throw new Exception("N�o foi poss�vel obter o bloqueio");
			}
		} catch (Exception e) {
			MensageiroUtils.warnMsg(null, "O " + ConstantUtils.NOME_SISTEMA + " j� est� aberto!");
			e.printStackTrace();
			System.exit(0);
		}
	}
}
